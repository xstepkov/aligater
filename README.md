# AliGater

`AliGater` is a tool for complementing nondeterministic finite automata.

## Installation

The following commands install the `aligater` package. It is recommended to use a python virtual environment.

```
git clone https://gitlab.fi.muni.cz/xstepkov/aligater.git
cd aligater
pip install .
```

To run experiments or attached jupyter notebooks, additional dependencies may be needed.

`AliGater` is developed and tested on Linux (Ubuntu 24.04.1 LTS). We do not guarantee anything for other operation systems.

## Usage

To complement an NFA in one of the [supported formats](#input-format), run

```aligater <input path> <complementation algorithm>```

substituting `pow` for powerset, `gate`, or `port` for `<complementation algorithm>`.

To see all possible arguments, run

```aligater -h```

To see a small demonstration of AliGater, run the `demonstration.ipynb` notebook. For this, `jupyter` and `dot` must be installed. 

## Input format

AliGater can read files in the following formats:

* [.mata](https://github.com/VeriFIT/mata/blob/devel/AUTOMATAFORMAT.md)
* [.ba](https://languageinclusion.org/doku.php?id=tools) 
* .dot (a specific subset of the format)

## Compiling a standalone executable

### Pyinstaller

Inside your virtual environment with all dependencies installed, run

```
pip install --upgrade pyinstaller
pyinstaller --add-data=aligater/RABIT:aligater/RABIT --collect-all libmata --hidden-import tabulate run.py
```

The executable should be in `./dist/run`.

Pyinstaller has issues locationg graphviz correctly. If you get an error like this when running pyinstaller, try the following steps.

```
...
49395 INFO: Processing standard module hook 'hook-pygraphviz.py' from '/path/to/aligater/.venv/lib/python3.12/site-packages/_pyinstaller_hooks_contrib/stdhooks'
Unable to find '/usr/sbin/neato' when adding binary and data files.
```

In the file `/path/to/aligater/.venv/lib/python3.12/site-packages/_pyinstaller_hooks_contrib/stdhooks/hook-pygraphviz.py` (may be on a different different in your graphviz installation), exchange the line `graphviz_bindir = os.path.dirname(os.path.realpath(shutil.which("dot")))` for `graphviz_bindir = os.path.dirname(shutil.which("dot"))`.

Then manually add the graphviz binary as follows when running pyinstaller. The `--clean` option cleans the cache from previous installs. The `--onefile` option compiles the whole package into one file (useful for benchmarking). 

```
pyinstaller --clean --onefile --add-data=aligater/RABIT:aligater/RABIT --collect-all libmata --hidden-import tabulate --add-binary /usr/lib/x86_64-linux-gnu/graphviz:graphviz run.py
```

The path to the graphviz binary `/usr/lib/x86_64-linux-gnu/graphviz` may be different on your machine. 

### cx_Freeze

Inside your virtual environment with all dependencies installed, run

```
pip install --upgrade cx_Freeze
python cxfreeze_setup.py build
```

The executable will be `build/exe.[your system specifics]/run`.

## License
Copyright © 2000 Adéla Štěpková <xstepkov@fi.muni.cz>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
