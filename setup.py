from setuptools import setup, find_namespace_packages

setup(
    name = 'aligater',
    version = '0.4',
    author = "Adela Stepkova",
    description = "A tool for complementing nondeterministic finite automata.",
    include_package_data=True,
    packages = find_namespace_packages(exclude=["scripts", "experiments", "build"]),
    install_requires = [
        "libmata>=1.7.5",
        "networkx>=3.1",
        "numpy>=1.21.5",
        "setuptools>=59.6.0",
        "automata-lib[visual]>=8.3.0",
        "pytest>=8.3.3"
    ],
    entry_points = {
        'console_scripts': [
            'aligater=aligater.cli:main',
        ],
    },
)