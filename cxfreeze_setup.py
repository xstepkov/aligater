from cx_Freeze import setup, Executable
import sys

# Additional packages to include
build_exe_options = {
    'packages': ['libmata', 'tabulate', 'networkx', 'numpy', 'automata'],
    'include_files': [('aligater/RABIT', 'aligater/RABIT')],
}


# Define the executable to be created
executable = Executable(
    script='run.py',  # The script that acts as the entry point of the application
    base="Console"
)

# Call the setup function
setup(
    name='aligater',
    version='0.2',
    description='A tool for complementing nondeterministic finite automata.',
    options={'build_exe': build_exe_options},
    executables=[executable]
)
