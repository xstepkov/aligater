from typing import Any
from automata.fa.nfa import NFATransitionsT
from automata.fa.dfa import DFATransitionsT

# Type aliases for structures used by ExtendedNFA
State = Any
Symbol = str
States = set[State]
Alphabet = set[Symbol]
NFATransDict = dict[State, dict[Symbol, States]]
DFATransDict = dict[State, dict[Symbol, State]]
SymbolGroup = dict[str, States]

# Type aliases for structures from automata-lib. Immutable.
NFATransFrozen = NFATransitionsT
DFATransFrozen = DFATransitionsT

# Type aliases for structures used in complementation
PortSetsMap = dict[frozenset[State], set[State]]
PortSets = list[frozenset[State]]
PortMap = dict[State, set[State]]

TupleTransitions = set[tuple[State, str, State]]