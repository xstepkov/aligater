from enum import Enum

class ComplementMode(Enum):
    POWERSET = "pow"
    GATE = "gate"
    SEQ = "seq"
    FORWARD_FIRST = "ff"
    REVERSE_FIRST = "rf"
    HEURISTIC = "h"
    RESIDUAL = "res"


class PortConnectMode(Enum):
    INDIVIDUAL = "ind"
    GROUPED = "gr"


class PortPartitionMode(Enum):
    SCC = "s"
    LREV = "r"
    CUSTOM = "c"
    DETERMINISTIC = "d"
    MIN_CUT = "m"
    DET_LREV = "dr"


class Heuristic(Enum):
    DET_SUCCS = "ds"
    NONDET_STATES = "ns"


class Direction(Enum):
    FORWARD = "f"
    REVERSE = "r"


class PowersetDirection(Enum):
    FORWARD = "f"
    REVERSE = "r"
    HEURISTIC = "h"
    BEST = "b"


class ModeWithDescription:
    def __init__(self, mode_type: Enum, description: str):
        self.type = mode_type
        self.name = mode_type.name.lower()
        self.command = mode_type.value
        self.description = description


COMPLEMENT_MODE_LIST = [
        ModeWithDescription(ComplementMode.POWERSET, "powerset complement"),
        ModeWithDescription(ComplementMode.GATE, "gate complement"),
        ModeWithDescription(ComplementMode.SEQ, "sequential complement"),
        ModeWithDescription(ComplementMode.FORWARD_FIRST, "forward-first complement"),
        ModeWithDescription(ComplementMode.REVERSE_FIRST, "reverse-first complement"),
        ModeWithDescription(ComplementMode.HEURISTIC, "forward-first/reverse-first complement chosen by a heuristic"),
        ModeWithDescription(ComplementMode.RESIDUAL, "use the residual reduction (must specify direction)")
    ]

PORT_CONNECT_MODE_LIST = [
    ModeWithDescription(PortConnectMode.INDIVIDUAL, "consider target entry ports individually"),
    ModeWithDescription(PortConnectMode.GROUPED, "take all target entry ports as a set"),
]

PORT_PARTITION_MODE_LIST = [
    ModeWithDescription(PortPartitionMode.SCC, "partition into sccs"),
    ModeWithDescription(PortPartitionMode.LREV, "partition into 2 components, second is the largest possible reverse deterministic"),
    ModeWithDescription(PortPartitionMode.DETERMINISTIC, "partition into deterministic components"),
    ModeWithDescription(PortPartitionMode.MIN_CUT, "minimal cut partition"),
    ModeWithDescription(PortPartitionMode.DET_LREV, """partition into deterministic components,
    last component is the largest possible reverse deterministic"""),
]

POWERSET_DIRECTION_LIST = [
    ModeWithDescription(PowersetDirection.BEST, "compute both complements and take the smaller one"),
    ModeWithDescription(PowersetDirection.HEURISTIC, "choose the direction by the `deterministic successors` heuristic"),
    ModeWithDescription(PowersetDirection.FORWARD, "always choose forward powerset"),
    ModeWithDescription(PowersetDirection.REVERSE, "always choose reverse powerset"),
]

    
def translate_from_string(mode_list: list[ModeWithDescription], mode_str: str) -> Enum | None:
    if mode_str == "":
        return None
    for mode in mode_list:
        if mode.command == mode_str:
            return mode.type
    raise ValueError(f"Invalid mode: {mode_str}")


def help(mode_list: list[ModeWithDescription]):
    help_str = ""
    for mode in mode_list:
        help_str += f"{mode.command}\t{mode.description}\n"
    return help_str


class Option(Enum):
    RABIT = "mr"
    DFA_MIN = "md"
    RABIT_AFTER = "mra"
    SIM_MIN = "ms"
    RES_MIN = "mres"
    RES_MIN_AFTER = "mresa"
    VALIDATE = "V"
    REVERSE = "r"
    POWERSET_DIRECTION = "D"
    PORT_PARTITION = "P"
    PORT_CONNECT = "C"


class ArgWithDescription:
    def __init__(self, opt_type: Option, long_arg_name: str, data_export_name: str, description: str, 
                 options: list[ModeWithDescription] | None = None):
        self.type = opt_type
        self.short_arg_name = "-" + opt_type.value
        self.long_arg_name = long_arg_name
        self.description = description
        self.data_export_name = data_export_name
        self.options = options


ARGUMENTS = [
    ArgWithDescription(Option.REVERSE, "--reverse", "r", "complement a reversed automaton"),
    ArgWithDescription(Option.DFA_MIN, "--min-dfa", "dfa_min", 
                       "minimize every deterministic automaton"),
    ArgWithDescription(Option.RABIT, "--min-rabit-during", "rabit", 
                       "minimize every intermediate result with RABIT"),
    ArgWithDescription(Option.RABIT_AFTER, "--min-rabit-after", "rabit_after", 
                       "minimize with RABIT after computation"),
    ArgWithDescription(Option.SIM_MIN, "--min-simulation", "sim_min", 
                       "apply simulation reduction on every intermediate result (using `reduce` from `mata`)"),
    ArgWithDescription(Option.RES_MIN, "--min-residual", "res_min", 
                       "apply residual reduction on every intermediate result (using `reduce_residual` from `mata`)"),
    ArgWithDescription(Option.RES_MIN_AFTER, "--min-residual-after", "res_min_after", 
                       "apply residual reduction after computation (using `reduce_residual` from `mata`)"),
    ArgWithDescription(Option.POWERSET_DIRECTION, "--pow-direction", "", 
                       "which powerset direction to choose in sequential and gate complementation:\n", POWERSET_DIRECTION_LIST),
    ArgWithDescription(Option.PORT_PARTITION, "--port-partition", "",
                       "how to partition the input automaton for sequential complement:\n", PORT_PARTITION_MODE_LIST),
    ArgWithDescription(Option.PORT_CONNECT, "--port-connect", "", 
                       "how the components should be connected in sequential complement:\n", PORT_CONNECT_MODE_LIST),
    ArgWithDescription(Option.VALIDATE, "--validate", "", 
                       "check produced complement for correctness")
]


class PortOptions:
    """
    Options container for port complementation.
    """
    def __init__(self, connect_mode: PortConnectMode, partition_method: PortPartitionMode, 
                 rename: bool = True):
        self.connect_mode = connect_mode
        self.partition_method = partition_method
        self.rename = rename


class ComplementationOptions:
    """
    Class for sending info to subprograms.
    """
    
    def __init__(self, rabit_during: bool = False, dfa_min: bool = False, rabit_min_after: bool = False, 
                 simulation_min: bool = False, residual_min: bool = False, residual_min_after: bool = False,
                 validate: bool = False, 
                 reverse: bool = False, powerset_direction: PowersetDirection = PowersetDirection.BEST,
                 lookahead: int = 10):
        self.rabit_during = rabit_during
        self.dfa_min = dfa_min
        self.rabit_min_after = rabit_min_after
        self.simulation_min = simulation_min
        self.residual_min = residual_min 
        self.residual_min_after = residual_min_after
        self.validate = validate
        self.reverse = reverse
        self.lookahead = lookahead
        self.powerset_direction = powerset_direction

    def list_active(self) -> list[Option]:
        active = []

        if self.reverse:
            active.append(Option.REVERSE)
        if self.dfa_min:
            active.append(Option.DFA_MIN)
        if self.rabit_during:
            active.append(Option.RABIT)
        if self.rabit_min_after:
            active.append(Option.RABIT_AFTER)
        if self.simulation_min:
            active.append(Option.SIM_MIN)
        if self.residual_min:
            active.append(Option.RES_MIN)
        if self.residual_min_after:
            active.append(Option.RES_MIN_AFTER)
        if self.validate:
            active.append(Option.VALIDATE)

        return active

