from aligater.types.aliases import *
from copy import deepcopy
from collections.abc import Iterator, KeysView, ValuesView, ItemsView
from automata.fa.nfa import NFA
from automata.fa.dfa import DFA
import networkx as nx
from pprint import pprint

class NFATransitions:
    def __init__(self, trans_dict: NFATransDict | None = None):
        self._transitions: NFATransDict = trans_dict if trans_dict is not None else {}

    def __getitem__(self, state: State):
        return self._transitions[state]
    
    def __setitem__(self, state: State, inside_dict: dict[Symbol, States]):
        if not isinstance(inside_dict, dict):
            raise TypeError("Object is not of type 'dict'.")

        self._transitions[state] = inside_dict

    def __iter__(self):
        return iter(self._transitions)

    def __repr__(self):
        return repr(self._transitions)

    def __len__(self):
        return len(self._transitions)

    def get(self, state, default):
        return self._transitions.get(state, default)
    
    def values(self) -> ValuesView[dict[Symbol, States]]:
        return self._transitions.values()

    def keys(self) -> KeysView[State]:
        return self._transitions.keys()
    
    def items(self) -> ItemsView[State, dict[Symbol, States]]:
        return self._transitions.items()
    
    def deepcopy(self) -> 'NFATransitions':
        return NFATransitions(deepcopy(self._transitions))

    def get_dict(self) -> NFATransDict:
        return self._transitions
    
    def empty(self) -> bool:
        return not self._transitions
    
    def add_edge(self, symbol: Symbol, src: State, dst: State):
        if src not in self._transitions:
            self._transitions[src] = {}
        if symbol not in self._transitions[src]:
            self._transitions[src][symbol] = set()
        self._transitions[src][symbol].add(dst)

    def has_transition_under_symbol(self, state: State, symbol: Symbol) -> bool:
        return (state in self._transitions and 
               symbol in self._transitions[state] and
               len(self._transitions[state][symbol]) > 0)
               

    def iterate_dests_only(self, state: State) -> Iterator[State]:
        """
        Iterable of all states to which there is a transition from a given state.
        """
        if state in self._transitions:
            returned_dests = set()

            for dests in self._transitions[state].values():
                for dst in dests:
                    if dst not in returned_dests:
                        returned_dests.add(dst)
                        yield dst

    def iterate_all(self) -> Iterator[tuple[State, Symbol, State]]:
        """
        Iterable of all transitions from a given state.
        Returns tuples (source, symbol, destination).
        """
        for src in self._transitions:
            for symbol, dests in self._transitions[src].items():
                for dst in dests: 
                    yield src, symbol, dst


    def iterate(self, state: State, symbol: Symbol) -> Iterator[State]:
        """
        Iterable over all destinations from a given state under given symbol.
        """
        if state in self._transitions and symbol in self._transitions[state]:
            for dst in self._transitions[state][symbol]:
                yield dst

    def iterate_sym(self, state: State) -> Iterator[tuple[Symbol, State]]:
        """
        Iterable over all transitions from a given state.
        Returns tuples (symbol, destination).
        """
        if state in self._transitions:
            for symbol_inside, dests in self._transitions[state].items():
                for dst in dests:
                    yield symbol_inside, dst

    def get_deterministic_dict(self) -> DFATransDict:
        """
        Convert to (automata-lib) DFATransitionsT.
        Suppose transitions are deterministic 
        (there is at most 1 element in each destination set).
        """
        new_transitions: DFATransDict = {}

        for src in self._transitions:
            for symbol, dests in self._transitions[src].items():
                assert len(dests) <= 1
                
                if len(dests) == 1:
                    dst = next(iter(dests))
                    _add_edge_dfa(new_transitions, symbol, src, dst)

                else:
                    raise ValueError("Transitions are not deterministic.")

        return new_transitions
    
    def reverse(self) -> 'NFATransitions':
        """
        Return a dict of reversed transitions.
        For a transition (p, a, q) in 'transitions', 
        the new dict contains a transition (q, a, p).
        """
        new_transitions = NFATransitions()

        for src, symbol, dst in self.iterate_all():
            new_transitions.add_edge(symbol, dst, src)

        return new_transitions
    
    def update(self, other: 'NFATransitions'):
        if not self._transitions.keys().isdisjoint(other._transitions.keys()):
            raise ValueError("Dict keys are not disjoint, cannot union transitions.")

        self._transitions.update(other._transitions)

    def extract_symbols(self) -> Alphabet:
        return { symbol for _, symbol, _ in self.iterate_all() }

    @classmethod
    def from_frozen(cls, frozen: NFATransFrozen) -> 'NFATransitions':
        """
        Convert from (automata-lib) NFATransitionsT.
        """
        new_transitions = {}
        for src, src_transitions in frozen.items():
            new_transitions[src] = {}
            for symbol, dsts in src_transitions.items():
                new_transitions[src][symbol] = set(dsts)

        return NFATransitions(new_transitions)
    
    @classmethod
    def from_det_frozen(cls, frozen: NFATransFrozen) -> 'NFATransitions':
        """
        Convert from (automata-lib) DFATransitionsT.
        """
        new_transitions = NFATransitions()

        for src in frozen:
            for symbol, dst in frozen[src].items():
                new_transitions.add_edge(symbol, src, dst)

        return new_transitions


BetweenTransitions = dict[tuple[int, int], NFATransitions]

class pfrozenset(frozenset):
    """
    Custom frozenset type for printing:
        prints {1, 2, 3}
        instead of frozenset({1, 2, 3})
    """
    def __repr__(self):
        return set(self).__repr__()

    def __str__(self):
        return set(self).__str__()
    
    @classmethod
    def convert_rec(cls, value: Any):
        if isinstance(value, frozenset):
            return pfrozenset([cls.convert_rec(elem) for elem in value])
        elif isinstance(value, list):
            return [cls.convert_rec(elem) for elem in value]
        elif isinstance(value, set):
            return set([cls.convert_rec(elem) for elem in value])
        elif isinstance(value, tuple):
            return tuple([cls.convert_rec(elem) for elem in value])
        elif isinstance(value, dict):
            return {cls.convert_rec(key): cls.convert_rec(val) for key, val in value.items()}
        return value


class ExtendedNFA:
    """
    A nondeterministic finite automaton.
    Extends the class NFA from the library automata-lib.
    Supports multiple initial states and storing entry and exit ports.
    All data types are compatible with the class NFA, 
    transitions can be handled by the same functions.
    Used inside of our complementation functions.
    Mutable.
    """
    def __init__(self, states: States, 
                 input_symbols: Alphabet, 
                 transitions: NFATransitions | NFATransDict,
                 initial_states: States, 
                 final_states: States, 
                #  entry_ports: States | None = None, 
                #  exit_ports: States | None = None,
                #  in_group: SymbolGroup | None = None, 
                #  out_group: SymbolGroup | None = None,
                 sink: State | None = None):
        self.states = states
        self.input_symbols = input_symbols
        self.initial_states = initial_states
        self.final_states = final_states

        if isinstance(transitions, dict):
            self.transitions = NFATransitions(transitions)
        else:
            self.transitions: NFATransitions = transitions

        # self.entry_ports: States = entry_ports if entry_ports is not None else set()
        # self.exit_ports: States = exit_ports if exit_ports is not None else set()
        # self.in_group: SymbolGroup = in_group if in_group is not None else {}
        # self.out_group: SymbolGroup = out_group if out_group is not None else {}
        self.sink = sink

        self.validate()

    def validate(self):
        for state in self.transitions:
            for symbol, dst in self.transitions.iterate_sym(state):
                if not isinstance(symbol, str):
                    raise ValueError("Input symbols must be of type str.")
                if symbol == "":
                    raise ValueError("Epsilon-transitions are \
                                      not allowed in ExtendedNFA.")
                if symbol not in self.input_symbols:
                    raise ValueError(f"Symbol '{symbol}' not in input_symbols.")
                
                if state not in self.states:
                    raise ValueError(f"State {state} not in states.")
                if dst not in self.states:
                    raise ValueError(f"State {dst} not in states.")
                
        for state in self.initial_states:
            if state not in self.states:
                raise ValueError(f"Initial state {str(state)} not in states.")
            
        for state in self.final_states:
            if state not in self.states:
                raise ValueError(f"Final state {state} not in states.")

    def copy(self) -> 'ExtendedNFA':
        return ExtendedNFA(states=self.states.copy(),
                           input_symbols=self.input_symbols.copy(),
                           transitions=self.transitions.deepcopy(),
                           initial_states=self.initial_states.copy(),
                           final_states=self.final_states.copy(),
                           sink=self.sink
                           )


    def reverse(self) -> "ExtendedNFA":
        rev_transitions = self.transitions.reverse()
        return ExtendedNFA(states=self.states.copy(),
                           input_symbols=self.input_symbols.copy(),
                           transitions=rev_transitions,
                           initial_states=self.final_states.copy(),
                           final_states=self.initial_states.copy(),
                        #    entry_ports=self.exit_ports.copy(),
                        #    exit_ports=self.entry_ports.copy(),
                        #    in_group=deepcopy(self.out_group),
                        #    out_group=deepcopy(self.in_group)
                           )
    
    def remove_unused_symbols(self):
        used = self.used_symbols()
        self.input_symbols = used

    def used_symbols(self) -> set[str]:
        symbols: set[str] = set()

        for state in self.transitions:
            for symbol in self.transitions[state].keys():
                symbols.add(symbol)
        
        return symbols
    
    def remove_symbols(self, symbols_to_keep: set[Symbol]) -> 'ExtendedNFA':
        """
        Remove unwanted symbols from alphabet, all transitions with those symbols, 
        and states that become unreachable or dead.
        """
        new_transitions = NFATransitions()
        for src, symbol, dst in self.transitions.iterate_all():
            if symbol in symbols_to_keep:
                new_transitions.add_edge(symbol, src, dst)
        
        reachable = _compute_reachable_states(new_transitions, self.initial_states)
        nondead = _compute_reachable_states(new_transitions.reverse(), self.final_states)
        new_states, new_transitions, new_final = _remove_states(new_transitions, self.final_states, self.states & reachable & nondead)
        
        if not self.initial_states & new_states:
            return ExtendedNFA.create_empty(self.input_symbols)

        return ExtendedNFA(states=new_states,
                           input_symbols=symbols_to_keep,
                           transitions=new_transitions,
                           initial_states=self.initial_states & new_states,
                           final_states=new_final)


    def is_deterministic(self):
        if "" in self.input_symbols:
            return False
        
        if len(self.initial_states) > 1:
            return False

        for src in self.transitions:
            for symbol, dests in self.transitions[src].items():
                if len(dests) > 1:
                    return False
        
        return True

    def single_final_state(self) -> State:
        """
        Create a new single final state if there are multiple.
        Add epsilon-transitions from old final states to the new state.
        Modifies the arguments.
        """

        if len(self.final_states) == 1:
            return next(iter(self.final_states))
        
        new_fin_state = self.add_state_with_transitions(self.final_states, set())
        self.final_states = {new_fin_state}
        return new_fin_state

    def single_initial_state(self, epsilon_allowed: bool = True) -> State:
        """
        Create a new single initial state if there are multiple.
        Add epsilon-transitions from new initial state to all old ones.
        Modifies the arguments, does not modify 'initial states'.
        """

        if len(self.initial_states) == 1:
            return next(iter(self.initial_states))
        
        if epsilon_allowed:
            new_init_state = self.add_state_with_transitions(set(), self.initial_states)
            self.initial_states = {new_init_state}
            return new_init_state
        
        # epsilon-transitions are not allowed
        new_init_state = _unoccuring_state(self.states)
        self.states.add(new_init_state)

        for old_init_state in self.initial_states:
            for symbol, destinations in self.transitions.get(old_init_state, {}).items():
                for dst in destinations:
                    self.transitions.add_edge(symbol, new_init_state, dst)

        if self.final_states is None:
            raise ValueError("final states must be provided if epsilon-transitions are not allowed")

        if self.final_states & self.initial_states:
            self.final_states.add(new_init_state)

        self.initial_states = {new_init_state}

        return new_init_state
    
    def add_state_with_transitions(self,
                                connect_before: States, 
                                connect_after: States,
                                symbol: str = "") -> State:
        """
        Add given state to the state set. 
        Connect the state by epsilon-transitions to given states.
        """
        new_state = _unoccuring_state(self.states)
        self.transitions[new_state] = {}

        for state in connect_before:
            self.transitions.add_edge(symbol, state, new_state)
        for state in connect_after:
            self.transitions.add_edge(symbol, new_state, state)
        
        self.states.add(new_state)
        return new_state
    
    def print(self, print_ports: bool = False):
        print("states:", pfrozenset.convert_rec(self.states))
        print("input_symbols:", self.input_symbols)
        print("transitions:", end=" ")
        pprint(pfrozenset.convert_rec(self.transitions))
        print("initial_states:", pfrozenset.convert_rec(self.initial_states))
        print("final_states:", pfrozenset.convert_rec(self.final_states))

    def to_nfa(self) -> NFA:
        """
        Parse ExtendedNFA to NFA.
        """
        self_copy = self.copy()
        initial_state = self_copy.single_initial_state()

        return NFA(states=self_copy.states,
                input_symbols=self_copy.input_symbols,
                transitions=self_copy.transitions.get_dict(),
                initial_state=initial_state,
                final_states=self_copy.final_states)
    
    def to_dfa(self) -> DFA:
        if not self.is_deterministic():
            raise ValueError("NFA is not deterministic, cannot be converted to DFA.")
        
        transitions = self.transitions.get_deterministic_dict()
        initial_state = next(iter(self.initial_states))

        return DFA(states=self.states,
                input_symbols=self.input_symbols,
                transitions=transitions,
                initial_state=initial_state,
                final_states=self.final_states)
    
    def to_nx_graph(self, weighted: bool = True) -> nx.DiGraph:
        G = nx.DiGraph()

        for state in self.states:
            G.add_node(state)
        
        for src, symbol, dst in self.transitions.iterate_all():
            if not G.has_edge(src, dst):
                G.add_edge(src, dst, weight=1)
            elif weighted:
                G[src][dst]['weight'] += 1

        return G
    
    def to_nx_graph_labeled(self) -> nx.DiGraph:
        G = nx.DiGraph()

        for state in self.states:
            G.add_node(state, initial=(state in self.initial_states), accepting=(state in self.final_states))
        
        for src, symbol, dst in self.transitions.iterate_all():
            G.add_edge(src, dst, label=symbol)

        return G

    def show_diagram(self, path: str) -> None:
        nfa = self.to_nfa()
        nfa.show_diagram(path=path)

    def is_complete(self):
        """
        Check if every state has at least one transition under every symbol.
        """
        for state in self.states:
            if state not in self.transitions:
                return False
            
            for symbol in self.input_symbols:
                if symbol not in self.transitions[state] or not self.transitions[state][symbol]:
                    return False
            
        return True
    
    def completify(self) -> State | None:
        """
        Make the transition function complete.
        Returns the new sink state.
        """
        sink = _unoccuring_state(self.states, prefix="sink")
        needed_sink = False

        for state in self.states:
            if state not in self.transitions:
                self.transitions[state] = {}

            for symbol in self.input_symbols:
                if symbol not in self.transitions[state] or not self.transitions[state][symbol]:
                    self.transitions[state][symbol] = {sink}
                    needed_sink = True

        if needed_sink:
            self.states.add(sink)
            for symbol in self.input_symbols:
                self.transitions.add_edge(symbol, sink, sink)

            self.sink = sink
            return sink
        
        return None
    
    def get_sink(self) -> State | None:
        """
        Return the sink (orr add a new state if it does not exist).
        """
        if self.sink is None:
            sink = _unoccuring_state(self.states, prefix="sink")
            self.states.add(sink)
            for symbol in self.input_symbols:
                self.transitions.add_edge(symbol, sink, sink)

            self.sink = sink
            
        return self.sink

        # for state in self.states - self.final_states:
        #     is_sink = True
        #     for symbol in self.input_symbols:
        #         if symbol not in self.transitions[state] or \
        #                 self.transitions[state][symbol] != {state}:
        #             is_sink = False
            
        #     if is_sink:
        #         return state
                
        # return None
    
    def remove_unreachable(self, 
                           must_remain: States = set(), 
                           init_reachable: States | None = None) \
                                -> 'ExtendedNFA':
        """
        Remove unreachable states.
        'must_remain' contains states that must not be removed.
        """
        if init_reachable is None:
            reachable = _compute_reachable_states(self.transitions, 
                                        self.initial_states | must_remain)
        else:
            init_reachable &= self.states
            reachable = _compute_reachable_states(self.transitions, init_reachable)
        
        states, transitions, final_states = _remove_states(self.transitions, 
                                                        self.final_states, 
                                                        reachable)
        
        initial_states = self.initial_states & reachable

        return ExtendedNFA(states=states,
                        input_symbols=self.input_symbols,
                        transitions=transitions,
                        initial_states=initial_states,
                        final_states=final_states)
    
    def remove_dead(self, starting_nondead: set[States] | None = None) -> 'ExtendedNFA':
        """
        Return an equivalent ExtendedNFA containing 
        only the states whose language is not empty.
        """
        nondead = _compute_backward_reachable(self, starting_nondead)

        initial_states = self.initial_states & nondead

        states, transitions, final_states = _remove_states(self.transitions, 
                                                        self.final_states, 
                                                        nondead)
        
        return ExtendedNFA(states=states,
                        input_symbols=self.input_symbols,
                        transitions=transitions,
                        initial_states=initial_states,
                        final_states=final_states)
    
    def remove_dead_and_unreachable(self, init_reachable: States | None = None,
                                    init_nondead: States | None = None) -> 'ExtendedNFA':
        if init_reachable is None:
            reachable = _compute_reachable_states(self.transitions, self.initial_states)
        else:
            reachable = _compute_reachable_states(self.transitions, init_reachable)

        nondead = _compute_backward_reachable(self, init_nondead)
        
        new_states, new_transitions, new_final = _remove_states(self.transitions, self.final_states, self.states & reachable & nondead)

        return ExtendedNFA(states=new_states,
                           input_symbols=self.input_symbols,
                           transitions=new_transitions,
                           initial_states=self.initial_states & new_states,
                           final_states=new_final)
    

    def update(self, other: 'ExtendedNFA'):
        # check if state sets are disjoint
        if not self.states.isdisjoint(other.states):
            raise ValueError("States are not disjoint, cannot union NFAs.")
        
        self.states.update(other.states)
        self.input_symbols.update(other.input_symbols)
        self.initial_states.update(other.initial_states)
        self.final_states.update(other.final_states)


    def update_with_rename(self, other: 'ExtendedNFA', state_counter: Iterator[State]) -> dict[State, State]:
        state_map = {state: next(state_counter) for state in other.states}
        if not self.states.isdisjoint(state_map.values()):
            raise ValueError("State counter did not produce disjoint states, cannot union NFAs.")

        self.states.update(state_map.values())
        
        for state in other.final_states:
            self.final_states.add(state_map[state])

        for state in other.initial_states:
            self.initial_states.add(state_map[state])

        for src, symbol, dst in other.transitions.iterate_all():
            self.transitions.add_edge(symbol, state_map[src], state_map[dst])

        return state_map

    
    def get_subautomaton_transitions(self, 
                     states: States,
                     input_symbols: Alphabet | None = None,
                     transitions: NFATransitions | None = None,
                     get_out_transitions: bool = False) \
                        -> tuple[Alphabet, NFATransitions, NFATransitions | None]:
        """
        Returns input symbols, transitions inside of a subautomaton, 
        and transitions out of the subautomaton.
        """
        if input_symbols is not None \
                and transitions is not None \
                and not get_out_transitions:
            return input_symbols, transitions, None
        
        out_transitions = NFATransitions()
        transitions = transitions if transitions is not None else NFATransitions()
        input_symbols = input_symbols if input_symbols is not None else set()

        for src, symbol, dst in self.transitions.iterate_all():
            if dst in states:
                transitions.add_edge(src, symbol, dst)
            elif get_out_transitions:
                out_transitions.add_edge(src, symbol, dst)

        if get_out_transitions:
            return input_symbols, transitions, out_transitions
        return input_symbols, transitions, None
    

    def get_subautomaton(self, 
                        states: States,
                        input_symbols: Alphabet | None = None,
                        transitions: NFATransitions | None = None,
                        initial_states: States | None = None,
                        final_states: States | None = None,
                        get_out_transitions: bool = False) \
                            -> tuple['ExtendedNFA', NFATransitions | None]:
        """
        Returns a subautomaton for a given set of states and 
        transitions outside of the subautomaton. 
        """
        input_symbols, transitions, out_transitions = \
            self.get_subautomaton_transitions(states, input_symbols,
                                        transitions, get_out_transitions)

        if initial_states is None:
            initial_states = self.initial_states & states
        if final_states is None:
            final_states = states & self.final_states

        subaut = ExtendedNFA(states=states,
                            input_symbols=input_symbols,
                            transitions=transitions,
                            initial_states=initial_states,
                            final_states=final_states)

        return subaut, out_transitions

    
    @classmethod
    def from_nfa(cls, nfa: NFA) -> 'ExtendedNFA':
        """
        Parse NFA to ExtendedNFA.
        """
        nfa_no_eps = eliminate_lambda(nfa)
        return ExtendedNFA(states=set(nfa_no_eps.states),
                        input_symbols=set(nfa_no_eps.input_symbols),
                        transitions=NFATransitions.from_frozen(nfa_no_eps.transitions),
                        initial_states={nfa_no_eps.initial_state},
                        final_states=set(nfa_no_eps.final_states))
    
    @classmethod
    def from_dfa(cls, dfa: DFA, 
                 state_map: dict[State, State] | None = None, 
                 given_input_symbols: Alphabet | None = None) -> 'ExtendedNFA':
        """
        Parse DFA to ExtendedNFA.
        If the state_map and given_input_symbols are specified, only the symbols and states
        present in them are included.
        """
        input_symbols = set(dfa.input_symbols) if given_input_symbols is None else given_input_symbols

        if state_map is None:
            nfa_trans = NFATransitions.from_det_frozen(dfa.transitions)
            return ExtendedNFA(states=set(dfa.states),
                            input_symbols=input_symbols,
                            transitions=nfa_trans,
                            initial_states={dfa.initial_state},
                            final_states=set(dfa.final_states))
        
        nfa_trans = NFATransitions()
        for src in dfa.transitions:
            for symbol, dst in dfa.transitions[src].items():
                if symbol in input_symbols and src in state_map and dst in state_map:
                    nfa_trans.add_edge(symbol, state_map[src], state_map[dst])

        return ExtendedNFA(states=set(state_map.values()),
                           input_symbols=input_symbols,
                           transitions=nfa_trans,
                           initial_states={ state_map[dfa.initial_state] },
                           final_states={ state_map[state] for state in dfa.final_states })
    
    @classmethod
    def create_empty(cls, input_symbols: set[Symbol]) -> 'ExtendedNFA':
        """
        Return an ExtendedNFA recognizing an empty language.
        """
        state = 0
        transitions = NFATransitions()
        for symbol in input_symbols:
            transitions.add_edge(symbol, state, state)
        
        return ExtendedNFA(states={state},
                        input_symbols=input_symbols,
                        transitions=transitions,
                        initial_states={state},
                        final_states=set())
    

class PortsNFA:
    def __init__(self, nfa: ExtendedNFA, 
                 in_port_sets: PortSets | None = None, 
                 out_port_sets: PortSets | None = None):
        self.nfa: ExtendedNFA = nfa
        self.in_port_sets: PortSets = self._preprocess_port_sets(in_port_sets)
        self.out_port_sets: PortSets = self._preprocess_port_sets(out_port_sets)

    def _preprocess_port_sets(self, port_sets: PortSets | None) -> PortSets:
        if port_sets is None:
            return []
        
        port_sets = list(set(port_sets))
        if port_sets == [{}]:
            return []
        return port_sets

    def exchange_port_sets(self, new_in: PortSets | None = None, 
                           new_out: PortSets | None = None) -> 'PortsNFA':
        new_in_ports = new_in if new_in is not None else self.in_port_sets
        new_out_ports = new_out if new_out is not None else self.out_port_sets
        return PortsNFA(self.nfa, new_in_ports, new_out_ports)
    
    def reverse(self) -> 'PortsNFA':
        return PortsNFA(self.nfa.reverse(), self.out_port_sets.copy(), self.in_port_sets.copy())
    
    def copy(self) -> 'PortsNFA':
        return PortsNFA(self.nfa.copy(), self.in_port_sets.copy(), self.out_port_sets.copy())

    def get_subautomaton(self, 
                        states: States,
                        input_symbols: Alphabet | None = None,
                        transitions: NFATransitions | None = None,
                        initial_states: States | None = None,
                        final_states: States | None = None,
                        in_port_sets: PortSets | None = None,
                        out_port_sets: PortSets | None = None,
                        get_out_transitions: bool = False) \
                            -> tuple['PortsNFA', NFATransitions | None]:
        """
        Returns a subautomaton for a given set of states and 
        transitions outside of the subautomaton. 
        """
        ext_subaut, out_transitions = self.nfa.get_subautomaton(states, input_symbols, transitions,
                                               initial_states, final_states, get_out_transitions)
        
        if in_port_sets is None:
            in_port_sets = [port_set & states for port_set in self.in_port_sets]
        if out_port_sets is None:
            out_port_sets = [port_set & states for port_set in self.out_port_sets]
        
        subaut = PortsNFA(ext_subaut, in_port_sets, out_port_sets)
        return subaut, out_transitions
    
    def has_incoming(self) -> bool:
        """
        Return if there are any initial states or entry ports.
        """
        return bool(self.nfa.initial_states) or bool(self.in_port_sets)
    
    def has_outcoming(self) -> bool:
        """
        Return if there are any final states or exit ports.
        """
        return bool(self.nfa.final_states) or bool(self.out_port_sets)


class MappedPortsNFA:
    def __init__(self, nfa: ExtendedNFA, in_ports_map: PortSetsMap, out_ports_map: PortSetsMap):
        self.nfa: ExtendedNFA = nfa
        self.in_map: PortSetsMap = in_ports_map
        self.out_map: PortSetsMap = out_ports_map


    def get_sliced_self(self, new_nfa: ExtendedNFA) -> 'MappedPortsNFA':
        in_map = {key: val & new_nfa.states for key, val in self.in_map.items()}
        out_map = {key: val & new_nfa.states for key, val in self.out_map.items()}
        
        return MappedPortsNFA(new_nfa, in_map, out_map)


    def remove_dead(self) -> 'MappedPortsNFA':
        """
        Return an equivalent MappedPortsNFA containing 
        only the states whose language is not empty. 
        Update group_map values.
        """
        nondead = set().union(*self.out_map.values()) | self.nfa.final_states

        without_dead = self.nfa.remove_dead(starting_nondead=nondead)
        return self.get_sliced_self(without_dead)
        
    
    def remove_dead_and_unreachable(self) -> 'MappedPortsNFA':
        """
        Return an equivalent MappedPortsNFA containing 
        only the states whose language is not empty. 
        Update group_map values.
        """
        nondead = set().union(*self.out_map.values()) | self.nfa.final_states
        reachable = set().union(*self.in_map.values()) | self.nfa.initial_states

        result_nfa = self.nfa.remove_dead_and_unreachable(reachable, nondead)
        
        return self.get_sliced_self(result_nfa)


class PortComponent:
    def __init__(self, component: MappedPortsNFA,
                       between_transitions: NFATransitions,
                       wanted_in: PortSets,
                       wanted_out: PortSets,
                       orig_has_init_states: bool):
        self.component = component
        self.between_transitions = between_transitions
        self.wanted_in_ports = wanted_in
        self.wanted_out_ports = wanted_out
        self.orig_has_init_states = orig_has_init_states


def _add_edge_dfa(transitions: DFATransDict, 
                 symbol: str, src: State, dst: State):
    """
    Add new edge to given transition dict. For DFAs.
    """
    if src not in transitions:
        transitions[src] = {}
    transitions[src][symbol] = dst


def _compute_lambda_closures(states, transitions):
        """
        Computes a dictionary of the lambda closures for this NFA, where each
        key is the state name and the value is the lambda closure for that
        corresponding state. This dictionary is cached for the lifetime of the
        instance, and is available via the 'lambda_closures' attribute.

        The lambda closure of a state q is the set containing q, along with
        every state that can be reached from q by following only lambda
        transitions.
        """
        lambda_graph = nx.DiGraph()
        lambda_graph.add_nodes_from(states)
        lambda_graph.add_edges_from([
            (start_state, end_state)
            for start_state, transition in transitions.items()
            for char, end_states in transition.items()
            if char == ''
            for end_state in end_states
        ])

        return {
            state: frozenset(nx.descendants(lambda_graph, state) | {state})
            for state in states
        }


def _eliminate_lambda(aut: NFA, lambda_closures):
    """
    Internal helper function for eliminate lambda. Doesn't create final NFA.
    """
    # Create new transitions and final states for running this algorithm
    new_transitions = {
        state: {
            symbol: set(dest)
            for symbol, dest in paths.items()
        }
        for state, paths in aut.transitions.items()
    }
    new_final_states = set(aut.final_states)

    for state in aut.states:
        lambda_enclosure = lambda_closures[state]
        for input_symbol in aut.input_symbols:
            next_current_states = set(aut._get_next_current_states(lambda_enclosure, input_symbol))

            # Don't do anything if no new current states
            if next_current_states:
                state_transition_dict = new_transitions.setdefault(state, dict())

                if input_symbol in state_transition_dict:
                    state_transition_dict[input_symbol].update(next_current_states)
                else:
                    state_transition_dict[input_symbol] = next_current_states

        if (new_final_states & lambda_enclosure):
            new_final_states.add(state)

        if state in new_transitions:
            new_transitions[state].pop('', None)

    # Remove unreachable states
    reachable_states = NFA._compute_reachable_states(aut.initial_state, new_transitions)
    reachable_final_states = reachable_states & new_final_states

    for state in aut.states - reachable_states:
        new_transitions.pop(state, None)

    return reachable_states, new_transitions, reachable_final_states


def eliminate_lambda(aut: NFA):
    """
    Remove epsilon transitions from the given NFA.
    Function is copied from the Automata library and modified to 
    preserve the states' languages. 
    """
    lambda_closures = _compute_lambda_closures(aut.states, aut.transitions)
    reachable_states, new_transitions, reachable_final_states = _eliminate_lambda(aut, lambda_closures)

    return NFA(
        states=reachable_states,
        input_symbols=aut.input_symbols,
        transitions=new_transitions,
        initial_state=aut.initial_state,
        final_states=reachable_final_states
    )

def _remove_states(transitions: NFATransitions, 
                  final_states: States, 
                  keep: States) \
                    -> tuple[States, NFATransitions, States]:
    """
    Remove states that should not be kept.
    Returns tuple (states, transitions, final states).
    """
    new_transitions = NFATransitions()

    for state in keep:
        new_transitions[state] = {}
        for ap, dests in transitions.get(state, {}).items():
            new_transitions[state][ap] = dests & keep

    final_states = final_states & keep

    return keep.copy(), new_transitions, final_states


def _compute_backward_reachable(nfa: ExtendedNFA, want_to_reach: States | None = None):
    if want_to_reach is None:
        # automatically computing nondead states
        want_to_reach = nfa.final_states
    
    rev_transitions = nfa.transitions.reverse()
    backward_reachable = _compute_reachable_states(rev_transitions, want_to_reach)
    return backward_reachable


def _compute_reachable_states(transitions: NFATransitions, start_with: States):
    reachable = start_with.copy()
    stack = list(start_with)

    while stack:
        new_state = stack.pop()
        for succ in transitions.iterate_dests_only(new_state):
            if succ not in reachable:
                reachable.add(succ)
                stack.append(succ)

    return reachable


def _unoccuring_state(states: States, prefix: str = "") -> str:
    """
    Return a string name of a state not occuring in a given state set.
    """
    if prefix not in states and prefix != "":
        return prefix

    num = 0
    while prefix + str(num) in states or (prefix == "" and num in states):
        num += 1
    return prefix + str(num)


def _unoccuring_int_state(states: States) -> int:
    """
    Return a string name of a state not occuring in a given state set.
    """
    num = 0
    while str(num) in states or num in states:
        num += 1
    return num
