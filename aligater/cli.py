import argparse, os, sys
from aligater.types.options import COMPLEMENT_MODE_LIST, PORT_CONNECT_MODE_LIST, \
    PORT_PARTITION_MODE_LIST, ARGUMENTS, POWERSET_DIRECTION_LIST, help, translate_from_string, \
    ComplementationOptions, PortOptions, PortConnectMode, PortPartitionMode, ComplementMode, \
    PowersetDirection
from aligater.main import complement_from_file

def get_enum_help(enum):
    help_str = ""
    for instance in enum:
        help_str += f"{instance.get_command()}    {instance.get_description()}\n"
    return help_str


def print_help():
    help_path = os.path.join(os.path.dirname(__file__), "help.txt")
    with open(help_path, "r") as file:
        print(file.read())


def parse_arguments():
    parser = argparse.ArgumentParser(
    formatter_class=argparse.RawTextHelpFormatter,
    description="AliGater can complement nondeterministic finite automata.")
    
    # Mandatory arguments
    parser.add_argument("inputpath", help="Path to the input file (.mata, .ba, .dot)")

    parser.add_argument('algorithm',
                        choices=[m.command for m in COMPLEMENT_MODE_LIST], 
                        help="complementation algorithm to be used:\n" + help(COMPLEMENT_MODE_LIST))

    for arg in ARGUMENTS:
        if arg.options is None:
            parser.add_argument(arg.short_arg_name, arg.long_arg_name,
                            action='store_true', help=arg.description)
        else:
            parser.add_argument(arg.short_arg_name, arg.long_arg_name,
                                choices=[opt.command for opt in arg.options], 
                                default=arg.options[0].command,
                                help=arg.description + help(arg.options))


    # Optional arguments
    parser.add_argument('-o', '--outputpath',
                        help='path to the output file, export in .dot')
    parser.add_argument('-v', '--verbose',
                        action='store_true', 
                        help='print the complementation statistics to stdout')
    parser.add_argument('-s', '--short-verbose',
                        action='store_true', 
                        help='print only the complement size to stdout')
    parser.add_argument('-x', '--xmlpath',
                        help='path to a .xml file to export statistics')
    parser.add_argument('-p', '--pic',
                        help='path to a .png file to export a picture of the complement')
    parser.add_argument('-l', '--lookahead',
                        type=int,
                        default=10,
                        help='lookahead parameter for RABIT')
    
    return parser.parse_args()


def validate_args(mode: ComplementMode, options: ComplementationOptions, port_options: PortOptions):
    if mode in [ComplementMode.POWERSET, ComplementMode.FORWARD_FIRST, ComplementMode.REVERSE_FIRST, 
                ComplementMode.HEURISTIC, ComplementMode.RESIDUAL]:
        if options.rabit_during:
            print("Warning: the option 'minimize with RABIT during' is active, but is not used in the chosen mode.", file=sys.stderr)

        # if options.powerset_direction:
        #     print("Warning: the option 'powerset direction' is active, but is not used in the chosen mode.", file=sys.stderr)

    if mode in [ComplementMode.FORWARD_FIRST, ComplementMode.REVERSE_FIRST, ComplementMode.HEURISTIC]:
        if options.reverse:
            print("Warning: the option 'reverse' is active, but is not used in the chosen mode.", file=sys.stderr)
        if options.dfa_min:
            print("Warning: the option 'dfa_min' is active, but is not used in the chosen mode.", file=sys.stderr)

def main():

    args = parse_arguments()

    input_path = args.inputpath
    xml_path = args.xmlpath
    save_path = args.outputpath
    pic_path = args.pic
    gate_symbols = None
    mode = translate_from_string(COMPLEMENT_MODE_LIST, args.algorithm)
    port_connect_mode = translate_from_string(PORT_CONNECT_MODE_LIST, args.port_connect)
    port_partition_mode = translate_from_string(PORT_PARTITION_MODE_LIST, args.port_partition)
    powerset_direction = translate_from_string(POWERSET_DIRECTION_LIST, args.pow_direction)
    verbose = args.verbose
    short_verbose = args.short_verbose

    assert isinstance(port_connect_mode, PortConnectMode)
    assert isinstance(port_partition_mode, PortPartitionMode)
    assert isinstance(mode, ComplementMode)
    assert isinstance(powerset_direction, PowersetDirection)

    options = ComplementationOptions(
        rabit_during=args.min_rabit_during,
        dfa_min=args.min_dfa,
        rabit_min_after=args.min_rabit_after,
        simulation_min=args.min_simulation,
        residual_min=args.min_residual,
        residual_min_after=args.min_residual_after,
        validate=args.validate,
        reverse=args.reverse,
        powerset_direction=powerset_direction,
        lookahead=args.lookahead)

    port_options = PortOptions(
        connect_mode=port_connect_mode, 
        partition_method=port_partition_mode,
        rename=True)
    
    if input_path is None:
        print(f"Input path must be provided.", file=sys.stderr)
        return

    if mode is None:
        print("Complementation algorithm must be provided.", file=sys.stderr)
        return

    if not os.path.exists(input_path):
        print(f"Input path {input_path} does not exist.", file=sys.stderr)
        return
    
    validate_args(mode, options, port_options)

    complement_from_file(input_path, 
                         mode, 
                         port_options,
                         options, 
                         gate_symbols=gate_symbols, 
                         xml_path=xml_path, 
                         save_path=save_path, 
                         pic_path=pic_path, 
                         verbose=verbose,
                         short_verbose=short_verbose)
    

if __name__ == "__main__":
    main()