from aligater.types import *
from aligater.utils import generic as utils, powerset_complement as cmpl
from aligater.complement.complement import Complement
from aligater.data_export import DataExporter
from aligater.partitioning.gate_finder import GateCompPair

# Implementation of the complementation algorithms for gate NFAs

class GateComplement(Complement):
    def __init__(self, nfa: PortsNFA, 
                 gate_pair: GateCompPair,
                 options: ComplementationOptions,
                 data: DataExporter | None = None):
        Complement.__init__(self, nfa, options, data)
        self.gate_pair = gate_pair
        self.state_counter = utils.str_counter(0)

        if (self.ports_nfa.in_port_sets or self.ports_nfa.out_port_sets):
            raise NotImplementedError()


    def add_loops(self, transitions: NFATransitions, state: State, symbols: set[str]):
        for symbol in symbols:
            transitions.add_edge(symbol, state, state)


    def gate_complement_basic(self) -> ExtendedNFA:
        """
        Performs the complement by parts on a gate partition.
        Based on the type of given a gate NFA, complements both components and composes them together.
        Does not create products.
        """

        # determine alphabets in respect to which the complements should be made
        self.determine_alphabets()

        # compute co-A_1 and co-A_2 with their port mappings
        comp1, is_dfa1 = cmpl.complement_with_tracking(self.gate_pair.ports_aut1, self.options)
        comp2, is_dfa2 = cmpl.complement_with_tracking(self.gate_pair.ports_aut2, self.options)

        # special state 'sink'
        accepting_sink = "sink"

        # initialize the sets for the resulting complement
        comp = ExtendedNFA(
            states = {accepting_sink},
            input_symbols = comp1.nfa.input_symbols | comp2.nfa.input_symbols | self.gate_pair.gate_symbols,
            transitions = NFATransitions(),
            initial_states = set(),
            final_states = {accepting_sink}
        )

        # incorporate co-A_1 and co-A_2 into the resulting complement
        state_map1_comp, state_map2_comp = self.add_subcomplements(comp1, comp2, comp)

        # if the gate NFA satisfies the 'equal' condition, add special state "t"
        if self.gate_pair.type.is_equal():
            self.add_state_t(comp1, comp2, comp, state_map2_comp)

        # if the 'disjoint individual' condition holds, add the whole subautomaton A_1
        elif self.gate_pair.type.is_disjoint_ind():
            self.add_subautomaton_A1_ind(comp2, comp, state_map2_comp)

        elif self.gate_pair.type.is_disjoint_grp():
            self.add_subautomaton_A1_grp(comp2, comp, state_map2_comp)

        self.add_edges_to_sink(comp1, comp2, accepting_sink, comp.transitions, state_map1_comp)

        # add intersection of co-A_1 and co-A_2
        if self.gate_pair.type.is_product_both():
            self.add_coA1_and_coA2_intersection(comp)
        
        comp = comp.remove_dead_and_unreachable()

        if self.data is not None:
            self.data.write_A1_det(is_dfa1)
            self.data.write_A2_det(is_dfa2)
        return comp


    def add_coA1_and_coA2_intersection(self, comp: ExtendedNFA):
        partial_comp_1, _ = cmpl.best_complement_with_tracking(
                self.gate_pair.ports_aut1.exchange_port_sets([], []), self.options)

        self.gate_pair.ports_aut2.nfa.input_symbols = self.gate_pair.input_symbols - self.gate_pair.gate_symbols
        partial_comp_2, _ = cmpl.best_complement_with_tracking(
                self.gate_pair.ports_aut2.exchange_port_sets([], []), self.options)
        self.gate_pair.ports_aut2.nfa.input_symbols = self.gate_pair.input_symbols

        product = self.intersection(partial_comp_1.nfa, partial_comp_2.nfa)

        state_map_product = comp.update_with_rename(product, self.state_counter)


    def add_edges_to_sink(self, comp1: MappedPortsNFA, comp2: MappedPortsNFA, 
                          accepting_sink: State, transitions: NFATransitions, 
                          state_map1_comp: dict[State, State]):
        self.add_loops(transitions, accepting_sink, comp2.nfa.input_symbols)

        # add edges from complementary exit ports in co-A_1 to 'sink' under gate symbols
        for symbol, port_set in self.gate_pair.symbols_out_group1.items():
            for comp_exit_port in comp1.out_map[frozenset(port_set)]:
                transitions.add_edge(symbol, state_map1_comp[comp_exit_port], accepting_sink)


    def add_subautomaton_A1_ind(self,
                            comp2: MappedPortsNFA, 
                            comp: ExtendedNFA, 
                            state_map2_comp: dict[State, State]):
        """
        Connect subautomaton A1, treat gates individually.
        """
        final_states = self.gate_pair.ports_aut1.nfa.final_states
        self.gate_pair.ports_aut1.nfa.final_states = set()
        state_map1 = comp.update_with_rename(self.gate_pair.ports_aut1.nfa, self.state_counter)
        self.gate_pair.ports_aut1.nfa.final_states = final_states

        # add edges from A_1 to complementary entry ports in co-A_2
        for exit_port, symbol, entry_port in self.gate_pair.gates.iterate_all():
            for comp_entry_port in comp2.in_map[frozenset([entry_port])]:
                comp.transitions.add_edge(symbol, state_map1[exit_port], state_map2_comp[comp_entry_port])


    def add_subautomaton_A1_grp(self,
                            comp2: MappedPortsNFA, 
                            comp: ExtendedNFA, 
                            state_map2_comp: dict[State, State]):
        """
        Connect subautomaton A1, treat gates in groups by their origin ports.
        """
        final_states = self.gate_pair.ports_aut1.nfa.final_states
        self.gate_pair.ports_aut1.nfa.final_states = set()
        state_map1 = comp.update_with_rename(self.gate_pair.ports_aut1.nfa, self.state_counter)
        self.gate_pair.ports_aut1.nfa.final_states = final_states

        # add edges from A_1 to complementary entry ports in co-A_2
        for exit_port in self.gate_pair.gates:
            for symbol in self.gate_pair.gates[exit_port]:

                entry_port_set = frozenset(self.gate_pair.gates.iterate(exit_port, symbol))
                for comp_entry_port in comp2.in_map[entry_port_set]:
                    comp.transitions.add_edge(symbol, state_map1[exit_port], state_map2_comp[comp_entry_port])


    def add_state_t(self, comp1: MappedPortsNFA, comp2: MappedPortsNFA, 
                    comp: ExtendedNFA, state_map2_comp: dict[State, State]):
        state_t = "t"
        comp.states.add(state_t)
        comp.initial_states.add(state_t)
        self.add_loops(comp.transitions, state_t, comp1.nfa.input_symbols)

            # add edges from s to complementary entry ports in co-A_2
        for symbol, port_set in self.gate_pair.symbols_in_group2.items():
            for comp_entry_port in comp2.in_map[frozenset(port_set)]:
                comp.transitions.add_edge(symbol, state_t, state_map2_comp[comp_entry_port])


    def add_subcomplements(self, comp1: MappedPortsNFA, comp2: MappedPortsNFA, comp: ExtendedNFA):
        if self.gate_pair.type.is_first():
            comp2.nfa.initial_states = set()
        
        elif self.gate_pair.type.is_product_both():
            comp1.nfa.final_states = set()
            comp2.nfa.initial_states = set()

        else:
            comp1.nfa.final_states = set()

        state_map1_comp = comp.update_with_rename(comp1.nfa, self.state_counter)
        state_map2_comp = comp.update_with_rename(comp2.nfa, self.state_counter)
        return state_map1_comp,state_map2_comp


    def determine_alphabets(self):
        if self.gate_pair.type.is_first() or self.gate_pair.type.is_product_both():
            self.gate_pair.ports_aut1.nfa.input_symbols = self.gate_pair.input_symbols - self.gate_pair.gate_symbols
            self.gate_pair.ports_aut2.nfa.input_symbols = self.gate_pair.input_symbols
        else:
            self.gate_pair.ports_aut1.nfa.input_symbols = self.gate_pair.input_symbols 
            self.gate_pair.ports_aut2.nfa.input_symbols = self.gate_pair.input_symbols - self.gate_pair.gate_symbols


    def intersection(self, nfa1: ExtendedNFA, nfa2: ExtendedNFA) -> ExtendedNFA:
        """
        Intersect two Extended NFAs.
        """
        nfa1_conversed = nfa1.to_nfa()
        nfa2_conversed = nfa2.to_nfa()

        intersection = nfa1_conversed & nfa2_conversed
        ext_intersection = ExtendedNFA.from_nfa(intersection)
        ext_intersection = utils.rename_states(ext_intersection)
        return ext_intersection


    def complement(self) -> PortsNFA:
        """
        Perform gate complement on a given gate partition according to its type.
        """
        if self.gate_pair.type.is_none():
            raise ValueError(f"Cannot complement a gate_pair of type '{self.gate_pair.type}'")

        basic_complement = self.gate_complement_basic()
        
        # intersection is needed
        if not self.gate_pair.type.is_basic() and not self.gate_pair.type.is_product_both():

            # must intersect with the second component
            if self.gate_pair.type.is_first():
                intersect_with, _ = cmpl.best_complement_with_tracking(
                    self.gate_pair.ports_aut2.exchange_port_sets([], []), self.options)
            # must intersect with the first component
            else:
                intersect_with, _ = cmpl.best_complement_with_tracking(
                    self.gate_pair.ports_aut1.exchange_port_sets([], []), self.options)
            
            product = self.intersection(basic_complement, intersect_with.nfa)
            return PortsNFA(product)

        return PortsNFA(basic_complement)