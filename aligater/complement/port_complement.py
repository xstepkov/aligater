from typing import Any
from aligater.types import *
from aligater.utils import generic as utils, minimizing as min, powerset_complement as powc
from aligater.data_export import DataExporter
from aligater.complement.complement import Complement
from collections.abc import Iterator
from collections import deque
from time import time_ns

class TupleState:
    """
    Class for states of the port complement
    """
    def __init__(self, state1: State, set2: frozenset[State], in_second_comp: bool):
        self.first: State = state1
        self.second: frozenset[State] = set2
        self.in_second_comp: bool = in_second_comp

    def __key(self):
        return (self.first, self.second, self.in_second_comp)
    
    def simpl_key(self):
        return (self.first, self.second)

    def __hash__(self):
        return hash(self.__key())

    def __eq__(self, other: object) -> bool:
        if isinstance(other, TupleState):
            return self.__key() == other.__key()
        return False
    
    def __repr__(self):
        return pfrozenset.convert_rec(self.simpl_key()).__repr__()

    def __str__(self):
        return pfrozenset.convert_rec(self.simpl_key()).__str__()

    def is_accepting(self, first_final: States, second_final: States):
        if self.first not in first_final:
            if not self.second:
                return not self.in_second_comp
            return all([s in second_final for s in self.second])
        return False

CacheTransT = dict[tuple[frozenset[State], Symbol], list[States]]
CacheInterTransT = dict[tuple[State, Symbol], list[States]]

class PortCompConnector:
    def __init__(self, 
                orig_nfa: PortsNFA,
                first: MappedPortsNFA, 
                second_complement: MappedPortsNFA,
                between_transitions: NFATransitions,
                options: PortOptions,
                wanted_in_port_sets: PortSets,
                wanted_out_port_sets: PortSets,
                second_orig_has_init_states: bool,
                iteration: int = -1):
        """
        Connect two components for port complement.
        Gets underlying original NFA, first determinized component,
        second complemented component, options, and sets of ports that 
        should be contained in the resulting complement.
        """
        self.orig_nfa: PortsNFA = orig_nfa
        self.first: MappedPortsNFA = first
        self.second: MappedPortsNFA = second_complement
        self.options: PortOptions = options
        self.wanted_in_port_sets: PortSets = wanted_in_port_sets
        self.wanted_out_port_sets: PortSets = wanted_out_port_sets
        self.second_orig_has_init_states = second_orig_has_init_states

        self.second_in_ports = set().union(*second_complement.in_map.keys())
        self.between_transitions = between_transitions

        # precompute dead states in the second component for optimization
        self.dead_in_second = self.second.nfa.states - utils.compute_backward_reachable(self.second.nfa)

        # precompute from which states in first only final_states are reachable
        # states from which there is a nonfinal state reachable
        reachable_nonfinal_state = utils.compute_backward_reachable(self.first.nfa, 
                                            self.first.nfa.states - self.first.nfa.final_states)
        self.all_final_in_first = self.first.nfa.states - reachable_nonfinal_state

        self.iteration = iteration

        # cached multi-product transition function
        self.cached_trans: CacheTransT = {}
        # cached intergalactic multi-product transition function
        self.cached_inter_trans: CacheInterTransT = {}


    def multi_product_transition_function(self, 
                                          src_set: frozenset[State], 
                                          symbol: str) -> list[States]:
        """
        Compute multi-product transition function (transitions inside the second component).
        Into the multi-product transition function belong all combinations
        of states to which the automaton can transition from the states in src_set.
        (for each src state one dst state in one destination set)
        """
        if (src_set, symbol) in self.cached_trans:
            return self.cached_trans[(src_set, symbol)]

        dst_sets: list[States] = [set()]

        for src in src_set:
            updated_dst_sets = []

            # if one element of src_set has no successors, the whole src_set has no successors
            if src not in self.second.nfa.transitions \
                    or symbol not in self.second.nfa.transitions[src] \
                    or not self.second.nfa.transitions[src][symbol]:
                self.cached_trans[(src_set, symbol)] = []
                return []

            for dst_set in dst_sets:
                for dst in self.second.nfa.transitions[src][symbol]:
                    updated_dst_sets.append(dst_set | {dst})

            dst_sets = updated_dst_sets

        self.cached_trans[(src_set, symbol)] = dst_sets
        return dst_sets


    def inter_multi_product_transition_function(self, src: State, symbol: str) -> list[States]:
        """
        Compute intergalactic multi-product transition function 
        (transitions from the first component to the mapped ports of the second component).
        """
        if (src, symbol) in self.cached_inter_trans:
            return self.cached_inter_trans[(src, symbol)]

        # src has no transitions to the second component
        if not self.is_exit_port_under_symbol(src, symbol):
            result = [set()]
            self.cached_inter_trans[(src, symbol)] = result
            return result
        
        if self.options.connect_mode == PortConnectMode.INDIVIDUAL:
            # handle each entry port of the second component individually
            dst_sets = [set()]
            for dst in self.between_transitions[src][symbol]:
                updated_dst_sets = []

                # if a state does not map to anything, 
                # do not allow any transitions
                dst_index = frozenset([dst])
                if not self.second.in_map[dst_index]:
                    result = []
                    self.cached_inter_trans[(src, symbol)] = result
                    return result

                for dst_set in dst_sets:
                    for complement_dst in self.second.in_map[dst_index]:
                        updated_dst_sets.append(dst_set | {complement_dst})
                
                dst_sets = updated_dst_sets

        elif self.options.connect_mode == PortConnectMode.GROUPED:
            dst_sets = []
            entry_ports_2 = frozenset(self.between_transitions[src][symbol])

            for mapped_port in self.second.in_map[entry_ports_2]:
                dst_sets.append({mapped_port})

        self.cached_inter_trans[(src, symbol)] = dst_sets
        return dst_sets


    def remove_little_brothers(self, state_sets: list[set[State]] | list[frozenset[State]]) \
            -> list[frozenset[State]]:
        """
        Remove states sets that are supersets of another set.
        """
        state_sets.sort(key=lambda s: len(s), reverse=True)
        filtered_sets: list[frozenset[State]] = []
        
        for i in range(len(state_sets)):
            larger_set = state_sets[i]

            is_superset = False
            for j in range(i + 1, len(state_sets)):
                smaller_set = state_sets[j]
                if larger_set.issuperset(smaller_set):
                    is_superset = True
                    break

            if not is_superset:
                filtered_sets.append(frozenset(larger_set))     

        return filtered_sets


    def is_exit_port_under_symbol(self, src: State, symbol: Symbol) -> bool:
        return src in self.between_transitions \
            and symbol in self.between_transitions[src] \
            and len(self.between_transitions[src][symbol]) != 0


    def transitioned_to_second_component(self, in_second_comp: bool, src: State, symbol: Symbol) -> bool:
        return in_second_comp or self.is_exit_port_under_symbol(src, symbol)


    def next_states(self, tuple_state: TupleState, symbol: str) \
                        -> Iterator[TupleState]:
        """
        Compute the transition function for a given new tuple state.
        """
        src1 = tuple_state.first
        src_set2 = tuple_state.second

        assert self.first.nfa.transitions.has_transition_under_symbol(src1, symbol)
        
        # successor of src1 in first
        # first is deterministic - there is only one successor
        dst1 = next(iter(self.first.nfa.transitions[src1][symbol]))
            
        in_second_comp = self.transitioned_to_second_component(tuple_state.in_second_comp,
                                                        src1, symbol)
        
        # continue only in the first component if we do not transition to the second one
        if not in_second_comp and not src_set2:
            yield TupleState(dst1, frozenset(), in_second_comp)

        # compute transitions in the second component
        trans_in_second = self.multi_product_transition_function(src_set2, symbol)

        # if some run in the second component dies, do not follow this path
        if not trans_in_second:
            return

        state_sets = []
        for set_S in trans_in_second:
            for set_T in self.inter_multi_product_transition_function(src1, symbol):
                state_sets.append(frozenset(set_S | set_T))
        
        for state_set in self.remove_little_brothers(state_sets):
            yield TupleState(dst1, state_set, in_second_comp)


    def continue_from_state(self, state: TupleState):
        """
        Determine if it makes sense to expand from this state.
        Do not expand if all states reachable from this state will be dead.
        """
        # there is definitely no reachable final state if:
        # all states reachable from the first state in new component are final
        # or
        # some state in the state set of the second component is dead
        if state.first in self.all_final_in_first:
            return False

        for state2 in state.second:
            if state2 in self.dead_in_second:
                return False
            
        return True


    def connect_component(self) -> MappedPortsNFA:
        """
        Connect new component to the partial complement.
        Suppose the new component is ready to be connected (is determinized and complete).
        Between_transitions map states from new component to states in original NFA 
        corresponding to states in the conntect_to component.
        """

        # initialize sets for the cross-product automaton
        input_symbols: set[Symbol] = self.orig_nfa.nfa.input_symbols

        initial_states = self.get_initial_states()

        new_in_map = self.get_in_map()

        # initialize queue
        states, transitions = self.get_states_and_transitions(initial_states, new_in_map)

        # construct final states
        final_states = self.get_final_states(states)

        new_out_map = self.get_out_map(states)

        result_nfa = ExtendedNFA(states=states,
                        input_symbols=input_symbols,
                        transitions=transitions,
                        initial_states=initial_states,
                        final_states=final_states)
        
        ports_nfa = MappedPortsNFA(result_nfa, new_in_map, new_out_map)

        # rename states
        if self.options.rename:
            renamed_nfa = utils.rename_states_mapped(ports_nfa)
            return renamed_nfa
        return ports_nfa
    
    def get_out_map(self, states) -> PortSetsMap:
        if self.wanted_out_port_sets:
            raise NotImplementedError()
        return {}

    def get_final_states(self, states) -> States:
        final_states: set[TupleState] = set()
        for tuple_state in states:
            if tuple_state.is_accepting(self.first.nfa.final_states, self.second.nfa.final_states):
                final_states.add(tuple_state)
        return final_states

    def get_states_and_transitions(self, initial_states, new_in_ports_map) -> tuple[States, NFATransitions]:
        states: set[TupleState] = set()
        transitions: NFATransitions = NFATransitions()

        queue: deque[TupleState] = deque()
        for init_state in initial_states | set().union(*new_in_ports_map.values()):
            queue.append(init_state)
            states.add(init_state)

        # construct new states and transitions
        while queue:
            state = queue.popleft()

            if not self.continue_from_state(state):
                continue

            for symbol in self.first.nfa.input_symbols:
                for next_state in self.next_states(state, symbol):
                        if next_state not in states:
                            queue.append(next_state)
                            states.add(next_state)
                        
                        transitions.add_edge(symbol, state, next_state)

        return states, transitions

    def get_in_map(self):
        new_in_ports_map: PortSetsMap = {}
        for port_set in self.wanted_in_port_sets:
            new_in_ports_map[port_set] = set()

            port_set1 = port_set - self.second_in_ports
            port_set2 = port_set & self.second_in_ports

            # suppose first is deterministic -> has exactly one entry port
            mapped_port1 = next(iter(self.first.in_map[port_set1]))
            if not port_set2:
                new_mapped_port = TupleState(mapped_port1, frozenset(), False)
                new_in_ports_map[port_set].add(new_mapped_port)

            else:
                for mapped_port2 in self.second.in_map[port_set2]:
                    new_mapped_port = TupleState(mapped_port1, frozenset({mapped_port2}), True)
                    new_in_ports_map[port_set].add(new_mapped_port)
        return new_in_ports_map

    def get_initial_states(self) \
            -> set[TupleState]:
        initial_states = set()

        # if the original second component has no initial states, we do not
        # need to start any copy of the second complement
        if not self.second_orig_has_init_states:
            for init_state1 in self.first.nfa.initial_states:
                initial_states.add(TupleState(init_state1, frozenset(), False))
            return initial_states

        # if the original second component has initial states
        for init_state1 in self.first.nfa.initial_states:
            for init_state2 in self.second.nfa.initial_states:
                new_init_state = TupleState(init_state1, frozenset({init_state2}), True)
                initial_states.add(new_init_state)
        
        return initial_states
    

class PortComplement(Complement):
    def __init__(self, orig_nfa: PortsNFA, 
                 to_connect: list[PortComponent],
                 last_component: PortsNFA,
                 options: ComplementationOptions,
                 port_options: PortOptions,
                 data: DataExporter | None = None):
        Complement.__init__(self, orig_nfa, options, data)
        self.to_connect = to_connect
        self.last_component = last_component
        self.port_options = port_options


    def minimize_component_rabit(self, component: MappedPortsNFA) \
                            -> MappedPortsNFA:

        min_component = min.rabit_minimize_with_tracking(component, self.options)

        if min_component is None:
            if self.data is not None:
                self.data.increase_rabit_error_count()
            return component
        
        return min_component
    
    def minimize_component_simulation(self, component: MappedPortsNFA) -> MappedPortsNFA:
        return min.mata_simulation_minimize_with_tracking(component)
    
    def minimize_component_residual(self, component: MappedPortsNFA) -> MappedPortsNFA:
        return min.mata_residual_minimize_with_tracking(component, Direction.FORWARD)


    def complement(self):
        """
        Complement NFA with port complement.
        """
        if self.options.rabit_during:
            self.port_options.rename = True

        self.to_connect.reverse()

        # complement the last component
        partial_complement, forw = powc.complement_with_tracking(self.last_component, self.options)

        second_has_init_states = len(self.last_component.nfa.initial_states) > 0

        rabit_time = 0
        iteration = 1

        # iteratively add previous components
        for comp_to_connect in self.to_connect:
            
            connector = PortCompConnector(self.ports_nfa, 
                                          comp_to_connect.component, 
                                          partial_complement,
                                          comp_to_connect.between_transitions, 
                                          self.port_options, 
                                          comp_to_connect.wanted_in_ports, 
                                          comp_to_connect.wanted_out_ports,
                                          second_has_init_states,
                                          iteration) 
            partial_complement = connector.connect_component()
            second_has_init_states = second_has_init_states or comp_to_connect.orig_has_init_states

            partial_complement = partial_complement.remove_dead()

            if self.options.simulation_min:
                partial_complement = self.minimize_component_simulation(partial_complement)

            if self.options.residual_min:
                partial_complement = self.minimize_component_residual(partial_complement)

            if self.options.rabit_during:
                rabit_start_time = time_ns()
                partial_complement = self.minimize_component_rabit(partial_complement)
                rabit_time += time_ns() - rabit_start_time

            iteration += 1
        
        if self.data is not None:
            self.data.write_comp_size_useless(len(partial_complement.nfa.states))
            self.data.write_last_comp_forw(forw)
        
        partial_complement = partial_complement.remove_dead_and_unreachable()
        # partial_complement.completify()

        if self.options.rabit_during and self.data is not None:
            self.data.write_rabit_min_during_time(rabit_time)

        return partial_complement
