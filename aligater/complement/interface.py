from aligater.types import *
from aligater.complement.gate_complement import GateComplement
from aligater.complement.port_complement import PortComplement
import aligater.partitioning as part
from aligater.utils import mata_handler, parser, heuristics
import aligater.data_export as export
from time import time_ns
import os

# Complementing functions for each method that are supposed to be called from `main.py`


def complement_forward_powerset_mata(nfa: ExtendedNFA | mata_handler.MataNfaContainer, 
                                     options: ComplementationOptions,
                                     data: export.DataExporter | None = None) \
                                     -> mata_handler.MataNfaContainer:
    """
    Run forward powerset complement and get statistics.
    """
    if isinstance(nfa, ExtendedNFA):
        nfa = mata_handler.ext_nfa_to_mata_nfa(nfa)

    start_time = time_ns()
    forward_complement = mata_handler.powerset_complement_mata(nfa, minify=options.dfa_min)
    complement_time = time_ns() - start_time

    if data is not None:
        data.write_comp_size(forward_complement.get_num_states())
        data.write_complement_time(complement_time)

    return forward_complement


def complement_reverse_powerset_mata(nfa: ExtendedNFA | mata_handler.MataNfaContainer,
                                     options: ComplementationOptions,
                                     data: export.DataExporter | None = None,) \
                                        -> mata_handler.MataNfaContainer:
    """
    Run reverse powerset complement and get statistics. 
    """
    if isinstance(nfa, ExtendedNFA):
        nfa = mata_handler.ext_nfa_to_mata_nfa(nfa)

    start_time = time_ns()
    rev_complement = mata_handler.reverse_complement_mata(nfa, minify=options.dfa_min,
                                                          reduce_simulation=options.simulation_min)
    complement_time = time_ns() - start_time

    if data is not None:
        data.write_comp_size(rev_complement.get_num_states())
        data.write_complement_time(complement_time)

    return rev_complement


def complement_forward_reverse_first_mata(nfa: ExtendedNFA | mata_handler.MataNfaContainer,
                                          forward_first: bool,
                                          output_path: str | None,
                                          data: export.DataExporter | None = None,) \
                                        -> mata_handler.MataNfaContainer:
    """
    Run forward-first or reverse-first complement.
    If `output_path` is specified, the intermediate result is stored to the path.
    If `data` is not None, statistics are stored in data.
    """
    if isinstance(nfa, ExtendedNFA):
        nfa = mata_handler.ext_nfa_to_mata_nfa(nfa)

    start_time = time_ns()

    complements = mata_handler.forward_first_complement(nfa) \
        if forward_first \
        else mata_handler.reverse_first_complement(nfa)
    
    first_comp = next(complements)
    first_complement_time = time_ns() - start_time

    if output_path is not None:
        path, ext = os.path.splitext(output_path)
        output_path = f"{path}_intermediate_result{ext}"
        parser.save_to_file(mata_handler.mata_nfa_to_ext_nfa(first_comp), output_path)

    second_comp = next(complements)
    second_complement_time = time_ns() - start_time

    first_is_smaller = first_comp.get_num_states() < second_comp.get_num_states()

    if data is not None:
        data.write_comp_size(first_comp.get_num_states())
        data.write_comp_min_size(second_comp.get_num_states())
        data.write_complement_time(first_complement_time)
        data.write_min_complement_time(second_complement_time)
        data.write_first_smaller(first_is_smaller)

    return first_comp if first_is_smaller else second_comp


def complement_residual_mata(nfa: ExtendedNFA | mata_handler.MataNfaContainer,
                             forward: bool,
                             data: export.DataExporter | None = None,) \
                                -> mata_handler.MataNfaContainer:
    """
    Run complementation with residual reduction.
    If `forward` is True, run `residual(rev(co(det(rev(A)))))`. 
    If `forward` is False, run `rev(residual(rev(co(det(A)))))`.
    If `data` is not None, statistics are stored in data.
    """
    if isinstance(nfa, ExtendedNFA):
        nfa = mata_handler.ext_nfa_to_mata_nfa(nfa)

    start_time = time_ns()

    complement = mata_handler.residual_complement_forward(nfa) \
        if forward else mata_handler.residual_complement_reverse(nfa)
    
    complement_time = time_ns() - start_time

    if data is not None:
        data.write_comp_size(complement.get_num_states())
        data.write_complement_time(complement_time)

    return complement


def complement_heuristic_forw_rev_first(nfa: ExtendedNFA | mata_handler.MataNfaContainer,
                                        heuristic: Heuristic,
                                        output_path: str | None,
                                        data: export.DataExporter | None = None) \
                                            -> mata_handler.MataNfaContainer:
    """
    Run forward-first or reverse-first based on the result of the given heuristic.
    If `output_path` is specified, the intermediate result is stored to the path.
    If `data` is not None, statistics are stored in data.
    """
    ext_nfa = mata_handler.make_nfa_ext(nfa)

    run_forward_first = heuristics.decide_direction(ext_nfa, heuristic) == Direction.FORWARD

    if data is not None:
        data.write_direction(run_forward_first)

    return complement_forward_reverse_first_mata(nfa, run_forward_first, output_path, data)



def complement_gate(ext_nfa: ExtendedNFA,
                    options: ComplementationOptions,
                    number_of_partitions: int = 1,
                    gate_symbols: set[str] | None  = None,
                    data: export.DataExporter | None = None) \
                        -> ExtendedNFA | None:
    """
    Run gate complement and get statistics.
    """
    ports_nfa = PortsNFA(ext_nfa)
    partitioner = part.GatePartitioner(ports_nfa, number_of_partitions)

    if gate_symbols is not None:
        start_time = time_ns()
        gate_pairs = partitioner.partition_by_gate_symbols(gate_symbols)
        partition_time = time_ns() - start_time
    else:
        start_time = time_ns()
        gate_pairs = partitioner.partition()
        partition_time = time_ns() - start_time

    if data is not None:
        data.write_partition_time(partition_time)

    if not gate_pairs:
        if data is not None:
            data.write_num_gates("N/A")
            data.write_A1_size("N/A")
            data.write_A2_size("N/A")
            data.write_comp_size("N/A")
            data.write_A1_det("N/A")
            data.write_A2_det("N/A")
            data.write_gate_type("NONE")
            data.write_complement_time("N/A")
        return None

    gate_comps: list[PortsNFA] = []
    complement_times: list[int] = []

    for gate_pair in gate_pairs:
        complement_start_time = time_ns()
        complementer = GateComplement(PortsNFA(ext_nfa), gate_pair, options, data)
        gate_comp = complementer.complement()
        complement_time = time_ns() - complement_start_time

        gate_comps.append(gate_comp)
        complement_times.append(complement_time)

    best_idx = gate_comps.index(min(gate_comps, key=lambda comp: len(comp.nfa.states)))

    if data is not None:
        data.write_num_gates(len(gate_pairs[best_idx].gates))
        data.write_A1_size(len(gate_pairs[best_idx].ports_aut1.nfa.states))
        data.write_A2_size(len(gate_pairs[best_idx].ports_aut2.nfa.states))
        data.write_comp_size(len(gate_comps[best_idx].nfa.states))
        data.write_gate_type(gate_pairs[best_idx].type.to_string())
        data.write_complement_time(complement_time)

    return gate_comp.nfa


def complement_port(ext_nfa: ExtendedNFA,
                    options: ComplementationOptions,
                    port_options: PortOptions,
                    data: export.DataExporter | None = None) \
                        -> ExtendedNFA:
    """
    Run port complement and get statistics.
    """
    start_time = time_ns()
    ports_nfa = PortsNFA(ext_nfa)
    partitioner = part.PortPartitioner(ports_nfa, options, port_options, data=data)
    partition, last_component = partitioner.partition()

    complementer = PortComplement(ports_nfa, partition, last_component, options, port_options, data=data)
    comp = complementer.complement()
    comp_time = time_ns() - start_time

    if data is not None:
        data.write_comp_size(len(comp.nfa.states))
        data.write_complement_time(comp_time)

    return comp.nfa
