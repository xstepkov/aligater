from aligater.types import *
from aligater.data_export.export_types import DataExporter
from abc import ABC, abstractmethod

class Complement(ABC):
    def __init__(self, ports_nfa: PortsNFA, 
                 options: ComplementationOptions,
                 data: DataExporter | None = None):
        self.ports_nfa: PortsNFA = ports_nfa
        self.options: ComplementationOptions = options
        self.data: DataExporter | None = data

    @abstractmethod
    def complement(self) -> PortsNFA:
        pass
    