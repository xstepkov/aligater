import os
from random import choice

def create_bubble(sentence: str):
    top_char = "‾"
    bottom_char = "_"

    top = f" /‾‾{len(sentence) * top_char}‾‾\\"
    middle = f"|   {sentence}   |"
    bottom = f" \\_  {(len(sentence) - 2) * bottom_char}___/"

    return top + "\n" + middle + "\n" + bottom


def choose_compliment():
    path = os.path.join(os.path.dirname(__file__), "compliments.txt")

    with open(path, "r") as file:
        lines = file.readlines()
        return choice(lines).strip()
    

def generate_compliment():
    sentence = choose_compliment()
    bubble = create_bubble(sentence)


    compliment = bubble + """
   \\/
           .-._   _ _ _ _ _ _ _ _
.-''-.__.-'00  '-' ' ' ' ' ' ' ' '-.
'.___ '    .   .--_'-' '-' '-' _'-' '._
 V: V 'vv-'   '_   '.       .'  _..' '.'.
   '=.____.=_.--'   :_.__.__:_   '.   : :
           (((____.-'        '-.  /   : :
 snd                         (((-'\\ .' /
                           _____..'  .'
                          '-._____.-'
"""
    return compliment

if __name__ == "__main__":
    print(generate_compliment())