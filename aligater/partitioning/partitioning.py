from aligater.types import *
from aligater.partitioning.sccs import scc_labeling
from enum import Enum
from abc import ABC
import networkx as nx
import numpy as np

class CannotPartitionException(Exception):
    pass


class NondetHandling(Enum):
    # throw exception whenever a nondeterministic SCC is found
    EXCEPTION = 0

    # leave every nondeterministic SCC as a standalone component
    DONT_JOIN = 1

    # connect all nondeterministic SCCs to the back of the current component
    JOIN = 2

    # connect only the first encountered nondeterministic SCC to the current component,
    # the rest are left as standalone
    JOIN_FIRST_ONLY = 3


class ComponentInfo:
    def __init__(self, ports_nfa: PortsNFA, 
                 exit_ports: States,
                 succ_components: set[int],
                 is_deterministic: bool):
        self.ports_nfa: PortsNFA = ports_nfa
        self.exit_ports: States = exit_ports
        self.succ_components: set[int] = succ_components
        self.is_deterministic = is_deterministic

    def copy(self) -> 'ComponentInfo':
        return ComponentInfo(ports_nfa=self.ports_nfa.copy(),
                             exit_ports=self.exit_ports.copy(),
                             succ_components=self.succ_components.copy(),
                             is_deterministic=self.is_deterministic)
    

class Partitioner(ABC):
    def __init__(self, orig_nfa: PortsNFA,
                 state_partition: list[set[State]],
                 alphabet: set[Symbol] | None = None):
        self.orig_nfa = orig_nfa
        self.state_partition = state_partition
        self.alphabet = alphabet

    def join_sccs(self, sccs: list[set[State]], indices_to_join: list[list[int]]) \
                    -> list[set[State]]:
        new_partition = []
        for indices in indices_to_join:
            if len(indices) > 0:
                joined_states: set[State] = set().union(*[sccs[i] for i in indices])
                new_partition.append(joined_states)
        
        return new_partition
    
    def partition(self) -> list[ComponentInfo]:
        raise NotImplementedError()


class DeterministicPartitioner(Partitioner):
    def __init__(self, orig_nfa: PortsNFA,
                 state_partition: list[set[State]],
                 alphabet: set[Symbol] | None = None,
                 handle_nondet_scc: NondetHandling = NondetHandling.EXCEPTION):
        Partitioner.__init__(self, orig_nfa, state_partition, alphabet)
        self.handle_nondet_scc = handle_nondet_scc

    def check_det_ports_between(self, comp1: ComponentInfo, comp2: ComponentInfo):
        state_union = comp1.ports_nfa.nfa.states | comp2.ports_nfa.nfa.states
        deterministic_union = True

        for port in comp1.exit_ports:

            for symbol in self.orig_nfa.nfa.input_symbols:
                count_dests_in = 0

                for dst in self.orig_nfa.nfa.transitions.iterate(port, symbol):

                    if dst in state_union:
                        count_dests_in += 1
                
                if count_dests_in > 1:
                    deterministic_union = False

        return deterministic_union


    def try_join_components(self,
                            first: ComponentInfo, 
                            second: ComponentInfo) -> bool:
        """
        Merges second component into first, if they should be joined.
        Returns True if joined. 
        """
        deterministic_ports = self.check_det_ports_between(first, second)

        is_deterministic = first.is_deterministic \
                        and second.is_deterministic \
                        and deterministic_ports \
                        and len(first.ports_nfa.nfa.initial_states) + len(second.ports_nfa.nfa.initial_states) <= 1
        
        # check if we should join components or not
        if deterministic_ports \
                and len(first.ports_nfa.nfa.initial_states) + len(second.ports_nfa.nfa.initial_states) <= 1:
            
            if not second.is_deterministic:
                if self.handle_nondet_scc == NondetHandling.EXCEPTION:
                    raise CannotPartitionException("Nondeterministic SCC found.")
                elif self.handle_nondet_scc == NondetHandling.DONT_JOIN:
                    return is_deterministic
                
            if not first.is_deterministic and self.handle_nondet_scc == NondetHandling.JOIN_FIRST_ONLY:
                # joined one nondeterministic SCC already, not allowed to join more SCCs
                return is_deterministic

        else:
            return is_deterministic

        first.ports_nfa.nfa.states.update(second.ports_nfa.nfa.states)
        first.exit_ports.update(second.exit_ports)

        first.is_deterministic = is_deterministic

        return is_deterministic


    def mark_succs_ignore(self, ignore: list[bool], start: int, comp_infos: list[ComponentInfo]):
        stack = [start]

        while stack:
            index = stack.pop()
            ignore[index] = True

            for succ in comp_infos[index].succ_components:
                if not ignore[succ]:
                    stack.append(succ)


    def find_largest_deterministic_part(self,
                                        comp_infos: list[ComponentInfo], 
                                        ignore: list[bool]) \
                                            -> list[int]:
        start_with = min([i for i in range(len(ignore)) if not ignore[i]])
        current_component = comp_infos[start_with].copy()

        if not current_component.is_deterministic:
            self.mark_succs_ignore(ignore, start_with, comp_infos)
            if self.handle_nondet_scc == NondetHandling.EXCEPTION:
                raise CannotPartitionException("Nondeterministic SCC found.")
            return [start_with]

        used_components: list[int] = [start_with]
        ignore[start_with] = True

        for i in range(start_with + 1, len(comp_infos)):
            if ignore[i]:
                continue

            comp = comp_infos[i]
            joined = self.try_join_components(current_component, comp)

            if joined:
                # component is still deterministic after joining
                used_components.append(i)
                ignore[i] = True
            else:
                # found nondeterminism, cannot join components
                # must ignore all successor components
                self.mark_succs_ignore(ignore, i, comp_infos)

        return used_components
    
    def get_deterministic_components(self) -> list[set[State]]:
        comp_infos = partition_into_comp_infos(self.orig_nfa, self.state_partition, self.alphabet)

        used_sccs = set()
        deterministic_clusters = []

        while len(used_sccs) != len(self.state_partition): # while some sccs are unprocessed
            ignore = [(True if i in used_sccs else False) for i in range(len(comp_infos))]
            new_det_cluster = self.find_largest_deterministic_part(comp_infos, ignore)

            if new_det_cluster == []: # some scc is not deterministic -- cannot be partitioned
                if self.handle_nondet_scc == NondetHandling.EXCEPTION:
                    raise CannotPartitionException("Cannot partition into deterministic components.")

            used_sccs.update(new_det_cluster)
            deterministic_clusters.append(new_det_cluster)

        deterministic_partition = self.join_sccs(self.state_partition, deterministic_clusters)
        return deterministic_partition
    
    def find_largest_rev_det_tail(self) -> list[int]:
        """
        Join SCCs from the back into a largest possible reverse-deterministic subautomaton.
        """
        rev_orig_nfa = self.orig_nfa.reverse()
        rev_partition = self.state_partition.copy()
        rev_partition.reverse()

        rev_comp_infos = partition_into_comp_infos(rev_orig_nfa, rev_partition, self.alphabet)

        self.handle_nondet_scc = NondetHandling.EXCEPTION
        ignore_sccs = [False for _ in range(len(rev_comp_infos))]
        most_used_indices = []

        while not all(ignore_sccs):
            try:
                rev_partitioner = DeterministicPartitioner(rev_orig_nfa, rev_partition, self.alphabet)
                used_indices = rev_partitioner.find_largest_deterministic_part(rev_comp_infos, ignore_sccs)

            except CannotPartitionException:
                # the first component is nondeterministic
                used_indices = []

            # try to find the largest possible reverse deterministic component
            if len(used_indices) > len(most_used_indices):
                most_used_indices = used_indices

        used_indices_in_orig_order = [len(self.state_partition) - i - 1 for i in used_indices]
        return used_indices_in_orig_order


class LastRevPartitioner(DeterministicPartitioner):
    def get_last_rev_partition(self):
        rev_indices = self.find_largest_rev_det_tail()
        other_indices = [i for i in range(len(self.state_partition)) if i not in rev_indices]

        updated_partition = self.join_sccs(self.state_partition, [other_indices, rev_indices])
        return updated_partition


    def partition(self) -> list[ComponentInfo]:
        updated_partition = self.get_last_rev_partition()

        updated_comp_infos = partition_into_comp_infos(self.orig_nfa, updated_partition, self.alphabet)    
        return updated_comp_infos


class DetCompPartitioner(DeterministicPartitioner):
    def partition_deterministic_comps(self) -> list[ComponentInfo]:
        deterministic_partition = self.get_deterministic_components()
        updated_comp_infos = partition_into_comp_infos(self.orig_nfa, deterministic_partition, self.alphabet)

        return updated_comp_infos


    def partition(self):
        self.handle_nondet_scc = NondetHandling.JOIN
        return self.partition_deterministic_comps()



class DetCompLastRevPartitioner(DeterministicPartitioner):
    def partition(self) -> list[ComponentInfo]:
        rev_indices = self.find_largest_rev_det_tail()
        last_rev_comp_list = self.join_sccs(self.state_partition, [rev_indices])
        if not last_rev_comp_list:
            last_rev_comp_list = [set()]
        last_rev_component = last_rev_comp_list[0]

        self.handle_nondet_scc = NondetHandling.JOIN
        deterministic_partition = self.get_deterministic_components()
        
        final_partition = [comp - last_rev_component for comp in deterministic_partition] + last_rev_comp_list
        final_partition = [comp for comp in final_partition if comp]

        updated_comp_infos = partition_into_comp_infos(self.orig_nfa, final_partition, self.alphabet)
        return updated_comp_infos


class MinCutPartitioner(Partitioner):
    def increase_capacity(self, graph: nx.DiGraph, starting_val: int, 
                          decrease_by: int, 
                          start_in: int, forward: bool = True):
        sources = {start_in} # starting in the first node
        for capacity in range(starting_val, 0, -decrease_by):
            new_sources = set()

            for src in sources:
                next_nodes = graph.successors(src) if forward else graph.predecessors(src)
                for succ in next_nodes:

                    first, second = (src, succ) if forward else (succ, src)
                    graph[first][second]['capacity'] += capacity

                    if succ not in sources:
                        new_sources.add(succ)

            sources = new_sources


    def add_backward_edges(self, graph: nx.DiGraph, new_edge_capacity: int):
        backward_reachable = np.zeros((len(graph.nodes()),)*2)

        for node in graph.nodes():
            s = list(nx.ancestors(graph, node))
            backward_reachable[node, s] = 1

        for node1, node2 in np.ndindex(backward_reachable.shape):
            if backward_reachable[node1, node2] == 1:
                graph.add_edge(node1, node2, capacity=new_edge_capacity)


    def create_nx_graph(self) -> nx.DiGraph:
        state_labels = scc_labeling(self.state_partition)

        graph = nx.DiGraph()
        for node in range(len(self.state_partition)):
            graph.add_node(node)
        
        # add edges into graph for each transition between components
        for src, symbol, dst in self.orig_nfa.nfa.transitions.iterate_all():
            src_node = state_labels[src]
            dst_node = state_labels[dst]

            if src_node != dst_node:
                if not graph.has_edge(src_node, dst_node):
                    graph.add_edge(src_node, dst_node, capacity=1)
                else:
                    graph[src_node][dst_node]['capacity'] += 1
        
        return graph


    def create_source(self, graph: nx.DiGraph, new_edge_capacity: int) -> int:
        without_pred = []

        for node in graph.nodes:
            # if iterator is empty
            if sum(1 for _ in graph.predecessors(node)) == 0:
                without_pred.append(node)

        if len(without_pred) == 0:
            return 0 # assign the first scc in the topological ordar as source
        if len(without_pred) == 1:
            return without_pred[0]
        
        new_source = max(graph.nodes) + 1
        graph.add_node(new_source)
        for node in without_pred:
            graph.add_edge(new_source, node, capacity=new_edge_capacity)

        return new_source


    def print_graph(self, graph):
        for from_state, to_state, data in graph.edges(data=True):
            symbol = data['capacity']
            print(f"Transition: {from_state} --({symbol})--> {to_state}")


    def get_min_cut_components(self) \
                                    -> list[States]:
        graph = self.create_nx_graph()

        new_edge_capacity = sum([graph[u][v]['capacity'] for u, v in graph.edges]) * 1000

        source = self.create_source(graph, new_edge_capacity)
        sink = len(self.state_partition) - 1

        # artificially increase capacity of edges from the start and end
        # to force the min cut to be more balanced
        starting_incr_val = len(self.state_partition) // 5
        self.increase_capacity(graph, starting_val=starting_incr_val, decrease_by=1, start_in=source, forward=True)
        self.increase_capacity(graph, starting_val=starting_incr_val, decrease_by=1, start_in=sink, forward=False)

        self.add_backward_edges(graph, new_edge_capacity)

        cut_val, new_partition = nx.minimum_cut(graph, source, sink, capacity="capacity")

        new_state_partition = []
        for part in new_partition:
            state_cluster = set()

            for scc in part:
                if 0 <= scc and scc < len(self.state_partition):
                    for state in self.state_partition[scc]:
                        state_cluster.add(state)
            
            new_state_partition.append(state_cluster)

        return new_state_partition
        

    def partition(self) -> list[ComponentInfo]:
        new_state_partition = self.get_min_cut_components()

        return partition_into_comp_infos(self.orig_nfa, new_state_partition, self.alphabet)


class BasicPartitioner(Partitioner):
    def subaut_transitions_and_ports(self,
                                    comp_infos: list[ComponentInfo],
                                    labels: dict[State, int],
                                    ports_only: bool = False):
        """
        Partitions transitions into subautomata of orig_nfa.
        Computes entry and exit ports of the subautomata.
        """

        for src in self.orig_nfa.nfa.states:
            for symbol, dst in self.orig_nfa.nfa.transitions.iterate_sym(src):

                src_comp = comp_infos[labels[src]]

                if labels[src] == labels[dst]: # both in same component
                    if not ports_only:
                        src_comp.ports_nfa.nfa.transitions.add_edge(symbol, src, dst)
                        src_comp.ports_nfa.nfa.input_symbols.add(symbol)

                else: # transition between components
                    src_comp.exit_ports.add(src)
                    src_comp.succ_components.add(labels[dst])


    def update_deterministic(self, comp_infos: list[ComponentInfo]):
        for ci in comp_infos:
            ci.is_deterministic = ci.ports_nfa.nfa.is_deterministic()


    def partition(self) -> list[ComponentInfo]:
        """
        Computes subautomata for a given state partition of a given NFA.
        Returns a list of subautomata ordered accordingly to the partition.
        """
        labels = scc_labeling(self.state_partition)
        scc_infos: list[ComponentInfo] = []

        # determine states of each component
        for state_set in self.state_partition:
            states = state_set.copy()
            final_states = states & self.orig_nfa.nfa.final_states
            initial_states = states & self.orig_nfa.nfa.initial_states
            input_symbols = self.alphabet.copy() if self.alphabet is not None else set()

            new_subaut = ExtendedNFA(states=states,
                                    input_symbols=input_symbols,
                                    transitions=NFATransitions(),
                                    initial_states=initial_states,
                                    final_states=final_states)
            
            scc_infos.append(ComponentInfo(PortsNFA(new_subaut), set(), set(), False))

        # fill in transitions and port information
        self.subaut_transitions_and_ports(scc_infos, labels)
        self.update_deterministic(scc_infos)
        return scc_infos
    

def get_partitioner(orig_nfa: PortsNFA, 
                    state_partition: list[set[State]],
                    alphabet: set[Symbol] | None = None,
                    method: PortPartitionMode = PortPartitionMode.SCC) -> Partitioner:
    if method == PortPartitionMode.SCC:
        return BasicPartitioner(orig_nfa, state_partition, alphabet)
    elif method == PortPartitionMode.LREV:
        return LastRevPartitioner(orig_nfa, state_partition, alphabet)
    elif method == PortPartitionMode.DETERMINISTIC:
        return DetCompPartitioner(orig_nfa, state_partition, alphabet)
    elif method == PortPartitionMode.MIN_CUT:
        return MinCutPartitioner(orig_nfa, state_partition, alphabet)
    elif method == PortPartitionMode.DET_LREV:
        return DetCompLastRevPartitioner(orig_nfa, state_partition, alphabet)
    assert False


def partition_into_comp_infos(orig_nfa: PortsNFA, 
                      state_partition: list[set[State]],
                      alphabet: set[Symbol] | None = None,
                      method: PortPartitionMode = PortPartitionMode.SCC) \
                        -> list[ComponentInfo]:
    partitioner = get_partitioner(orig_nfa, state_partition, alphabet=alphabet, method=method)
    return partitioner.partition()
