from aligater.types import *
from aligater.partitioning.partitioning import partition_into_comp_infos
from aligater.partitioning.sccs import find_sccs, scc_labeling
from aligater.data_export import DataExporter
from aligater.utils import minimizing as min, powerset_complement as powc


class PortPartitioner:
    def __init__(self, ports_nfa: PortsNFA,
                       options: ComplementationOptions,
                       port_options: PortOptions,
                       data: DataExporter | None = None):
        self.ports_nfa = ports_nfa
        self.options = options
        self.port_options = port_options
        self.data = data


    def add_port_set_to_successors(self, port_set: frozenset[State], from_label: int, entry: bool):
        """
        Add intersections of the given port set to each successor component.
        """
        for label in range(from_label, len(self.comp_infos)):
            comp_states = self.comp_infos[label].ports_nfa.nfa.states

            # add the whole current port set to the between-component
            if entry:
                self.wanted_in_port_sets[label].add(port_set)
            else:
                self.wanted_out_port_sets[label].add(port_set)

            # add the states from each component
            in_this_comp = frozenset(port_set & comp_states)
            add_to = self.comp_infos[label].ports_nfa.in_port_sets if entry else \
                        self.comp_infos[label].ports_nfa.out_port_sets
            
            if in_this_comp not in add_to:
                add_to.append(in_this_comp)
        
            # update port_set for later components
            port_set = port_set - comp_states


    def add_internal_ports_from_state_indiv(self, src: State):
        for dst in self.ports_nfa.nfa.transitions.iterate_dests_only(src):
            src_label = self.labels[src]
            dst_label = self.labels[dst]

            assert src_label <= dst_label
            dst_frozenset = frozenset([dst])

            # add each entry port to each between-component on the way
            for label in range(src_label + 1, dst_label + 1):
                self.wanted_in_port_sets[label].add(dst_frozenset)

            # add destination entry ports to their origin component
            if dst_label != self.labels[src] and \
                    dst_frozenset not in self.comp_infos[dst_label].ports_nfa.in_port_sets:
                self.comp_infos[dst_label].ports_nfa.in_port_sets.append(frozenset([dst]))


    def add_internal_ports_indiv(self) -> None:
        """
        Add entry ports to other components enforced by the INDIVIDUAL connect method.
        """
        for component in self.comp_infos:
            for src in component.exit_ports:
                self.add_internal_ports_from_state_indiv(src)


    def add_internal_ports_grouped_from(self, between_transitions: NFATransitions, current_label: int) -> None:
        """
        Add internal ports enforced by the transitions from a previous component.
        """
        for dests_dict in between_transitions.values():
            for port_set in dests_dict.values():

                if self.port_options.connect_mode == PortConnectMode.GROUPED:
                    self.add_port_set_to_successors(frozenset(port_set), current_label, entry=True)


    def add_exit_ports(self) -> None:
        """
        Add individual exit ports to each component's out_port_sets.
        """
        for component in self.comp_infos:
            for port in component.exit_ports:
                component.ports_nfa.out_port_sets.append(frozenset([port]))
    

    def add_external_ports(self) -> None:
        """
        Add port sets enforced by the underlying NFA to all components.
        """
        for port_set in self.ports_nfa.in_port_sets:
            self.add_port_set_to_successors(port_set, 0, entry=True)

        for port_set in self.ports_nfa.out_port_sets:
            self.add_port_set_to_successors(port_set, 0, entry=False)


    def add_empty_entry_sets(self) -> None:
        empty = frozenset()
        for i in range(1, len(self.comp_infos)):
            if empty not in self.comp_infos[i].ports_nfa.in_port_sets:
                self.comp_infos[i].ports_nfa.in_port_sets.append(empty)

            if empty not in self.wanted_in_port_sets[i]:
                self.wanted_in_port_sets[i].add(empty)


    def determinize_component(self, component: PortsNFA) \
            -> MappedPortsNFA:
        """
        Determinize the given component.
        """
        det_component = powc.determinize_with_tracking(component)
        
        if self.options.dfa_min:
            det_component = min.minimize_dfa_with_tracking(det_component)
        return det_component
    

    def compute_between_transitions(self, first_component: MappedPortsNFA) -> NFATransitions:
        """
        Compute transitions from first_component to the bottom part of the automaton.
        Between_transitions contain transitions 
            (exit port of deteminized new component -> entry port of orig NFA)
        """
        betw_trans = NFATransitions()
        processed = set()
        for port_set1, mapped_ports1 in first_component.out_map.items():
            for port1 in port_set1:
                if port1 not in processed:
                    processed.add(port1)
                    self.add_dests_for_port(betw_trans, port1, mapped_ports1)

        return betw_trans


    def add_dests_for_port(self, between_transitions: NFATransitions, 
                           port1: State, mapped_ports1: set[State]):
        for symbol, dst in self.ports_nfa.nfa.transitions.iterate_sym(port1):
            if self.labels[dst] != self.labels[port1]: # dst is in a different component than port1
                for src in mapped_ports1:
                    between_transitions.add_edge(symbol, src, dst)


    def prepare_ports(self):
        """
        Determinize all components, compute between transitions, and add all needed entry ports.
        """
        self.labels = scc_labeling([c.ports_nfa.nfa.states for c in self.comp_infos])

        self.wanted_in_port_sets = [set() for _ in self.comp_infos]
        self.wanted_out_port_sets = [set() for _ in self.comp_infos]
        self.determinized = []
        self.between_trans = []

        # prepare the components for fresh information about ports
        for component in self.comp_infos:
            component.ports_nfa.in_port_sets.clear()
            component.ports_nfa.out_port_sets.clear()

        # add ports that are known before determinization to all components
        self.add_external_ports()
        self.add_exit_ports()

        if self.port_options.connect_mode == PortConnectMode.INDIVIDUAL:
            self.add_internal_ports_indiv()
            self.add_empty_entry_sets()

        # determinize each component, compute between_transitions, 
        # and add needed entry ports to the successor components
        current_label = 0
        for i in range(len(self.comp_infos) - 1):
            component = self.comp_infos[i]

            det_comp = self.determinize_component(component.ports_nfa)
            between_trans = self.compute_between_transitions(det_comp)

            if self.port_options.connect_mode == PortConnectMode.GROUPED:
                self.add_internal_ports_grouped_from(between_trans, current_label + 1)

            current_label += 1
            self.determinized.append(det_comp)
            self.between_trans.append(between_trans)



    def partition(self) -> tuple[list[PortComponent], PortsNFA]:
        # find sccs, they are already ordered as needed
        sccs = find_sccs(self.ports_nfa.nfa)

        # merge some sccs according to partition method
        self.comp_infos = partition_into_comp_infos(self.ports_nfa, sccs, alphabet=self.ports_nfa.nfa.input_symbols, 
                                               method=self.port_options.partition_method)

        # add needed entry and exit ports to each component
        self.prepare_ports()

        if self.data is not None:
            self.data.write_component_count(len(self.comp_infos))
            self.data.write_last_comp_size(len(self.comp_infos[-1].ports_nfa.nfa.states))

        # retype everything to the required form
        port_components = [PortComponent(self.determinized[i], self.between_trans[i],
                                         list(self.wanted_in_port_sets[i]), list(self.wanted_out_port_sets[i]),
                                         len(self.comp_infos[i].ports_nfa.nfa.initial_states) > 0)
                           for i in range(len(self.comp_infos) - 1)]

        return port_components, self.comp_infos[-1].ports_nfa

    
    
