from aligater.types import *
from aligater.partitioning.gate_types import *
from aligater.partitioning.state_equivalence_checker import StateEquivalenceChecker


class GateChecker:
    """
    Checks whether a given partition of an NFA satisfies the conditions of gate NFAs.
    """
    def __init__(self, ports_nfa: PortsNFA, comp1: States, comp2: States,
                 equivalence_checker: StateEquivalenceChecker):
        self.ports_nfa = ports_nfa
        self.states1: States = comp1
        self.states2: States = comp2
        self.equivalence_checker = equivalence_checker

        # divide transitions into sets based on their properties
        # compute gate symbols and symbols in both components
        # self.symbols_in_1 = ...
        # self.symbols_in_2 = ...
        # self.trans1 = ...
        # self.trans2 = ...
        # self.gates = ...
        # self.gate_symbols = ...
        # self.gate_symbols1 = ...
        # self.gate_symbols2 = ...
        self._partition_transitions_and_symbols()

        # check if the given partition is a gate partition
        self.is_gate = self._is_gate_nfa()
        if not self.is_gate:
            return
        
        # compute inner ports and symbol groups from gates
        self._get_inner_ports()

        # create subautomata for given state sets comp1 and comp2
        self.aut1, _ = self.ports_nfa.get_subautomaton(self.states1, 
                                                    input_symbols=self.symbols_in_1,
                                                    transitions=self.trans1,
                                                    get_out_transitions=False)
        
        self.aut2, _ = self.ports_nfa.get_subautomaton(self.states2, 
                                                    input_symbols=self.symbols_in_2,
                                                    transitions=self.trans2,
                                                    get_out_transitions=False)
        
        # check if given partition is equal or disjoint
        self.type = self.get_type()


    def _partition_transitions_and_symbols(self):
        """
        Helper function. Partitions transitions into sets based on their status.
        """
        self.gates = NFATransitions()
        self.trans1 = NFATransitions()
        self.trans2 = NFATransitions()
        self.gate_symbols = set()
        self.symbols_in_1 = set()
        self.symbols_in_2 = set()

        for src, symbol, dst in self.ports_nfa.nfa.transitions.iterate_all():
            if src in self.states1:
                if dst in self.states1:
                    self.trans1.add_edge(symbol, src, dst)
                    self.symbols_in_1.add(symbol)

                elif dst in self.states2:
                    self.gates.add_edge(symbol, src, dst)
                    self.gate_symbols.add(symbol)
                
                else:
                    raise ValueError(f"State {dst} is in neither partition set.")

            elif src in self.states2:
                if dst in self.states1:
                    raise ValueError("Gates in a different direction, bad partition!")
                
                elif dst in self.states2:
                    self.trans2.add_edge(symbol, src, dst)
                    self.symbols_in_2.add(symbol)

                else:
                    raise ValueError(f"State {dst} is in neither partition set.")
                
            else:
                raise ValueError(f"State {src} is in neither partition set.")
        
        self.gate_symbols1 = self.symbols_in_1 & self.gate_symbols
        self.gate_symbols2 = self.symbols_in_2 & self.gate_symbols


    def _get_inner_ports(self):
        self.out_ports1: States = set()
        self.in_ports2: States = set()
        self.symbols_out_group1: dict[Symbol, set[State]] = {symbol: set() for symbol in self.gate_symbols}
        self.symbols_in_group2: dict[Symbol, set[State]] = {symbol: set() for symbol in self.gate_symbols}

        for src, symbol, dst in self.gates.iterate_all():
            self.out_ports1.add(src)
            self.in_ports2.add(dst)
            self.symbols_out_group1[symbol].add(src)
            self.symbols_in_group2[symbol].add(dst)


    def _symbols_satisfy_gate_condition(self) -> bool:
        """
        Check if either first or second component does not contain any gate symbols
        and that gates are only in one direction.
        """
        return (not self.gate_symbols1) or (not self.gate_symbols2)


    def _is_gate_nfa(self) -> bool:
        """
        Check if the given partition is a gate partition.
        """
        if not self._symbols_satisfy_gate_condition():
            return False
        
        init_states = self.ports_nfa.nfa.initial_states | set().union(*self.ports_nfa.in_port_sets)
        fin_states = self.ports_nfa.nfa.final_states | set().union(*self.ports_nfa.in_port_sets)

        return self._symbols_satisfy_gate_condition() and \
               len(init_states & self.states1) > 0 and \
               len(fin_states & self.states2) > 0
    
    
    def _is_basic_first(self):
        """
        Check if there are no gate symbols in the first component
        and no initial states or entry ports in the second component.
        """
        return not (self.gate_symbols1 or self.aut2.has_incoming())
    

    def _is_product_both(self):
        """
        Check if there are no gate symbols in both components,
        final states in the first component, and initial states in the second component.
        """
        return not self.gate_symbols1 and not self.gate_symbols2 and \
               self.aut1.has_outcoming() and self.aut2.has_incoming()
    
    def _is_basic_second(self):
        """
        Check if there are no gate symbols in the second component
        and no final states or exit ports in the first component.
        """
        return not (self.gate_symbols2 or self.aut1.has_outcoming())
        
    def _is_product_first(self):
        """
        Check if there are no gate symbols in the first component, 
        but there are initial states or entry ports in the second component.
        """
        return not self.gate_symbols1 and self.aut2.has_incoming()

    def _is_product_second(self):
        """
        Check if there are no gate symbols in the second component,
        but there are final states or exit ports in the first component.
        """
        return not self.gate_symbols2 and self.aut1.has_outcoming()

    def get_gate_comp_pair(self) -> GateCompPair:
        """
        Create a structure with data about this gate NFA.
        The structure is used in complementation algorithms.
        """
        if not self.is_gate or self.type.is_none():
            raise ValueError("Partition is not gate, cannot generate GateCompPair.")

        # add inner ports to sumautomata
        out_port_sets1: PortSets = list({frozenset(pset) for pset in self.symbols_out_group1.values()}.union(self.aut1.out_port_sets))

        if self.type.is_equal():
            in_port_sets2: PortSets = list({frozenset(pset) for pset in self.symbols_in_group2.values()}.union(self.aut2.in_port_sets))
        
        elif self.type.is_disjoint_ind():
            in_port_sets2 = list({frozenset([port]) for port in self.in_ports2}.union(self.aut2.in_port_sets))
        
        elif self.type.is_disjoint_grp():
            in_port_sets2_tmp = set()
            for in_port in self.gates:
                for g_symbol in self.gate_symbols:
                    in_port_sets2_tmp.add(frozenset(self.gates.iterate(in_port, g_symbol)))
            in_port_sets2 = list(in_port_sets2_tmp.union(self.aut2.in_port_sets))

        self.aut1.out_port_sets = out_port_sets1
        self.aut2.in_port_sets = in_port_sets2

        return GateCompPair(ports_aut1=self.aut1,
                            ports_aut2=self.aut2,
                            gates=self.gates,
                            gate_symbols=self.gate_symbols,
                            input_symbols=self.ports_nfa.nfa.input_symbols,
                            symbols_in_group2=self.symbols_in_group2,
                            symbols_out_group1=self.symbols_out_group1,
                            type=self.type)
    

    def _get_symbols_type(self) -> GateType_Symbols:
        if not self.gate_symbols1:
            if not self.gate_symbols2:
                return GateType_Symbols.BOTH
            return GateType_Symbols.FIRST
        if not self.gate_symbols2:
            return GateType_Symbols.SECOND
        raise ValueError("NFA is not Gate, cannot assign symbols gate type.")
    

    def _get_symbols_and_product_type(self) -> tuple[GateType_Symbols, GateType_Product]:
        if self._is_basic_first():
            return GateType_Symbols.FIRST, GateType_Product.BASIC
        if self._is_basic_second():
            return GateType_Symbols.SECOND, GateType_Product.BASIC
        if self._is_product_both():
            return GateType_Symbols.BOTH, GateType_Product.PRODUCT
        if self._is_product_first():
            return GateType_Symbols.FIRST, GateType_Product.PRODUCT
        if self._is_product_second():
            return GateType_Symbols.SECOND, GateType_Product.PRODUCT
        raise ValueError("Cannot determine gate type.")

    def _get_partition_type(self):
        if self._check_equal():
            return GateType_Partition.EQUAL
        return self._check_disjoint()


    def get_type(self) -> GateType:
        """
        Determine type of this gate NFA.
        """
        symbols_type, product_type = self._get_symbols_and_product_type()
        partition_type = self._get_partition_type()
        return GateType(partition_type, product_type, symbols_type)


    def _check_equal(self) -> bool:
        """
        Check if partition is equal.
        """
        self.equivalence_checker.remove_transitions(from_states=self.states2)

        for symbol, state_set in self.symbols_in_group2.items():
            
            self.equivalence_checker.remove_transitions(transitions=self.gates, under_symbols=(self.gate_symbols - {symbol}))
            equal_in_set = self._check_equal_in_set(state_set)
            self.equivalence_checker.add_removed_transitions()

            if not equal_in_set:
                self.equivalence_checker.add_removed_transitions()
                return False
            
        self.equivalence_checker.add_removed_transitions()
        return True
    
    def _check_disjoint(self) -> GateType_Partition:
        sorted_out_ports1 = sorted(self.out_ports1)

        is_disjoint_grp = True
        is_disjoint_ind = True

        for in_port_set in [self.ports_nfa.nfa.initial_states] + self.ports_nfa.in_port_sets:
            self.equivalence_checker.assign_initial_states(in_port_set)

            disjoint_info = self._check_disjoint_out_port_languages(sorted_out_ports1)

            if is_disjoint_grp and not self._check_disjoint_grp(disjoint_info):
                is_disjoint_grp = False
                is_disjoint_ind = False
            elif is_disjoint_ind and not self._check_disjoint_ind(disjoint_info):
                is_disjoint_ind = False

            if not is_disjoint_grp and not is_disjoint_ind:
                return GateType_Partition.NONE
            
        if is_disjoint_ind:
            return GateType_Partition.DISJOINT_IND
        if is_disjoint_grp:
            return GateType_Partition.DISJOINT_GRP
        return GateType_Partition.NONE

    
    def _check_disjoint_ind(self, disjoint_info: dict[tuple[State, State], bool]) -> bool:
        """
        Check if partition is disjoint, checking all gates individually.
        """
        for (state1, state2), are_disjoint in disjoint_info.items():
            if not are_disjoint:
                for symbol in self.gate_symbols:

                    for dst1 in self.gates[state1].get(symbol, set()):
                        for dst2 in self.gates[state2].get(symbol, set()):
                            if not self.equivalence_checker.are_init_equivalent({dst1}, {dst2}):
                                return False
        return True
    

    def _check_disjoint_grp(self, disjoint_info: dict[tuple[State, State], bool]) -> bool:
        """
        Check if partition is disjoint, grouping all destinations from one exit port together in sets.
        """
        for (state1, state2), are_disjoint in disjoint_info.items():
            if not are_disjoint:
                for symbol in self.gate_symbols:
                    dests1 = self.gates[state1].get(symbol, set())
                    dests2 = self.gates[state2].get(symbol, set())
                    if dests1 and dests2 and not self.equivalence_checker.are_init_equivalent(dests1, dests2):
                        return False
        return True
    

    def _check_disjoint_out_port_languages(self, sorted_out_ports: list[State]) -> dict[tuple[State, State], bool]:
        """
        Find out which out ports of aut1 accept disjoint languages.
        """

        disjoint_info: dict[tuple[State, State], bool] = {}

        for out_port1 in sorted(sorted_out_ports):
            for out_port2 in sorted(sorted_out_ports):
                # make sure that we do not check some pair twice
                if out_port1 > out_port2:
                    continue

                disjoint_info[(out_port1, out_port2)] =self.equivalence_checker.are_acc_disjoint({out_port1}, {out_port2})

        return disjoint_info


    def _check_equal_in_set(self, states: States) -> bool:
        """
        Check if all states in a given set accept equivalent languages.
        """
        if len(states) <= 1:
            return True
        
        states_list = list(states)
        state1 = set([states_list.pop()])
        while states_list:
            state2 = set([states_list.pop()])

            if not self.equivalence_checker.are_acc_equivalent(state1, state2):
                return False

            state1 = state2
        
        return True