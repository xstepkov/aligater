from aligater.types import *
from collections.abc import Iterator
import networkx as nx

########### SCC finding ##############################

def dfs_rec(aut: ExtendedNFA, state: State, 
            visited: States, stack: list[State]):
    visited.add(state)

    for dst in aut.transitions.iterate_dests_only(state):
        if dst not in visited:
            dfs_rec(aut, dst, visited, stack)
    
    stack.append(state)


def dfs(aut: ExtendedNFA):
    stack = []
    visited = set()
    for state in aut.states:
        if state not in visited:
            dfs_rec(aut, state, visited, stack)
    return stack


def find_sccs_recursive(aut: ExtendedNFA) -> list[States]:
    """
    Find strongly connected components.
    Using the Kosaraju-Sharir algorithm.
    """
    stack = dfs(aut)
    rev_aut = aut.reverse()

    components = []
    visited = set()

    for state in stack[::-1]:
        if state not in visited:
            old_visited = visited.copy()
            dfs_rec(rev_aut, state, visited, [])

            new_comp = visited - old_visited
            components.append(new_comp)

    return components


def find_sccs(aut: ExtendedNFA) -> list[States]:
    graph = aut.to_nx_graph(weighted=False)
    sccs = list(nx.strongly_connected_components(graph))
    sccs.reverse()
    return sccs
    

def scc_labeling(components: list[States]) -> dict[State, int]:
    """
    Return a dict of component labels for states.
    """
    labels: dict[State, int] = {}
    for i, component in enumerate(components):
        for state in component:
            labels[state] = i
    
    return labels


class ComponentGraph:
    """
    Directed acyclic graph of strongly connected components (SCCs).
    Stores alphabet symbols in SCCs and on the transitions between them.
    """
    def __init__(self, aut: ExtendedNFA, labels: dict[State, int]):
        """
        Build an SCC graph from a given automaton and SCC labels.
        """

        num_labels = max(labels.values()) + 1

        # sccs = list indexes
        # scc successors in "_graph" list indexed by component label
        self._graph: list[set[int]] = [set() for label in range(num_labels)]
        self._reverse_graph: list[set[int]] = [set() for label in range(num_labels)]
        self._comp_symbols: list[set[str]] = [set() for label in range(num_labels)]
        self._trans_symbols: dict[tuple[int, int], set[str]] = {}

        for src in aut.states:
            for symbol, dst in aut.transitions.iterate_sym(src):
                if labels[src] == labels[dst]:
                    self._comp_symbols[labels[src]].add(symbol)

                if labels[src] != labels[dst]:
                    self._graph[labels[src]].add(labels[dst])
                    self._reverse_graph[labels[dst]].add(labels[src])

                    if (labels[src], labels[dst]) not in self._trans_symbols:
                        self._trans_symbols[(labels[src], labels[dst])] = set()
                    self._trans_symbols[(labels[src], labels[dst])].add(symbol)

    def successors_of_set(self, labels: set[int]) -> set[int]:
        """
        Return all succeccors of a set of SCCs in SCC graph.
        """
        successors = set()
        for label in labels:
            for successor in self._graph[label]:
                if successor not in labels:
                    successors.add(successor)
        
        return successors

    def successors_of_single(self, label: int) -> Iterator[int]:
        """
        Return an iterator over all successors of one SCC in SCC graph.
        """
        for successor in self._graph[label]:
            yield successor

    def predecessors_of_single(self, label: int) -> Iterator[int]:
        for predecessor in self._reverse_graph[label]:
            yield predecessor

    def symbols_between(self, labels1: set[int], label2: int) -> set[str]:
        """
        Return all symbols on transitions from the SCC set 'labels1' to the SCC 'label2'.
        """
        symbols = set()
        for label1 in labels1:
            for symbol in self._trans_symbols.get((label1, label2), set()):
                symbols.add(symbol)
        
        return symbols
    
    def reverse(self):
        self._graph, self._reverse_graph = self._reverse_graph, self._graph
        self._trans_symbols = {(dst, src): val for (src, dst), val in self._trans_symbols.items()}


def create_scc_nfa(aut: ExtendedNFA, labels: dict[State, int]) -> ExtendedNFA:
    sccs = set(labels.values())
    input_symbols = aut.input_symbols.copy()
    scc_transitions = NFATransitions()
    initial_sccs = set()
    final_sccs = set()
    
    for src in aut.transitions:
        for symbol, dst in aut.transitions.iterate_sym(src):
            if labels[src] != labels[dst]:
                scc_transitions.add_edge(symbol, labels[src], labels[dst])

                if dst in aut.final_states:
                    final_sccs.add(labels[dst])
            
        if src in aut.final_states:
            final_sccs.add(labels[src])

        if src in aut.initial_states:
            initial_sccs.add(labels[src])
    
    nfa = ExtendedNFA(states=sccs,
              input_symbols=input_symbols,
              transitions=scc_transitions,
              initial_states=initial_sccs,
              final_states=final_sccs)
    return nfa
