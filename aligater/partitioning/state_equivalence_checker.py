from aligater.types import *
from aligater.utils import mata_handler
from collections.abc import Iterable


class StateEquivalenceChecker:
    """
    For a given ExtendedNFA, is able to check initiating and accepting state equivalence
    for states and sets of states.
    Underlying algorithm: antichains implemented in mata.
    """

    def __init__(self, nfa: ExtendedNFA):
        mata_cont = mata_handler.ext_nfa_to_mata_nfa(nfa)

        self.alphabet = mata_cont.get_alphabet()
        self.symbol_map_real_to_mata = mata_cont.symbol_dict

        if mata_cont.state_map is None:
            raise ValueError("Mata container does not contain state map")
        self.state_map: dict[State, int] = mata_cont.state_map

        self.m_nfa1 = mata_cont.nfa
        self.m_nfa2 = mata_cont.nfa.deepcopy()

        # intersection structures for disjoint checking
        # None if the intersection was not computed yet
        self.product = None 
        self.product_state_map: dict[tuple[int, int], int] | None = None

        self._removed_transitions: list[list] = []

    def are_init_equivalent(self, set1: States, set2: States):
        """
        Check if the two given sets of states initiate equivalent languages.
        Should be states of the ExtNFA that was sent to the constructor.
        """
        if set1 == set2:
            return True

        old_initial = self.m_nfa1.initial_states
        self._assign_initial_to(self.m_nfa1, self._translate_state_set(set1))
        self._assign_initial_to(self.m_nfa2, self._translate_state_set(set2))
        equiv = mata_handler.equivalence_check(self.m_nfa1, self.m_nfa2, self.alphabet)
        self._assign_initial_to(self.m_nfa1, old_initial)
        self._assign_initial_to(self.m_nfa2, old_initial)
        return equiv

    def are_acc_equivalent(self, set1: States, set2: States):
        """
        Check if the two given sets of states accept equivalent languages.
        Should be states of the ExtNFA that was sent to the constructor.
        """
        if set1 == set2:
            return True

        old_final = self.m_nfa1.final_states
        self._assign_final_to(self.m_nfa1, self._translate_state_set(set1))
        self._assign_final_to(self.m_nfa2, self._translate_state_set(set2))
        equiv = mata_handler.equivalence_check(self.m_nfa1, self.m_nfa2, self.alphabet)
        self._assign_final_to(self.m_nfa1, old_final)
        self._assign_final_to(self.m_nfa2, old_final)
        return equiv
    
    def are_acc_disjoint(self, set1: States, set2: States):
        """
        Check if the two given sets of states accept disjoint languages.
        Should be states of the ExtNFA that was sent to the constructor.
        """
        if not set1 or not set2:
            return True

        if set1 == set2:
            return False

        self._compute_product_in_not_exists()
        assert self.product_state_map is not None
        
        for state1 in set1:
            for state2 in set2:
                if (self.state_map[state1], self.state_map[state2]) in self.product_state_map:
                    return False
        return True

    def assign_initial_states(self, new_init_states: States):
        """
        Assign given set as initial states in the underlying automata for equivalence checking.
        Useful for checking equivalence if the automaton has multiple port sets.
        """
        init_list = self._translate_state_set(new_init_states)
        self._assign_initial_to(self.m_nfa1, init_list)
        self._assign_initial_to(self.m_nfa2, init_list)
        self.product = None

    def assign_final_states(self, new_final_states: States):
        """
        Assign given set as final states in the underlying automata for equivalence checking.
        Useful for checking equivalence if the automaton has multiple port sets.
        """
        final_list = self._translate_state_set(new_final_states)
        self._assign_final_to(self.m_nfa1, final_list)
        self._assign_final_to(self.m_nfa2, final_list)

    def _assign_initial_to(self, assign_to, new_init_states: list[State]):
        assign_to.clear_initial()
        assign_to.make_initial_states(new_init_states)

    def _assign_final_to(self, assign_to, new_final_states: list[State]):
        assign_to.clear_final()
        assign_to.make_final_states(new_final_states)

    def _translate_state_set(self, states: Iterable[State]) -> list[State]:
        return [self.state_map[state] for state in states]
    
    def _compute_product_in_not_exists(self):
        if self.product is None or self.product_state_map is None:
            old_final_states = self.m_nfa1.final_states
            all_states = list(self.state_map.values())

            self._assign_final_to(self.m_nfa1, all_states)
            self._assign_final_to(self.m_nfa2, all_states)

            self.product, self.product_state_map = mata_handler.intersection_with_product_map(self.m_nfa1, self.m_nfa2)

            self._assign_final_to(self.m_nfa1, old_final_states)
            self._assign_final_to(self.m_nfa2, old_final_states)

    def remove_transitions(self, transitions: NFATransitions | None = None, 
                           from_states: States | None = None, 
                           under_symbols: set[Symbol] | None = None):
        """
        Remove transitions satisfying all given constraints.
        If transitions are specified, remove only those.
        If from_states are specified, remove only transitions starting in those states.
        If symbols are apecified, remove only transitions under those symbols. Otherwise under all symbols.
        """
        new_removed_transitions = []
        allowed_src_states = from_states if from_states is not None else set(self.state_map.keys())
        allowed_symbols = under_symbols if under_symbols is not None else set(self.symbol_map_real_to_mata.keys())
        allowed_mata_symbols = {val for key, val in self.symbol_map_real_to_mata.items() if key in allowed_symbols}

        if transitions is not None:
            for src, symbol, dst in transitions.iterate_all():
                if src in allowed_src_states and symbol in allowed_symbols:
                    mata_trans = mata_handler.make_transition(self.state_map[src], self.symbol_map_real_to_mata[symbol], self.state_map[dst])
                    new_removed_transitions.append(mata_trans)
        
        else:
            for src in self._translate_state_set(allowed_src_states):
                trans_from_state = self.m_nfa1.get_trans_from_state_as_sequence(src)
                for transition in trans_from_state:
                    if transition.symbol in allowed_mata_symbols:
                        new_removed_transitions.append(transition)

        for transition in new_removed_transitions:
            self.m_nfa1.remove_trans(transition)
            self.m_nfa2.remove_trans(transition)

        self._removed_transitions.append(new_removed_transitions)

    def add_removed_transitions(self, num_batches: int = 1):
        """
        Add transitions that were removed in the last (specified number of) batches.
        """
        for _ in range(num_batches):
            if not self._removed_transitions:
                raise ValueError("No remaining batch of transitions to add.")
            
            trans_to_add = self._removed_transitions.pop()
            for transition in trans_to_add:
                self.m_nfa1.add_transition_object(transition)
                self.m_nfa2.add_transition_object(transition)
