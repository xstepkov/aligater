from aligater.types import *
from enum import Enum


class GateType_Partition(Enum):
    """
    Gate partition type: equivalence/disjointness of ports or port sets
    """
    NONE = 0
    EQUAL = 1
    DISJOINT_IND = 2
    DISJOINT_GRP = 3


class GateType_Product(Enum):
    """
    Gate type: is product needed?
    """
    BASIC = 0
    PRODUCT = 1


class GateType_Symbols(Enum):
    """
    Gate type: in which component are no gate symbols?
    """
    FIRST = 0   # no gate symbols in first
    SECOND = 1  # no gate symbols in second
    BOTH = 2    # no gate symbols in both


class GateType():
    """
    Types of gate NFAs. Determines which version of complementation algorithm can be used.
    """
    def __init__(self, partition: GateType_Partition, product: GateType_Product, symbols: GateType_Symbols):
        self._partition: GateType_Partition = partition
        self._product: GateType_Product = product
        self._symbols: GateType_Symbols = symbols

    def is_equal(self) -> bool:
        return self._partition == GateType_Partition.EQUAL
    
    def is_disjoint_ind(self) -> bool:
        return self._partition == GateType_Partition.DISJOINT_IND
    
    def is_disjoint_grp(self) -> bool:
        return self._partition == GateType_Partition.DISJOINT_GRP
    
    def is_first(self) -> bool:
        return self._symbols == GateType_Symbols.FIRST or \
               (self._symbols == GateType_Symbols.BOTH and self._product == GateType_Product.BASIC)
    
    def is_basic(self) -> bool:
        return self._product == GateType_Product.BASIC

    def is_product_both(self) -> bool:
        return self._symbols == GateType_Symbols.BOTH and self._product == GateType_Product.PRODUCT
    
    def is_none(self) -> bool:
        return self._partition == GateType_Partition.NONE
    
    def to_string(self) -> str:
        if self.is_none():
            return "NONE"
        return f"{self._partition.name}_{self._symbols.name}_{self._product.name}"


class GateCompPair:
    """
    Container for gate NFA data.
    Contains partition into components, information about gates and a type of the gate NFA.
    """
    def __init__(self, ports_aut1: PortsNFA, 
                 ports_aut2: PortsNFA, 
                 gates: NFATransitions, 
                 gate_symbols: set[str],
                 input_symbols: set[str],
                 symbols_out_group1: SymbolGroup,
                 symbols_in_group2: SymbolGroup,
                 type: GateType):
        
        self.ports_aut1: PortsNFA = ports_aut1
        self.ports_aut2: PortsNFA = ports_aut2
        self.gates: NFATransitions = gates
        self.gate_symbols: set[str] = gate_symbols
        self.input_symbols: set[str] = input_symbols
        self.symbols_out_group1: SymbolGroup = symbols_out_group1
        self.symbols_in_group2: SymbolGroup = symbols_in_group2
        self.type: GateType = type