
from typing import Optional
from aligater.types import *
import aligater.partitioning.sccs as sccs
from aligater.partitioning.gate_checker import GateChecker
from aligater.partitioning.gate_types import GateCompPair
from aligater.partitioning.state_equivalence_checker import StateEquivalenceChecker


class GatePartitioner:

    def __init__(self, ports_nfa: PortsNFA, number_of_partitions: int):
        self.ports_nfa = ports_nfa

        # find SCCs and build SCC graph
        self.components = sccs.find_sccs(self.ports_nfa.nfa)

        self.labels = sccs.scc_labeling(self.components)
        self.comp_graph = sccs.ComponentGraph(self.ports_nfa.nfa, self.labels)

        self.visited_partitions: set[frozenset[int]] = set()

        self.equivalence_checker = StateEquivalenceChecker(self.ports_nfa.nfa)

        self.number_of_partitions = number_of_partitions
        # list of currently best partitions, the best one is at 0
        self.best_partitions: list[GateChecker] = []


    def check_gate_update_symbols(self,
                                current_symbols: set[str], 
                                labels1: set[int], label2: int) -> bool:
        """
        Check if an SCC is conected to a predecessor SCC set by gates
        (all symbols on the transitions between 'labels1' and 'label2' do not appear in the SCC set).
        If not, add all symbols from the new SCC to the current_symbols set.
        """
        symbols_between = self.comp_graph.symbols_between(labels1, label2)
        is_gate = True

        for symbol in current_symbols:
            if symbol in current_symbols and symbol in symbols_between:
                is_gate = False
        
        if is_gate:
            return True
        
        for symbol in current_symbols:
            if symbol in symbols_between or symbol in self.comp_graph._comp_symbols[label2]:
                current_symbols.add(symbol)

        return False


    def update_symbols(self,
                       predecessor: set[int] | int | None, 
                       new_label: int, 
                       current_symbols: set[str]):
        """
        Add all symbols on the transitions between the predecessor and 'new_label'
        and inside the 'new_label' component to the 'current_symbols' set.
        'predecessor' can be either one label ar a set of labels.
        """
        if isinstance(predecessor, int):
            current_symbols.update(self.comp_graph._trans_symbols.get((predecessor, new_label), set()))
        
        if isinstance(predecessor, set):
            current_symbols.update(self.comp_graph.symbols_between(predecessor, new_label))

        current_symbols.update(self.comp_graph._comp_symbols[new_label])


    def add_components_before(self,
                            start_label: int,
                            current_labels: set[int],
                            current_symbols: set[str]):
        """
        Add all SCCs, from which there is a path to a newly added SCC 'start_label'.
        """
        if start_label < 0:
            # if no start label is specified, add SCCs from which there is a path
            # to any component in current_labels
            stack = list(current_labels)
        else:
            stack = [start_label]
        newly_added: set[int] = set()

        while stack:
            label = stack.pop()
            for pred in self.comp_graph.predecessors_of_single(label):

                if pred not in current_labels:
                    current_labels.add(pred)
                    newly_added.add(pred)
                    self.update_symbols(label, pred, current_symbols)
                    stack.append(pred)


    def try_connect_one_scc(self, current_labels: set[int], current_symbols: set[Symbol]) -> bool:
        """
        Try to connect one successor SCC that is not connected to the current SCC set by gates.
        """
        for successor in self.comp_graph.successors_of_set(current_labels):
            if successor not in current_labels:

                is_gate = self.check_gate_update_symbols(current_symbols, current_labels, successor)
                if not is_gate:
                    current_labels.add(successor)
                    self.add_components_before(successor, current_labels, current_symbols)
                    return True
        return False


    def find_largest_component(self, current_labels: set[int], current_symbols: set[Symbol]):
        """
        Find the largest set of SCCs that are not connected by gates.
        Modifies the sets 'current_labels' and 'current_symbols'.
        """
        self.add_components_before(-1, current_labels, current_symbols)
        connected = True
        while connected:
            connected = self.try_connect_one_scc(current_labels, current_symbols)


    def make_gate_checker(self, current_labels: set[int], reverse: bool) \
                        -> Optional[GateChecker]:
        """
        Build the GateChecker class for a given partition. 
        The checker automatically tests if the partition is gate, equal, or disjoint.
        """
        component1: States = set().union(*[self.components[label] for label in current_labels])
        component2 = self.ports_nfa.nfa.states - component1

        other_labels = set(range(len(self.components))) - current_labels
        if len(component1) <= 1 or len(component2) <= 1 or other_labels in self.visited_partitions:
            return None

        if reverse:
            checker = GateChecker(self.ports_nfa, component2, component1, self.equivalence_checker)
        else:
            checker = GateChecker(self.ports_nfa, component1, component2, self.equivalence_checker)
        
        if checker.is_gate and not checker.type.is_none():
            return checker
        return None
    

    def add_new_partition(self, new_partition: GateChecker):
        """
        Check if the new partition should be added into the list of best partitions; 
        If yes, insert it to the correct place.
        """
        if not new_partition.is_gate or new_partition.type.is_none():
            return

        if len(self.best_partitions) == 0:
            self.best_partitions.append(new_partition)
            return

        insert_at = 0
        for other_partition in self.best_partitions:
            if new_partition == self.better_partition(new_partition, other_partition):
                break
            insert_at += 1

        if insert_at < self.number_of_partitions:
            if len(self.best_partitions) == self.number_of_partitions:
                self.best_partitions.pop()
            self.best_partitions.insert(insert_at, new_partition)


    def expand_partition(self,
                        current_labels: set[int],
                        current_symbols: set[Symbol],
                        reverse: bool):
        """
        Search for gate partitions from a given 'current_labels' set.
        Labels denote the SCCs.
        """

        self.find_largest_component(current_labels, current_symbols)

        # test if this partition was tried already
        frozen_labels = frozenset(current_labels)
        if frozen_labels in self.visited_partitions:
            return None
        self.visited_partitions.add(frozen_labels)

        # check if the partition is equal gate or disjoint gate
        checker = self.make_gate_checker(current_labels, reverse)
        # compare the new partition with the old one
        if checker is not None:
            self.add_new_partition(checker)

        old_labels = current_labels.copy()
        old_symbols = current_symbols.copy()

        # try to add every successor SCC by one to the existing SCC set
        # to find more gate partitions
        for successor in self.comp_graph.successors_of_set(current_labels):
            current_labels.add(successor)
            self.update_symbols(current_labels, successor, current_symbols)
            self.add_components_before(successor, current_labels, current_symbols)

            self.expand_partition(current_labels, current_symbols, reverse)

            current_labels.intersection_update(old_labels)
            current_symbols.intersection_update(old_symbols)


    def find_best_gate_partition(self, no_gate_sym_in_first: bool = True):
        """
        Try to find an equal or disjoint gate partition of a given nfa.
        `no_gate_sym_in_first` specifies if we should search for a partition 
        with no gate symbols in the first component or in the second component.
        """

        # initialize variables
        initial_label = 0
        current_symbols: set[str] = set()

        if no_gate_sym_in_first:
            initiating_states: set[State] = self.ports_nfa.nfa.initial_states | \
                                            set().union(*self.ports_nfa.in_port_sets)
        else:
            # if we are searching for a partition with no gate symbols in the second component,
            # we start in the final states.
            initiating_states: set[State] = self.ports_nfa.nfa.final_states | \
                                            set().union(*self.ports_nfa.out_port_sets)

        for init_state in initiating_states:

            # initialize search
            current_labels = {self.labels[init_state]}
            current_symbols.clear()
            self.update_symbols(None, self.labels[init_state], current_symbols)

            if no_gate_sym_in_first:
                new_partition = self.expand_partition(current_labels, current_symbols, reverse=False)
            else:
                # if we are searching for a partition with no gate symbols in the second component,
                # we work on the reversed SCC graph.
                self.comp_graph.reverse()
                new_partition = self.expand_partition(current_labels, current_symbols, reverse=True)
                self.comp_graph.reverse()


    ################## PARTITIONS COMPARING ##########################

    def more_balanced_partition(self, checker1: GateChecker, checker2: GateChecker) -> GateChecker:
        """
        Return a partition, where the components have more similar sizes.
        """
        size_ratio1 = (min(len(checker1.states1), len(checker1.states2)) 
                       / max(len(checker1.states1), len(checker1.states2)))
        size_ratio2 = (min(len(checker2.states1), len(checker2.states2)) 
                       / max(len(checker2.states1), len(checker2.states2)))

        if size_ratio1 >= size_ratio2:
            return checker1
        return checker2


    def is_balanced_enough(self, checker: GateChecker) -> bool:
        return len(checker.states1) > 1 and len(checker.states2) > 1


    def equal_partition(self, checker1: GateChecker, checker2: GateChecker) -> GateChecker:
        """
        Return an equal partition, if one is equal and the other is not.
        Otherwise return a more balanced one.
        """
        if checker1.type.is_equal():
            if checker2.type.is_equal():
                return self.more_balanced_partition(checker1, checker2)
            return checker1
        
        if checker2.type.is_equal():
            return checker2
        return self.more_balanced_partition(checker1, checker2)


    def better_partition(self, checker1: Optional[GateChecker], 
                        checker2: Optional[GateChecker]) \
                            -> Optional[GateChecker]:
        # check that both partitions are not None
        if checker1 is None or not self.is_balanced_enough(checker1) \
                or checker1.type.is_none():
            if checker2 is not None and self.is_balanced_enough(checker2) \
                    and checker2.type.is_none():
                return checker2
            return None
        if checker2 is None or not self.is_balanced_enough(checker2) \
                or checker2.type.is_none():
            return checker1
        
        # if one is basic, return basic, otherwise return equal or a more balanced one
        # (basic = does not need product)
        if checker1.type.is_basic():
            if checker2.type.is_basic():
                return self.equal_partition(checker1, checker2)
            return checker1
        if checker2.type.is_basic():
            return checker2
        
        return self.equal_partition(checker1, checker2)
    

    ################## ENTRY POINT ###################
    
    def partition(self) -> list[GateCompPair]:
        """
        Try to find an equal or disjoint gate partition of the given nfa. 
        """
        # try to find a partition with no gate symbols in the first component
        self.find_best_gate_partition(no_gate_sym_in_first=True)

        # try to find a partition with no gate symbols in the second component
        self.find_best_gate_partition(no_gate_sym_in_first=False)

        return [partition.get_gate_comp_pair() for partition in self.best_partitions]


    #################### PARTITIONING BY GATE SYMBOLS ##################

    def partition_by_gate_symbols_from_state(self, 
                                             transition_nfa: PortsNFA, 
                                             real_nfa: PortsNFA,
                                             gate_symbols: set[str],
                                             state: str, reversed: bool):
        """
        Try to find a gate partition by given gate symbols starting from given state.
        """
        visited: set[str] = {state}
        stack: list[str] = [state]

        while stack:
            state = stack.pop()
            for symbol, dst in transition_nfa.nfa.transitions.iterate_sym(state):
                if symbol not in gate_symbols and dst not in visited:
                    visited.add(dst)
                    stack.append(dst)

        if reversed: 
            return GateChecker(real_nfa,  real_nfa.nfa.states - visited, visited, self.equivalence_checker)
        return GateChecker(real_nfa, visited, real_nfa.nfa.states - visited, self.equivalence_checker)


    def partition_by_gate_symbols_general(self, 
                                          transition_nfa: PortsNFA, 
                                          real_nfa: PortsNFA,
                                          gate_symbols: set[str], 
                                          reversed: bool):
        """
        Try to find a gate partition by given gate symbols,
        where the transitions are checked in the 'transition_nfa'
        and the found partitions are validated on the 'real_nfa'
        """
        visited_initial = set()

        # try to start the partition in every initial state
        for initial_state in transition_nfa.nfa.initial_states:
            if initial_state not in visited_initial:
                new_partition = self.partition_by_gate_symbols_from_state(transition_nfa, 
                                                                    real_nfa, 
                                                                    gate_symbols, 
                                                                    initial_state,
                                                                    reversed)

                if new_partition.is_gate:
                    self.add_new_partition(new_partition)


    def partition_by_gate_symbols_first(self, gate_symbols: set[str]):
        """
        Try to find a gate partition with no gate symbols in the first component.
        """
        self.partition_by_gate_symbols_general(self.ports_nfa, self.ports_nfa, 
                                                      gate_symbols, reversed=False)


    def partition_by_gate_symbols_second(self, gate_symbols: set[str]):
        """
        Try to find a gate partition with no gate symbols in the first component.
        """
        reversed_nfa = self.ports_nfa.reverse()
        self.partition_by_gate_symbols_general(reversed_nfa, self.ports_nfa, 
                                                gate_symbols, reversed=True)


    def partition_by_gate_symbols(self, gate_symbols: set[str]) \
                                    -> list[GateCompPair]:
        """
        Partition a gate NFA into two parts based on a given set of gate symbols.
        """
        # try to partition as if there are no gate symbols in the first component
        self.partition_by_gate_symbols_first(gate_symbols)

        # try to partition as if there are no gate symbols in the second component
        self.partition_by_gate_symbols_second(gate_symbols)

        # compare the partitions, if one is more blanced
        return [partition.get_gate_comp_pair() for partition in self.best_partitions]
        

    def parse_gate_symbols(self, name: str) -> set[str]:
        sym_str = name.split("_")[-1]
        return set(sym_str)

