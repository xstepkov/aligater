from aligater.types import *
import sys, traceback
from time import time_ns
from aligater.data_export import DataExporter
import aligater.complement as cmpl
from aligater.utils import mata_handler, minimizing as min, parser

def run_rabit_minimize_after(comp: ExtendedNFA | None, 
                             data: DataExporter,
                             lookahead: int = min.LOOKAHEAD_DEFAULT):
    if comp is not None:
        rabit_start_time = time_ns()
        min_comp = min.rabit_minimize(comp, comp.input_symbols, lookahead=lookahead)
        rabit_time = time_ns() - rabit_start_time

        if min_comp is not None:
            data.write_rabit_min_after_comp_size(len(min_comp.states))
            data.write_rabit_min_after_time(rabit_time)
        else:
            data.write_rabit_min_after_comp_size("ERROR")
            data.write_rabit_min_after_time("ERROR")

        return min_comp

    else:
        data.write_rabit_min_after_comp_size("N/A")
        data.write_rabit_min_after_time("N/A")


def load_nfa(file_path: str):
    try:
        result = parser.load_from_file(file_path, native_format=True)
    except Exception as e:
        print("Error in building nfa " + file_path + ": " + str(e), file=sys.stderr)
        return None
    
    return result


def complement_from_file(file_path: str, 
                         complement_mode: ComplementMode, 
                         port_options: PortOptions,
                         compl_options: ComplementationOptions,
                         gate_symbols: set[str] | None, 
                         xml_path: str | None, 
                         save_path: str | None,
                         pic_path: str | None,
                         verbose: bool,
                         short_verbose: bool):
    """
    Complement an automaton in given file by given mode, perform output actions.
    """

    loaded = load_nfa(file_path)
    if loaded is None:
        return
    raw_nfa, name = loaded

    mata_alphabet = mata_handler.get_mata_alphabet(raw_nfa)
    mata_nfa = mata_handler.make_nfa_mata(raw_nfa, mata_alphabet)

    comp = None
    data = DataExporter(complement_mode, compl_options, port_options)
    data.write_name(name)

    try:
        if complement_mode == ComplementMode.POWERSET:
            if compl_options.reverse:
                comp = cmpl.complement_reverse_powerset_mata(mata_nfa, compl_options, data)
            else:
                comp = cmpl.complement_forward_powerset_mata(mata_nfa, compl_options, data)

        elif complement_mode == ComplementMode.FORWARD_FIRST:
            comp = cmpl.complement_forward_reverse_first_mata(mata_nfa, True, save_path, data)
        elif complement_mode == ComplementMode.REVERSE_FIRST:
            comp = cmpl.complement_forward_reverse_first_mata(mata_nfa, False, save_path, data)

        elif complement_mode == ComplementMode.HEURISTIC:
            comp = cmpl.complement_heuristic_forw_rev_first(mata_nfa, Heuristic.DET_SUCCS, save_path, data)

        elif complement_mode == ComplementMode.RESIDUAL:
            comp = cmpl.complement_residual_mata(mata_nfa, not compl_options.reverse, data)

        else:
            ext_nfa = mata_handler.make_nfa_ext(raw_nfa)
            nfa_to_complement = ext_nfa.reverse() if compl_options.reverse else ext_nfa

            if complement_mode == ComplementMode.GATE:

                comp = cmpl.complement_gate(nfa_to_complement, compl_options, data=data,
                                        gate_symbols=gate_symbols)

            elif complement_mode == ComplementMode.SEQ:
                comp = cmpl.complement_port(nfa_to_complement, compl_options, port_options, data)

            else:
                raise NotImplementedError("Unhandled ComplementMode type.")
                
            if comp is not None and compl_options.reverse:
                comp = comp.reverse()
                comp = comp.remove_unreachable()

            # in powerset, reduction is applied directly in complementation methods
            if compl_options.simulation_min and comp is not None:
                comp = mata_handler.reduce(comp)
                data.write_sim_min_after_comp_size(len(comp.states))

        if compl_options.residual_min_after and comp is not None:
            ext_comp = mata_handler.make_nfa_ext(comp)
            comp = mata_handler.reduce_residual(ext_comp, Direction.FORWARD)

        if compl_options.rabit_min_after and comp is not None:
            ext_comp = mata_handler.make_nfa_ext(comp)
            rabit_comp = run_rabit_minimize_after(ext_comp, data)
            if rabit_comp is not None:
                comp = rabit_comp

        if compl_options.validate:
            if comp is None:
                data.write_correct("N/A")
            else:
                correct = mata_handler.validate_complement(mata_nfa, comp, mata_alphabet)
                data.write_correct(str(correct))
        
    except Exception as e:
        print("Error in complement " + name + ": " + str(e), file=sys.stderr)
        print(traceback.format_exc())
        comp = None
        data.write_success(False)

    if save_path is not None and comp is not None:
        try:
            ext_comp = mata_handler.make_nfa_ext(comp)
            parser.save_to_file(ext_comp, save_path)
        except Exception as e:
            print(str(e))
    
    if pic_path is not None and comp is not None:
        ext_comp = mata_handler.make_nfa_ext(comp)
        ext_comp.show_diagram(pic_path)

    if xml_path is not None:
        data.write_to_xml(xml_path)
    
    if verbose:
        print_stats(data.get_dict())

    if short_verbose:
        print(data.get_complement_size())


def print_stats(data: dict):
    """
    Print complementation statistics to stdout.
    """
    for name, info in data.items():
        print(f"{name}: {str(info)}")


if __name__ == "__main__":
    # input_path = "/home/notme/nfa-bench/benchmarks/z3-noodler/automatark/complement/instance00279-1.mata"
    # input_path = "/home/notme/aligater/automata_libs/random_small/gate_2_r10_hg.dot"
    # xml_path = None
    # save_path = None
    # pic_path = None
    # gate_symbols = None
    # mode = ModeHandler.translate_from_string("p")
    # verbose = True
    # options = OptionsHandler(["r", "md", "v"])
    # complement_from_file(input_path, mode, options, gate_symbols, xml_path, save_path, pic_path, verbose)
    pass
