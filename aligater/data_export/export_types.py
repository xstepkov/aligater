from typing import Any
from enum import Enum
from collections.abc import Iterator
import xml.etree.ElementTree as ET
from aligater.types import Option, ComplementMode, PortOptions, ARGUMENTS, ComplementationOptions

def get_options_prefix(options: ComplementationOptions):
    prefixes = []
    for option in options.list_active():
        if option not in [Option.VALIDATE, Option.RABIT_AFTER, Option.REVERSE]:
            prefixes.append(option.name)

    return "_".join(sorted(prefixes))


def get_prefix(mode: ComplementMode, options: ComplementationOptions, port_options: PortOptions) -> str:
    options_prefix = get_options_prefix(options)

    if mode == ComplementMode.POWERSET:
        prefix = "reverse" if options.reverse else "forward"
    else:
        mode_prefix = mode.name
        
        prefix = "r_" if options.reverse else "f_"
        prefix += mode_prefix

    if mode in [ComplementMode.GATE, ComplementMode.SEQ]:
        prefix += "_" + (str(options.powerset_direction.name).lower())

    if mode == ComplementMode.SEQ:
        prefix += f"_{port_options.partition_method.value}_{port_options.connect_mode.value}"

    if len(options_prefix) > 0:
        prefix += "_" + options_prefix

    if len(prefix) > 0:
        prefix += "_"
    return prefix.lower()


class DataExporter:
    def __init__(self, mode: ComplementMode, options: ComplementationOptions, port_options: PortOptions):
        self._data_dict: dict[str, Any] = {}
        self._prefix = get_prefix(mode, options, port_options)

        self.name = "name"
        self.orig_size = "orig_size"
        self.num_gates = self._prefix + "num_gates"
        self.A1_size = self._prefix + "A1_size"
        self.A2_size = self._prefix + "A2_size"
        self.comp_size = self._prefix + "comp_size"
        self.comp_size_useless = self._prefix + "comp_size_useless"
        self.comp_min_size = self._prefix + "comp_min_size"
        self.rabit_min_after_comp_size = self._prefix + "rabit_min_after_comp_size"
        self.mata_min_after_comp_size = self._prefix + "sim_min_after_comp_size"
        self.A1_det = self._prefix + "A1_forward"
        self.A2_det = self._prefix + "A2_forward"
        self.gate_type = self._prefix + "type"
        self.complement_time = self._prefix + "complement_time"
        self.partition_time = self._prefix + "partition_time"
        self.minimization_time = self._prefix + "minimization_time"
        self.min_complement_time = self._prefix + "min_complement_time"
        self.rabit_min_after_time = self._prefix + "rabit_min_after_time"
        self.component_count = self._prefix + "component_count"
        self.last_comp_size = self._prefix + "last_comp_size"
        self.last_comp_forw = self._prefix + "last_comp_forw"
        self.rabit_error_count = self._prefix + "rabit_error_count"
        self.rabit_min_during_time = self._prefix + "rabit_min_during_time"
        self.correct = self._prefix + "correct"
        self.success = self._prefix + "success"
        self.first_complement_is_smaller = self._prefix + "first_complement_is_smaller"
        self.forward_first = self._prefix + "forward_first"

        if options.rabit_during:
            self._write(self.rabit_error_count, 0)


    def iterate(self) -> Iterator[tuple[str, Any]]:
        for item in self._data_dict.items():
            yield item

    def get_dict(self) -> dict[str, Any]:
        return self._data_dict

    def _write(self, key: str, val: Any):
        self._data_dict[key] = val

    def write_name(self, val: str):
        self._write(self.name, val)

    def write_orig_size(self, val):
        self._write(self.orig_size, val)

    def write_num_gates(self, val):
        self._write(self.num_gates, val)

    def write_A1_size(self, val):
        self._write(self.A1_size, val)

    def write_A2_size(self, val):
        self._write(self.A2_size, val)

    def write_comp_size(self, val):
        self._write(self.comp_size, val)

    def write_comp_size_useless(self, val):
        self._write(self.comp_size_useless, val)

    def write_comp_min_size(self, val):
        self._write(self.comp_min_size, val)

    def write_rabit_min_after_comp_size(self, val):
        self._write(self.rabit_min_after_comp_size, val)
    
    def write_sim_min_after_comp_size(self, val):
        self._write(self.mata_min_after_comp_size, val)

    def write_A1_det(self, val):
        self._write(self.A1_det, val)
    
    def write_A2_det(self, val):
        self._write(self.A2_det, val)
    
    def write_gate_type(self, val: str):
        self._write(self.gate_type, val)
    
    def write_complement_time(self, val, units: str = "ns"):
        self._write(self.complement_time + "_" + units, val)

    def write_partition_time(self, val, units: str = "ns"):
        self._write(self.partition_time + "_" + units, val)

    def write_minimization_time(self, val, units: str = "ns"):
        self._write(self.minimization_time + "_" + units, val)

    def write_min_complement_time(self, val, units: str = "ns"):
        self._write(self.min_complement_time + "_" + units, val)

    def write_rabit_min_after_time(self, val, units: str = "ns"):
        self._write(self.rabit_min_after_time + "_" + units, val)

    def write_rabit_min_during_time(self, val, units: str = "ns"):
        self._write(self.rabit_min_during_time + "_" + units, val)

    def write_component_count(self, val):
        self._write(self.component_count, val)

    def write_last_comp_size(self, val):
        self._write(self.last_comp_size, val)
    
    def write_last_comp_forw(self, val):
        self._write(self.last_comp_forw, val)

    def write_rabit_error_count(self, val):
        self._write(self.rabit_error_count, val)

    def increase_rabit_error_count(self):
        count = self._data_dict.get(self.rabit_error_count, 0)
        self._write(self.rabit_error_count, count + 1)

    def write_correct(self, val):
        self._write(self.correct, val)

    def write_success(self, val):
        self._write(self.success, val)

    def write_first_smaller(self, val: bool):
        self._write(self.first_complement_is_smaller, val)

    def write_direction(self, forward_first: bool):
        self._write(self.forward_first, forward_first)

    def write_to_xml(self, path: str):
        root = ET.Element("automaton")
        data_dict = self.get_dict()

        for key, value in data_dict.items():
            child_element = ET.SubElement(root, key)
            child_element.text = str(value)

        tree = ET.ElementTree(root)
        with open(path, "ab") as file:
            ET.indent(tree, space="  ", level=0)
            tree.write(file)

    def get_complement_size(self) -> int | None:
        complement_size_key = None
        if self.rabit_min_after_comp_size in self._data_dict:
            complement_size_key = self.rabit_min_after_comp_size
        elif self.comp_min_size in self._data_dict:
            complement_size_key = self.comp_min_size
        elif self.comp_size in self._data_dict:
            complement_size_key = self.comp_size

        if complement_size_key is not None:
            return self._data_dict[complement_size_key]
        return None

    

