from typing import Any
from collections.abc import Iterator
from automata.fa.nfa import NFA
from aligater.types import *
from aligater.utils import generic as utils, mata_handler
import os
from collections.abc import Iterable
from aligater.utils.mata_handler import MataNfaContainer

# Various functions to parse automata from and to files, and between types.

class InputError(Exception):
    pass

def save_to_file(nfa: ExtendedNFA, file_path: str):
    _, name = os.path.split(file_path)
    _, ext = os.path.splitext(name)

    if ext == ".dot":
        ext_nfa_to_dot(nfa, file_path)
    elif ext == ".vtf" or ext == ".mata":
        ext_nfa_to_vtf(nfa, file_path)
    elif ext == ".ba":
        ext_nfa_to_ba(nfa, file_path)
    else:
        raise InputError(f"Invalid output file extension: {ext}.")


def load_from_file(file_path: str, native_format: bool = False) -> tuple[Any , str]:
    _, name = os.path.split(file_path)
    _, ext = os.path.splitext(name)

    if ext == ".dot":
        nfa = dot_to_ext_nfa(file_path)
    elif ext == ".dfa" or ext == ".mdfa":
        nfa = mona_to_nfa(file_path)
        nfa = ExtendedNFA.from_nfa(nfa)
    elif ext == ".vtf" or ext == ".mata":
        # try:
        if native_format:
            nfa = mata_file_to_mata_nfa(file_path)
        else:
            nfa = mata_file_to_ext_nfa(file_path)
        # except Exception:
        #     # mata library failed reading the automaton
        #     # try another parser
            # nfa = vtf_to_ext_nfa(file_path)
    else:
        raise InputError("Invalid input file format.")
    
    return nfa, name


def load_from_iterable(paths: Iterable[str]) -> Iterator[tuple[ExtendedNFA, str]]:
    for path in paths:
        try:
            result = load_from_file(path, native_format=False)
            yield result
        except InputError:
            print(f"Could not load file {path} - invalid format.")


def iter_directory(dir: str) -> Iterator[str]:
    for filename in os.scandir(dir):
        full_path = filename.path
        
        if os.path.isfile(full_path):
            yield full_path


def go_through_directory(dir: str) -> Iterator[tuple[ExtendedNFA, str]]:
    """
    An iterator which returns pairs (ExtendedNFA, name) 
    from all .dot or .vtf files in a directory.
    A convenience function for debugging.
    """
    return load_from_iterable(iter_directory(dir))


def ignore_line(line: str) -> bool:
    """
    Determine if a line in a .dot file should be ignored.
    """
    return (line.startswith("rankdir")
        or line.startswith("label")
        or line.startswith("node") 
        or (line.startswith("fake") and "->" not in line)
        or (line.startswith("I") and "->" not in line)
        or line.isspace()
        or "{" in line or "}" in line
        or "digraph" in line)


def is_initial_state(line: str) -> bool:
   """
   Is state in .dot file initial?
   """
   return "fillcolor" in line


def is_final_state(line: str) -> bool:
    """
    Is state in .dot file final?
    """
    return "peripheries=2" in line


def dot_to_nfa(file_path: str) -> NFA:
    """
    Parse a special format of a .dot file into an NFA.
    """
    with open(file_path, "r") as file:
        states: States = set()
        input_symbols: Alphabet = set()
        transitions = NFATransitions()
        initial_state = ""
        final_states: States = set()

        for line in file:
            if ignore_line(line):
                continue

            new_state = line.split()[0]
            states.add(new_state)

            if is_initial_state(line):
                initial_state = new_state
            if is_final_state(line):
                final_states.add(new_state)

            if "->" in line: # is edge
                letter = line.partition('[label="')[2].split('"]')[0]
                if letter != "":
                    input_symbols.add(letter)

                src = new_state
                dst = line.partition("-> ")[2].split()[0]
                transitions.add_edge(letter, src, dst)

    nfa = NFA(states=states, input_symbols=input_symbols,
        transitions=transitions.get_dict(), initial_state=initial_state,
        final_states=final_states)
    return nfa


def dot_to_ext_nfa(file_path: str) -> ExtendedNFA:
    """
    Parse a special format of a .dot file into an ExtendedNFA.
    """
    with open(file_path, "r") as file:
        states: States = set()
        input_symbols: set[str] = set()
        transitions = NFATransitions()
        initial_states: States = set()
        final_states: States = set()

        for line in file:
            if ignore_line(line):
                continue

            new_state = line.split()[0]
            states.add(new_state)

            if is_initial_state(line):
                initial_states.add(new_state)
            if is_final_state(line):
                final_states.add(new_state)

            if "->" in line: # is edge
                letter = line.partition('[label="')[2].split('"]')[0]
                if letter != "":
                    input_symbols.add(letter)

                src = new_state
                dst = line.partition("-> ")[2].split()[0]
                transitions.add_edge(letter, src, dst)

    nfa = ExtendedNFA(states=states, 
                      input_symbols=input_symbols,
                      transitions=transitions, 
                      initial_states=initial_states,
                      final_states=final_states)
    return nfa


def nfa_to_dot(aut: NFA, file_path: str):
    """
    Export an NFA to a special format of .dot. 
    """
    with open(file_path, "w") as file:
        file.write("digraph {\n")
        file.write("rankdir=LR\n")
        file.write('labelloc="t"\n')
        file.write('node [shape="circle"]\n')

        for s in aut.states:
            line = '{} [label="{}"'.format(str(s), str(s))

            if s == aut.initial_state:
                line += ', style="filled", fillcolor="#66cc33"'
            if s in aut.final_states:
                line += ", peripheries=2"
            line += "]\n"

            file.write(line)

        for src, transitions in aut.transitions.items():
            for ap, dests in transitions.items():
                for dst in dests:
                    line = '{} -> {} [label="{}"]\n'.format(src, dst, ap)
                    file.write(line)
        
        file.write("}")


def ext_nfa_to_dot(aut: ExtendedNFA, file_path: str):
    """
    Export an ExtendedNFA to a special format of .dot.
    """
    with open(file_path, "w") as file:
        file.write("digraph {\n")
        file.write("rankdir=LR\n")
        file.write('labelloc="t"\n')
        file.write('node [shape="circle"]\n')

        for s in aut.states:
            line = '{} [label="{}"'.format(str(s), str(s))

            if s in aut.initial_states:
                line += ', style="filled", fillcolor="#66cc33"'
            if s in aut.final_states:
                line += ", peripheries=2"
            line += "]\n"

            file.write(line)

        for src, transitions in aut.transitions.items():
            for ap, dests in transitions.items():
                for dst in dests:
                    line = '{} -> {} [label="{}"]\n'.format(src, dst, ap)
                    file.write(line)
        
        file.write("}")


def ext_nfa_to_ba(aut: ExtendedNFA, file_path: str):
    """
    Export an ExtendedNFA to .ba (format used by the RABIT tool).
    """

    with open(file_path, "w") as file:
        aut_1_init = utils.single_initial_state_new(aut)

        for init_state in aut_1_init.initial_states:
            file.write(f"[{init_state}]\n")

        for src in aut_1_init.states:
            for symbol, dst in aut_1_init.transitions.iterate_sym(src):
                file.write(f"{symbol},[{src}]->[{dst}]\n")

        for fin_state in aut_1_init.final_states:
            file.write(f"[{fin_state}]\n")


def parse_state_ba(string: str) -> State:
    if string[0] == "[":
        string = string[1:]
    if string[-1] == "]":
        string = string[:-1]
    return string


def ba_to_ext_nfa(file_path: str) -> ExtendedNFA:
    """
    Parse a .ba file (used by RABIT tool) to ExtendedNFA.
    """

    states: States = set()
    input_symbols: Alphabet = set()
    transitions = NFATransitions()
    initial_states: States = set()
    final_states: States = set()

    with open(file_path, "r") as file:
        init_line = file.readline().strip()
        init_state = parse_state_ba(init_line)
        initial_states.add(init_state)
        states.add(init_state)

        for line in file:
            line = line.strip()

            # is a transition
            if "," in line:
                symbol, states_str = line.split(",")
                src, dst = states_str.split("->")
                src = parse_state_ba(src)
                dst = parse_state_ba(dst)

                states.add(src)
                states.add(dst)
                input_symbols.add(symbol)
                transitions.add_edge(symbol, src, dst)

            # is not a transition -> is a final state
            else:
                fin_state = parse_state_ba(line)
                final_states.add(fin_state)
                states.add(fin_state)

    return ExtendedNFA(states=states,
                       input_symbols=input_symbols,
                       transitions=transitions,
                       initial_states=initial_states,
                       final_states=final_states,)     


def ext_nfa_to_vtf(aut: ExtendedNFA, file_path: str, comment: str | None = None):
    """
    Export an ExtendedNFA to .vtf.
    See https://github.com/ondrik/automata-benchmarks/tree/master/vtf 
    for the format description.
    """

    with open(file_path, "w") as file:
        if comment is not None:
            file.write(f"# {comment}\n")
        
        file.write("@NFA\n")

        alphabet = " ".join(aut.input_symbols)
        file.write(f"%Alphabet {alphabet}\n")

        initial = " ".join(map(str, aut.initial_states))
        file.write(f"%Initial {initial}\n")

        final = " ".join(map(str, aut.final_states))
        file.write(f"%Final {final}\n")

        for src in aut.states:
            for symbol, dst in aut.transitions.iterate_sym(src):
                file.write(f"{src} {symbol} {dst}\n")


def vtf_to_ext_nfa(file_path: str):
    """
    Parse .vtf automata format into ExtendedNFA.
    At the moment doesn't take quotes into account.
    """

    states: States = set()
    input_symbols: set[str] = set()
    transitions = NFATransitions()
    initial_states: States = set()
    final_states: States = set()
    sections = 0
    line_num = 0
    
    with open(file_path, "r") as file:
        for line in file:
            line = line.strip()

            if line.startswith("@"):
                pass
                # should contain NFA

            elif line == "":
                # ignore blank lines
                pass

            elif line.startswith("%Initial"):
                # initial states
                line = line.removeprefix("%Initial")
                add = line.split()
                initial_states.update(add)
                states.update(add)

            elif line.startswith("%Final"):
                # final states
                line = line.removeprefix("%Final")
                add = line.split()
                final_states.update(add)
                states.update(add)

            elif line.startswith("%Alphabet"):
                # input symbols
                line = line.removeprefix("%Alphabet")
                add = line.split()
                input_symbols.update(add)

            elif line.startswith("%States"):
                # states
                line = line.removeprefix("%States")
                add = line.split()
                states.update(add)

            elif line.startswith("%"):
                # some other features, ignore
                pass

            elif not line.startswith("#"):
                # edge

                add = line.split()
                if len(add) != 3:
                    raise InputError(f'Parsing of .vtf, line {line_num}: expected line "state symbol state".')
                
                src, symbol, dst = add
                states.add(src)
                states.add(dst)
                input_symbols.add(symbol)
                transitions.add_edge(symbol, src, dst)

            line_num += 1

    return ExtendedNFA(states=states,
                       input_symbols=input_symbols,
                       transitions=transitions,
                       initial_states=initial_states,
                       final_states=final_states) 

def mata_file_to_mata_nfa(file_path: str) -> MataNfaContainer:
    return mata_handler.mata_file_to_mata_nfa(file_path)

def mata_file_to_ext_nfa(file_path: str) -> ExtendedNFA:
    return mata_handler.mata_file_to_ext_nfa(file_path)

def mata_nfa_to_ext_nfa(mata_nfa: MataNfaContainer, default_symbol_value: str = "") -> ExtendedNFA:
    return mata_handler.mata_nfa_to_ext_nfa(mata_nfa, default_symbol_value)

def ext_nfa_to_mata_nfa(ext_nfa: ExtendedNFA) -> MataNfaContainer:
    return mata_handler.ext_nfa_to_mata_nfa(ext_nfa)

def dot_to_png(src_path: str, dst_path: str):
    """
    Create a picture from a .dot file.
    """
    os.system(f"dot -Tpng {src_path} > {dst_path}")


def has_loop_nfa(nfa: NFA, state: State) -> bool:
    for ap in nfa.transitions[state]:
        if state in nfa.transitions[state][ap]:
            return True
    return False


def get_states(input: str, start: str) -> set[str]:
    """
    Helper function for the MONA format.
    """
    if not input.startswith(start):
        return set()
    input = input.replace(start, "")
    states = set(input.split())
    return states


def mona_to_nfa(path: str) -> NFA:
    """
    Parse an NFA from the .dfa format from the MONA tool.
    """
    with open(path, "r") as file:
        initial_state = "0"
        input_symbols = set()
        transitions = NFATransitions()

        # ignore first 3 lines
        for _ in range(3):
            line = file.readline()

        states = get_states(file.readline(), "States ")
        final_states = get_states(file.readline(), "Final States ")

        file.readline()

        for line in file:
            line = line.rstrip()
            ap, rest = line.split("(", 1)
            src, rest = rest.split(")", 1)
            _, dst = rest.split(" -> ")

            input_symbols.add(ap)
            transitions.add_edge(ap, src, dst)


    nfa = NFA(states=states, input_symbols=input_symbols,
        transitions=transitions.get_dict(), initial_state=initial_state,
        final_states=final_states)
    return nfa


def save_nfa(nfa: NFA, dir: str, name: str, dot: bool, pic: bool):
    if pic:
        nfa.show_diagram(path="{}lin_gate_{}.png".format(dir, name))
    if dot:
        nfa_to_dot(nfa, "{}lin_gate_{}.dot".format(dir, name))

