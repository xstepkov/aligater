from aligater.types import *
from enum import Enum
from aligater.utils import parser, generic as utils, mata_handler
import os
import subprocess
from datetime import datetime
from automata.fa.nfa import NFA
from automata.fa.dfa import DFA
import importlib.resources as resources

# default value for lookahead parameter for RABIT minimization
LOOKAHEAD_DEFAULT = 10
INITIAL_PLACEHOLDER = frozenset({"INITIAL_STATES"})
FINAL_PLACEHOLDER = frozenset({"FINAL_STATES"})


class MinMode(Enum):
    RABIT = 0
    DFA = 1


def rabit_minimize(nfa: ExtendedNFA, alphabet: Alphabet, lookahead: int = LOOKAHEAD_DEFAULT) -> ExtendedNFA | None:
    timestamp = str(datetime.now().time())
    in_path = f"/tmp/rabit_minimizing_in_{timestamp}.ba"
    out_path = f"/tmp/rabit_minimizing_out_{timestamp}.ba"

    parser.ext_nfa_to_ba(nfa, in_path)

    rabit_path = str(resources.files("aligater") / 'RABIT/Reduce.jar')
    # rabit_dir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    # rabit_path = os.path.join(rabit_dir, "RABIT/Reduce.jar")
    process = subprocess.run(["java", "-jar", rabit_path, in_path, 
                            str(lookahead), "-finite", "-o", out_path],
                            stdout=subprocess.DEVNULL)
    
    if process.returncode != 0:
        reduced_nfa = None

    else:
        reduced_nfa = parser.ba_to_ext_nfa(out_path)
        reduced_nfa.input_symbols.update(alphabet)

    if os.path.exists(in_path):
        os.remove(in_path)
    if os.path.exists(out_path):
        os.remove(out_path)

    return reduced_nfa


def prepare_for_minimization(ports_nfa: MappedPortsNFA) -> \
                             tuple[ExtendedNFA, dict[str, frozenset[State]], dict[str, frozenset[State]]]:
    """
    Adds new edges under special symbols to the ports and initial and final states.
    Returns a DFA prepared for minimization and a mapping of new symbols to ports sets.
    """
    new_init_state = utils.unoccuring_int_state(ports_nfa.nfa.states)
    new_fin_state = utils.unoccuring_int_state(ports_nfa.nfa.states | {new_init_state})
    new_states = ports_nfa.nfa.states | {new_init_state, new_fin_state}
    new_transitions = ports_nfa.nfa.transitions.deepcopy()
    new_symbols = ports_nfa.nfa.input_symbols.copy()

    in_symbols_map: dict[str, frozenset[State]] = {}
    out_symbols_map: dict[str, frozenset[State]] = {}

    symbol_counter = utils.str_counter_except(ports_nfa.nfa.input_symbols)

    # initial states
    new_symbol = next(symbol_counter)
    new_symbols.add(new_symbol)
    for state in ports_nfa.nfa.initial_states:
        new_transitions.add_edge(new_symbol, new_init_state, state)
    in_symbols_map[new_symbol] = INITIAL_PLACEHOLDER

    # in ports
    for port_set in ports_nfa.in_map:
        new_symbol = next(symbol_counter)
        new_symbols.add(new_symbol)

        for port in ports_nfa.in_map[port_set]:
            new_transitions.add_edge(new_symbol, new_init_state, port)
        in_symbols_map[new_symbol] = port_set

    # final states
    new_symbol = next(symbol_counter)
    new_symbols.add(new_symbol)
    for state in ports_nfa.nfa.final_states:
        new_transitions.add_edge(new_symbol, state, new_fin_state)
    out_symbols_map[new_symbol] = FINAL_PLACEHOLDER

    # out ports
    for port_set in ports_nfa.out_map:
        new_symbol = next(symbol_counter)
        new_symbols.add(new_symbol)

        for port in ports_nfa.out_map[port_set]:
            new_transitions.add_edge(new_symbol, port, new_fin_state)
        out_symbols_map[new_symbol] = port_set
    
    new_dfa = ExtendedNFA(states=new_states,
                          input_symbols=new_symbols,
                          transitions=new_transitions,
                          initial_states={new_init_state},
                          final_states={new_fin_state})
    return new_dfa, in_symbols_map, out_symbols_map


def prepare_for_dfa_minimization(nfa: MappedPortsNFA) -> \
                             tuple[DFA, dict[str, frozenset[State]], dict[str, frozenset[State]]]:
    """
    Suppose nfa is deterministic. Returns DFA prepared for minimization.
    """
    modified_nfa, in_symbols_map, out_symbols_map = prepare_for_minimization(nfa)
    modified_nfa.completify()
    new_dfa = modified_nfa.to_dfa()
    return new_dfa, in_symbols_map, out_symbols_map


def modify_after_minimization(orig_aut: MappedPortsNFA, minimized_aut: ExtendedNFA, 
                              in_symbols_map: dict[str, frozenset[State]],
                              out_symbols_map: dict[str, frozenset[State]]) \
                                  -> MappedPortsNFA:
    """
    Reconstructs ports in a minimized automaton.
    """
    new_init_states = set()
    new_fin_states = set()
    new_symbols = orig_aut.nfa.input_symbols.copy()
    new_states = set(minimized_aut.states - minimized_aut.initial_states - minimized_aut.final_states)
    new_transitions = NFATransitions()
    new_in_map: PortSetsMap = {orig_set: set() for orig_set in orig_aut.in_map}
    new_out_map: PortSetsMap = {orig_set: set() for orig_set in orig_aut.out_map}

    for src in minimized_aut.states:
        for symbol, dst in minimized_aut.transitions.iterate_sym(src):

            # special symbol, transition should be translated into a port
            if symbol not in orig_aut.nfa.input_symbols:
                # initial states or entry ports
                if src in minimized_aut.initial_states and symbol in in_symbols_map:
                    orig_state_set = in_symbols_map[symbol]

                    if orig_state_set == INITIAL_PLACEHOLDER: # initial states
                        new_init_states.add(dst)
                    elif orig_state_set in new_in_map: # entry ports
                        new_in_map[orig_state_set].add(dst)

                # final states of exit ports
                if dst in minimized_aut.final_states and symbol in out_symbols_map:
                    orig_state_set = out_symbols_map[symbol]

                    if orig_state_set == FINAL_PLACEHOLDER: # initial states
                        new_fin_states.add(src)
                    elif orig_state_set in new_out_map: # entry ports
                        new_out_map[orig_state_set].add(src)

            # transition should be saved as a transition in the resulting automaton
            elif src in new_states and dst in new_states:
                new_transitions.add_edge(symbol, src, dst)

    modified_min_aut = ExtendedNFA(states=new_states,
                                   input_symbols=new_symbols,
                                   transitions=new_transitions,
                                   initial_states=new_init_states,
                                   final_states=new_fin_states)
    return MappedPortsNFA(modified_min_aut, new_in_map, new_out_map)


def modify_after_dfa_minimization(orig_aut: MappedPortsNFA, minimized_dfa: DFA, 
                                  in_symbols_map: dict[str, State],
                                  out_symbols_map: dict[str, State]) \
                                    -> MappedPortsNFA:
    minimized_nfa = ExtendedNFA.from_dfa(minimized_dfa)
    return modify_after_minimization(orig_aut, minimized_nfa, in_symbols_map, out_symbols_map)


def minimize_dfa_with_tracking(ports_dfa: MappedPortsNFA,
                               mode = '') \
                                -> MappedPortsNFA:
    """
    Suppose aut is deterministic. 
    Returns a minimized automaton with updated port mappings.
    """
    dfa, in_symbols_map, out_symbols_map = prepare_for_dfa_minimization(ports_dfa)

    minimized_dfa = dfa.minify()
    if minimized_dfa.isempty(): # do not remove the parentheses, this is not an error
        return MappedPortsNFA(ExtendedNFA.create_empty(ports_dfa.nfa.input_symbols), \
               {pset: set() for pset in ports_dfa.in_map}, \
               {pset: set() for pset in ports_dfa.out_map})
    
    minimized_nfa = modify_after_dfa_minimization(ports_dfa, minimized_dfa, in_symbols_map, out_symbols_map)
    return minimized_nfa



def rabit_minimize_with_tracking(ports_nfa: MappedPortsNFA, options: ComplementationOptions) \
                                -> MappedPortsNFA | None:
    """
    Returns a minimized automaton with updated port mappings, minimized by RABIT.
    """
    prepared_nfa, in_symbols_map, out_symbols_map = prepare_for_minimization(ports_nfa)

    minimized_nfa = rabit_minimize(prepared_nfa, ports_nfa.nfa.input_symbols, lookahead=options.lookahead)
    if minimized_nfa is None:
        return None
        
    minimized_nfa = modify_after_minimization(ports_nfa, minimized_nfa, in_symbols_map, out_symbols_map)
    return minimized_nfa


def mata_simulation_minimize(nfa: ExtendedNFA) -> ExtendedNFA:
    """
    Returns a minimized automaton, using the reduce function in mata.
    """
    reduced = mata_handler.reduce(nfa)
    assert isinstance(reduced, ExtendedNFA) # holds if nfa is ExtNFA, to silence the type checker
    return reduced


def mata_simulation_minimize_with_tracking(ports_nfa: MappedPortsNFA) -> MappedPortsNFA:
    """
    Returns a minimized automaton with updated port mappings, 
    minimized by the reduce function in mata.
    """
    prepared_nfa, in_symbols_map, out_symbols_map = prepare_for_minimization(ports_nfa)

    minimized_nfa = mata_simulation_minimize(prepared_nfa)
        
    minimized_nfa = modify_after_minimization(ports_nfa, minimized_nfa, in_symbols_map, out_symbols_map)
    return minimized_nfa


def mata_residual_minimize(nfa: ExtendedNFA, direction: Direction) -> ExtendedNFA:
    """
    Returns a minimized automaton, using the reduce function in mata.
    """
    reduced = mata_handler.reduce(nfa)
    assert isinstance(reduced, ExtendedNFA) # holds if nfa is ExtNFA, to silence the type checker
    return reduced


def mata_residual_minimize_with_tracking(ports_nfa: MappedPortsNFA, direction: Direction) -> MappedPortsNFA:
    """
    Returns a minimized automaton with updated port mappings, 
    minimized by the reduce function in mata.
    """
    prepared_nfa, in_symbols_map, out_symbols_map = prepare_for_minimization(ports_nfa)

    minimized_nfa = mata_residual_minimize(prepared_nfa, direction)
        
    minimized_nfa = modify_after_minimization(ports_nfa, minimized_nfa, in_symbols_map, out_symbols_map)
    return minimized_nfa


def change_final_states(aut: NFA | DFA, new_final: States) -> NFA | DFA:
    """
    Changes the final state set in a NFA or DFA.
    Cannot be changed directly, because the classes are immutable.
    """
    params = aut.input_parameters
    params['final_states'] = new_final

    if type(aut) == DFA:
        return DFA(**params)
    return NFA(**params)
