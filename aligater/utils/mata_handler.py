from aligater.types import *
from libmata import alphabets, parser
from libmata.nfa import nfa as mata_nfa
import sys
import os
from itertools import count
from enum import Enum
from collections.abc import Iterator

# All communication with mata is delegated to this file 
# because the imports show "unknown import symbol" in my editor

class MataNfaContainer:
    def __init__(self, nfa, symbol_dict: dict[str, int] | None = None, alphabet = None, 
                 state_map: dict[State, int] | None = None):
        self.nfa = nfa

        if symbol_dict is None and alphabet is None:
            raise ValueError("At least one of alphabet and symbol_dict must be provided to MataNfaContainer.")

        if alphabet is not None:
            self.alphabet = alphabet
        else:
            self.alphabet = self.get_alphabet(symbol_dict=symbol_dict)

        if symbol_dict is None:
            self.symbol_dict: dict[str, int] = self._get_symbol_dict()
        else:
            self.symbol_dict: dict[str, int] = symbol_dict

        self.state_map = state_map

    def _get_symbol_dict(self) -> dict[str, int]:
        if self.alphabet is not None:
            symbols = self.alphabet.get_alphabet_symbols()
            return {self.alphabet.reverse_translate_symbol(sym): sym for sym in symbols}

        raise ValueError("Missing mata alphabet, cannot construct symbol_dict.")
    
    def get_alphabet(self, symbol_dict: dict[str, int] | None = None):
        if self.alphabet is None:
            if symbol_dict is not None:
                return alphabets.OnTheFlyAlphabet.from_symbol_map(symbol_dict)
            return alphabets.OnTheFlyAlphabet.from_symbol_map(self.symbol_dict)
        
        return self.alphabet
    
    def get_num_states(self):
        return self.nfa.num_of_states()
    

def get_mata_alphabet(nfa: ExtendedNFA | MataNfaContainer):
    if isinstance(nfa, ExtendedNFA):
        return get_alphabet_from_symbols(nfa.input_symbols)
    elif isinstance(nfa, MataNfaContainer):
        return nfa.alphabet
    return None


def mata_nfa_to_ext_nfa(mata_nfa: MataNfaContainer, default_symbol_value: str = "") -> ExtendedNFA:
    initial_states = set(mata_nfa.nfa.initial_states)
    final_states = set(mata_nfa.nfa.final_states)
    states: States = set().union(initial_states, final_states)
    transitions = NFATransitions()

    symbol_map = mata_nfa.symbol_dict
    inverted_symbol_map = {val:key for key, val in symbol_map.items()}

    input_symbols = set(symbol_map.keys())

    for trans in mata_nfa.nfa.get_trans_as_sequence():
        symbol = str(inverted_symbol_map[trans.symbol])
        # symbol = str(inverted_symbol_map.get(trans.symbol, default_symbol_value))

        transitions.add_edge(symbol, trans.source, trans.target)
        states.add(trans.source)
        states.add(trans.target)
        input_symbols.add(symbol)

    return ExtendedNFA(states=states,
                       input_symbols=input_symbols.copy(),
                       transitions=transitions,
                       initial_states=initial_states,
                       final_states=final_states)


def mata_file_to_mata_nfa(file_path: str, alphabet: alphabets.OnTheFlyAlphabet | None = None) -> MataNfaContainer:
    if not os.path.exists(file_path):
        raise ValueError("Path to mata nfa does not exist.")

    alpha = alphabet if alphabet is not None else alphabets.OnTheFlyAlphabet.from_symbol_map({})

    mata_nfa = parser.from_mata(file_path, alpha)

    return MataNfaContainer(mata_nfa, alphabet=alpha)


def mata_file_to_ext_nfa(file_path: str) -> ExtendedNFA:
    mata_cont = mata_file_to_mata_nfa(file_path)

    return mata_nfa_to_ext_nfa(mata_cont)


def get_symbol_dict(alpha) -> dict[str, int]:
    symbols = alpha.get_alphabet_symbols()

    return {alpha.reverse_translate_symbol(sym): sym for sym in symbols}


def get_alphabet_from_symbols(symbols: set[Symbol]):
    return alphabets.OnTheFlyAlphabet.for_symbol_names(list(sorted(symbols)))
    

def ext_nfa_to_mata_nfa(ext_nfa: ExtendedNFA, 
                        alphabet: alphabets.OnTheFlyAlphabet | None = None) -> MataNfaContainer:
    m_nfa = mata_nfa.Nfa()

    if alphabet is None:
        alphabet = get_alphabet_from_symbols(ext_nfa.input_symbols)
    
    symbol_dict = get_symbol_dict(alphabet)

    counter = count()
    state_map_to_int = {state: next(counter) for state in ext_nfa.states}

    state_map = {}
    for state in ext_nfa.states:
        state_num = m_nfa.add_state(state_map_to_int[state])
        state_map[state] = state_num

    initial_mapped = [state_map[state] for state in ext_nfa.initial_states]
    m_nfa.make_initial_states(initial_mapped)

    final_mapped = [state_map[state] for state in ext_nfa.final_states]
    m_nfa.make_final_states(final_mapped)

    for src, symbol, dst in ext_nfa.transitions.iterate_all():
        m_nfa.add_transition(state_map[src], symbol_dict[symbol], state_map[dst], alphabet=alphabet)

    m_nfa_cont = MataNfaContainer(m_nfa, alphabet=alphabet, state_map=state_map)
    return m_nfa_cont


def forward_first_complement(cont: MataNfaContainer) -> Iterator[MataNfaContainer]:
    """
    Complement using the forward-first approach: revert(determinize(revert(determinize(aut)))).
    Returns sequentially: forward powerset complement without minimization, reverse minimized complement.
    """
    param_dict = {"algorithm": "classical",
                  "minimize": "false"}
    alphabet = cont.get_alphabet()

    forward_complement = mata_nfa.complement(cont.nfa, alphabet, params=param_dict)
    yield MataNfaContainer(forward_complement, alphabet=alphabet)

    reverse_min_complement = mata_nfa.revert(mata_nfa.determinize(mata_nfa.revert(forward_complement))).trim()
    yield MataNfaContainer(reverse_min_complement, alphabet=alphabet)


def reverse_first_complement(cont: MataNfaContainer) -> Iterator[MataNfaContainer]:
    """
    Complement using the reverse-first approach: determinize(revert(determinize(revert(aut)))).
    Returns sequentially: reverse powerset complement without minimization, forward minimized complement.
    """
    param_dict = {"algorithm": "classical",
                  "minimize": "false"}
    alphabet = cont.get_alphabet()

    reverse_complement = mata_nfa.revert(mata_nfa.complement(mata_nfa.revert(cont.nfa), alphabet, params=param_dict)).trim()
    yield MataNfaContainer(reverse_complement, alphabet=alphabet)

    forward_min_complement = mata_nfa.determinize(reverse_complement).trim()
    yield MataNfaContainer(forward_min_complement, alphabet=alphabet)


def residual_complement_forward(cont: MataNfaContainer) -> MataNfaContainer:
    """
    Complement as residual(rev(det(rev(A)))).
    """
    param_dict = {"algorithm": "classical",
                  "minimize": "false"}
    alphabet = cont.get_alphabet()

    complement = mata_nfa.reduce_residual_after(
                 mata_nfa.revert(
                 mata_nfa.complement(
                 mata_nfa.revert(cont.nfa), alphabet, params=param_dict))).trim()
    
    return MataNfaContainer(complement, alphabet=alphabet)


def residual_complement_reverse(cont: MataNfaContainer) -> MataNfaContainer:
    """
    Complement as rev(residual(rev(det(A)))).
    """
    param_dict = {"algorithm": "classical",
                  "minimize": "false"}
    alphabet = cont.get_alphabet()

    complement = mata_nfa.revert(
                 mata_nfa.reduce_residual_after(
                 mata_nfa.revert(
                 mata_nfa.complement(
                 cont.nfa, alphabet, params=param_dict)))).trim()
    
    return MataNfaContainer(complement, alphabet=alphabet)


def powerset_complement_mata(cont: MataNfaContainer, minify: bool = False) -> MataNfaContainer:
    """
    Complement with forward powerset, use Hopcroft minimization.
    """
    param_dict = {"algorithm": "classical",
                  "minimize": "false"}
    alphabet = cont.get_alphabet()
    input_aut = cont.nfa

    complement = mata_nfa.complement(input_aut, alphabet, params=param_dict)

    if minify:
        complement = complement.trim()
        complement = mata_nfa.minimize(complement, params={"algorithm": "hopcroft"})

    return MataNfaContainer(complement, symbol_dict=cont.symbol_dict, alphabet=alphabet)


def powerset_complement_brzozowski_mata(cont: MataNfaContainer, minify: bool = False) -> MataNfaContainer:
    """
    Complement with forward powerset, use Brzozowski minimization.
    """
    param_dict = {"algorithm": "classical",
                  "minimize": "false"}
    alphabet = cont.get_alphabet()

    input_aut = cont.nfa
    if minify:
        # mata uses brzozowski minimization: determinize(revert(determinize(revert(aut))))
        input_aut = mata_nfa.revert(mata_nfa.determinize(mata_nfa.revert(input_aut)))

    complement = mata_nfa.complement(input_aut, alphabet, params=param_dict)

    return MataNfaContainer(complement, symbol_dict=cont.symbol_dict, alphabet=alphabet)


def reverse_complement_mata(cont: MataNfaContainer, minify : bool = False, 
                            reduce_simulation : bool = False) -> MataNfaContainer:
    """
    Complement with reverse powerset, use Hopcroft minimization.
    """
    param_dict = {"algorithm": "classical",
                  "minimize": "false"}
    alphabet = cont.get_alphabet()

    input_aut = cont.nfa
    reversed = mata_nfa.revert(input_aut)
    reversed_comp = mata_nfa.complement(reversed, alphabet, params=param_dict)

    if minify:
        reversed_comp = reversed_comp.trim()
        reversed_comp = mata_nfa.minimize(reversed_comp, params={"algorithm": "hopcroft"})
    
    comp_with_unreachable = mata_nfa.revert(reversed_comp)
    comp = comp_with_unreachable.trim()

    if reduce_simulation:
        comp = mata_nfa.reduce(comp)

    return MataNfaContainer(comp, symbol_dict=cont.symbol_dict, alphabet=alphabet)


def reverse_complement_brzozowski_mata(cont: MataNfaContainer, minify : bool = False, 
                            reduce_simulation : bool = False) -> MataNfaContainer:
    """
    Complement with reverse powerset, use Brzozowski minimization.
    """
    param_dict = {"algorithm": "classical",
                  "minimize": "false"}
    alphabet = cont.get_alphabet()

    input_aut = cont.nfa
    if minify:
        # mata uses brzozowski minimization: determinize(revert(determinize(revert(aut))))
        # reversing the automaton before and after results in the order of operations
        # revert(determinize(revert(determinize(revert(revert(aut))))))
        # -> the two first reverts cancel each other out 
        # -> we can just determinize the automaton before running basic reverse complement to get the minimal one
        input_aut = mata_nfa.determinize(input_aut)

    reversed = mata_nfa.revert(input_aut)
    reversed_comp = mata_nfa.complement(reversed, alphabet, params=param_dict)
    
    comp_with_unreachable = mata_nfa.revert(reversed_comp)
    comp = comp_with_unreachable.trim()

    if reduce_simulation:
        comp = mata_nfa.reduce(comp)

    return MataNfaContainer(comp, symbol_dict=cont.symbol_dict, alphabet=alphabet)


def make_nfa_ext(nfa: ExtendedNFA | MataNfaContainer) -> ExtendedNFA:
    if isinstance(nfa, MataNfaContainer):
        return mata_nfa_to_ext_nfa(nfa)
    return nfa


def make_nfa_mata(nfa: ExtendedNFA | MataNfaContainer, alphabet) -> MataNfaContainer:
    if isinstance(nfa, ExtendedNFA):
        return ext_nfa_to_mata_nfa(nfa, alphabet)
    return nfa


def validate_complement(orig: ExtendedNFA | MataNfaContainer, 
                        complement: ExtendedNFA | MataNfaContainer,
                        alphabet = None):
    if alphabet is None:
        if isinstance(orig, MataNfaContainer):
            alphabet = get_mata_alphabet(orig)
        else: 
            # if complement is mata_nfa, get its alphabet
            # if it is ExtendedNFA, both are ExtNFA and it doesnt matter anyway
            alphabet = get_mata_alphabet(complement)

    orig_mata = make_nfa_mata(orig, alphabet)
    comp_mata = make_nfa_mata(complement, alphabet)
    # print(comp_mata.symbol_dict)

    if orig_mata.alphabet != alphabet:
        print("The alphabet of original NFA is different from the provided one.")
        print(orig_mata.alphabet)
        print(alphabet)
        return False
    if comp_mata.alphabet != alphabet:
        print("The alphabet of complement is different from the provided one.")
        print(comp_mata.alphabet)
        print(alphabet)
        return False

    union = mata_nfa.union(orig_mata.nfa, comp_mata.nfa)
    if not union.is_universal(alphabet):
        print("Union not universal")
        return False
    
    intersection = mata_nfa.intersection(orig_mata.nfa, comp_mata.nfa)
    if not intersection.is_lang_empty():
        print("Intersection is not empty")
        return False
    
    return True


def reduce(nfa: ExtendedNFA) -> ExtendedNFA:
    """
    Uses the reduce function from mata to reduce the automaton with simulations.
    """
    alphabet = get_mata_alphabet(nfa)
    m_nfa = make_nfa_mata(nfa, alphabet)
    reduced = mata_nfa.reduce(m_nfa.nfa)

    m_cont_reduced = MataNfaContainer(reduced, alphabet=alphabet)
    return mata_nfa_to_ext_nfa(m_cont_reduced)


def reduce_with_state_map(nfa: ExtendedNFA) -> tuple[ExtendedNFA, dict[State, State]]:
    """
    Uses the reduce function from mata to reduce the automaton with simulations.
    Returns the reduced automaton and a state map { original_state: new_state }.
    Preserves only the forward equivalence of states (if they are treated as initial).
    """
    alphabet = get_mata_alphabet(nfa)
    m_nfa = ext_nfa_to_mata_nfa(nfa)
    reduced, reduced_state_map = mata_nfa.reduce_with_state_map(m_nfa.nfa)

    m_cont_reduced = MataNfaContainer(reduced, alphabet=alphabet)

    if m_nfa.state_map is None:
        raise ValueError("State map in MatNfaContainer is None.")

    state_map = {orig_state: reduced_state_map[m_nfa_state] 
                 for orig_state, m_nfa_state in m_nfa.state_map.items()}

    return mata_nfa_to_ext_nfa(m_cont_reduced), state_map


def reduce_residual(nfa: ExtendedNFA, direction: Direction):
    params = {
        "algorithm": "residual",
        "type": "after",
        "direction": "forward" if direction == Direction.FORWARD else "backward"
    }

    alphabet = get_mata_alphabet(nfa)
    m_nfa = make_nfa_mata(nfa, alphabet)
    reduced = mata_nfa.reduce(m_nfa.nfa, params=params)

    m_cont_reduced = MataNfaContainer(reduced, alphabet=alphabet)
    return mata_nfa_to_ext_nfa(m_cont_reduced)


def equivalence_check(nfa1, nfa2, alphabet):
    return mata_nfa.equivalence_check(nfa1, nfa2, alphabet=alphabet)


def intersection_with_product_map(nfa1, nfa2):
    return mata_nfa.intersection_with_product_map(nfa1, nfa2)

def make_transition(src: int, symbol: int, dst: int):
    return mata_nfa.Transition(src, symbol, dst)


def load_nfa(file_path: str) -> MataNfaContainer | None:
    try:
        nfa = mata_file_to_mata_nfa(file_path)
    except Exception as e:
        print(f"Unable to load mata NFA {file_path}: {str(e)}" , file=sys.stderr)
        return
    
    return nfa
