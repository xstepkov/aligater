from aligater.types import *
from aligater.utils import minimizing as min, heuristics
from collections.abc import Iterator
from collections import deque
from itertools import count

# Implementations of powerset complement 
# -- forward and reverse, with port tracking
# Mostly to be used by other complementing methods.

def sets_map_unwrap_keys(sets_map: PortSetsMap) -> PortMap:
    """
    Transform PortSetsMap to PortMap. 
    Suppose all keys in sets_map are one-element sets.
    """

    port_map = {}
    for key, value in sets_map.items():
        if len(key) != 1:
            raise ValueError(f"Key in PortSetsMap has {len(key)} elements instead of 1.")
        unfolded_key = next(iter(key))
        port_map[unfolded_key] = value

    return port_map


def transitions_by_symbol(transitions: NFATransitions, 
                          states: States, 
                          symbol: str) -> frozenset[State]:
    """
    Returns all states, to which there is a transition under 'symbol' from 'states'.
    """
    neighbourhood = set()

    for state in states:
        if state in transitions and symbol in transitions[state]:
            dests = transitions[state][symbol]

            for dst in dests:
                neighbourhood.add(dst)

    return frozenset(neighbourhood)


def next_name(name: State, counter: Iterator, retain_names: bool) -> State:
    if retain_names:
        return name
    return next(counter)


def powerset_construction(transitions: NFATransitions, 
                          input_symbols: set[str],
                          initiating_sets: PortSets,
                          retain_names: bool = False) \
                           -> tuple[NFATransitions, dict[frozenset[State], State]]:
    """
    Perform powerset construction on given transition set, starting in initiating sets.
    """
    state_sets_map: dict[frozenset[State], State] = {}
    new_states = set()
    new_transitions = NFATransitions()

    state_counter = count(0)
    queue: deque = deque()

    for state_set in initiating_sets:
        state_name = next_name(state_set, state_counter, retain_names)
        state_sets_map[state_set] = state_name
        queue.append(state_set)

    while queue:
        current_state_set = queue.popleft()
        current_state_name = state_sets_map[current_state_set]
        
        new_states.add(current_state_name)

        for symbol in input_symbols:
            dests = transitions_by_symbol(transitions, current_state_set, symbol)

            if dests not in state_sets_map:
                new_state_name = next_name(dests, state_counter, retain_names)
                state_sets_map[dests] = new_state_name

                queue.append(dests)
            
            else:
                new_state_name = state_sets_map[dests]
            
            new_transitions.add_edge(symbol, current_state_name, new_state_name)
    
    return new_transitions, state_sets_map


def get_determinized_ports(entry_sets: PortSets, 
                           exit_sets: PortSets, 
                           state_sets_map: dict[frozenset[State], State]) \
                           -> tuple[PortSetsMap, PortSetsMap]:
    """
    Get port mappings from a determinized automaton.
    """
    in_ports_map: PortSetsMap = {}
    out_ports_map: PortSetsMap = {exit_set: set() for exit_set in exit_sets}

    for state_set, state_name in state_sets_map.items():
        for exit_set in exit_sets:
            if state_set & exit_set:
                out_ports_map[exit_set].add(state_name)

        if state_set in entry_sets:
            in_ports_map[state_set] = {state_name}

    return in_ports_map, out_ports_map
    

def determinize_with_tracking(ports_nfa: PortsNFA,
                              retain_names: bool = False) \
                                -> MappedPortsNFA:
    """
    Returns an equivalent DFA with port mappings.
    """
    # if nfa.is_deterministic():
    #     sink = None
    #     if completify:
    #         sink = nfa.completify()

    #     if init_state_needed and not nfa.initial_states:
    #         new_init_state = sink if sink is not None else utils.unoccuring_state(nfa.states, prefix = "init")
    #         nfa.initial_states.add(new_init_state)
    #         nfa.states.add(new_init_state)

    #         for symbol in nfa.input_symbols:
    #             nfa.transitions.add_edge(symbol, new_init_state, new_init_state)

    #     in_ports_map = {ports: set(ports) for ports in entry_sets}
    #     out_ports_map = {ports: set(ports) for ports in exit_sets}
    #     return nfa, in_ports_map, out_ports_map

    states: States = set()
    final_states: States = set()

    init_sets = ports_nfa.in_port_sets.copy() + [frozenset(ports_nfa.nfa.initial_states)]
    transitions, state_sets_map = powerset_construction(ports_nfa.nfa.transitions, ports_nfa.nfa.input_symbols,
                                                        init_sets, retain_names=retain_names)

    for state_set, state_name in state_sets_map.items():
        states.add(state_name)

        if ports_nfa.nfa.final_states & state_set:
            final_states.add(state_name)

    initial_state_name = state_sets_map[frozenset(ports_nfa.nfa.initial_states)]
    sink_name = state_sets_map.get(frozenset(), None)

    determinized = ExtendedNFA(states=states, 
                      input_symbols=ports_nfa.nfa.input_symbols, 
                      transitions=transitions,
                      initial_states={initial_state_name}, 
                      final_states=final_states,
                      sink=sink_name)
    in_ports_map, out_ports_map = get_determinized_ports(ports_nfa.in_port_sets, ports_nfa.out_port_sets, state_sets_map)
    # add_ports(determinized, in_ports_map, out_ports_map)

    return MappedPortsNFA(determinized, in_ports_map, out_ports_map)


def complement_dfa_with_ports(ports_dfa: MappedPortsNFA) \
                              -> MappedPortsNFA:
    """
    Create a complement of a DFA, and complements of the port sets.
    """
    comp_out_ports_map: PortSetsMap = {}
    for orig_ports, new_ports in ports_dfa.out_map.items():
        comp_out_ports_map[orig_ports] = ports_dfa.nfa.states - new_ports
        
    comp_dfa = ExtendedNFA(states=ports_dfa.nfa.states,
                           input_symbols=ports_dfa.nfa.input_symbols,
                           transitions=ports_dfa.nfa.transitions,
                           initial_states=ports_dfa.nfa.initial_states,
                           final_states=ports_dfa.nfa.states - ports_dfa.nfa.final_states,
                           sink=ports_dfa.nfa.sink)
    return MappedPortsNFA(comp_dfa, ports_dfa.in_map, comp_out_ports_map)


def forward_complement_with_tracking(ports_nfa: PortsNFA, 
                                     options: ComplementationOptions) -> \
                                     MappedPortsNFA:
    """
    Performs forward powerset complement on a given ExtendedNFA with given entry and exit port sets.
    Returns a complement NFA and functions in_group_map and out_group_map.
    """
    determinization_result = determinize_with_tracking(ports_nfa)
    complement_result = complement_dfa_with_ports(determinization_result)

    if options.dfa_min:
        return min.minimize_dfa_with_tracking(complement_result)
    return complement_result


def reverse_complement_with_tracking(ports_nfa: PortsNFA, 
                                     options: ComplementationOptions) -> MappedPortsNFA:
    """
    Performs reverse powerset complement on a given ExtendedNFA with given entry and exit port sets.
    Returns a complement NFA and functions in_group_map and out_group_map.
    """
    reversed_nfa = ports_nfa.nfa.reverse()
    rev_complement_result = forward_complement_with_tracking(
            PortsNFA(reversed_nfa, ports_nfa.out_port_sets, ports_nfa.in_port_sets), options)

    complement_nfa = rev_complement_result.nfa.reverse()
    in_map, out_map = rev_complement_result.out_map, rev_complement_result.in_map

    must_remain = set().union(*out_map.values(), *in_map.values())
    reachable = complement_nfa.remove_unreachable(must_remain=must_remain)

    reachable_with_ports = MappedPortsNFA(reachable, in_map, out_map)

    if options.rabit_during:
        min_result = min.rabit_minimize_with_tracking(reachable_with_ports, options)
        if min_result is not None:
            return min_result

    return reachable_with_ports


def best_complement_with_tracking(ports_nfa: PortsNFA, 
                                  options: ComplementationOptions) -> tuple[MappedPortsNFA, bool]:
    """
    Try forward and reverse powerset complementation, return the smaller result.
    Returns tuple (complement, in_ports_map, out_ports_map, method(forward=true))
    """
    forw_result = forward_complement_with_tracking(ports_nfa, options)
    rev_result = reverse_complement_with_tracking(ports_nfa, options)

    if len(forw_result.nfa.states) <= len(rev_result.nfa.states):
        return (forw_result, True)
    return (rev_result, False)


def heuristic_complement_with_tracking(ports_nfa: PortsNFA,
                                       options: ComplementationOptions) -> tuple[MappedPortsNFA, bool]:
    """
    Choose the powerset direction by the `deterministic successors` heuristic.
    Returns tuple (complement, in_ports_map, out_ports_map, method(forward=true))
    """

    dir = heuristics.decide_direction_port_nfa(ports_nfa, Heuristic.DET_SUCCS)
    if dir == Direction.FORWARD:
        return forward_complement_with_tracking(ports_nfa, options), True
    elif dir == Direction.REVERSE:
        return reverse_complement_with_tracking(ports_nfa, options), False
    
    raise NotImplementedError("Missing handled case for direction.")


def complement_with_tracking(ports_nfa: PortsNFA, options: ComplementationOptions) -> tuple[MappedPortsNFA, bool]:
        if options.powerset_direction == PowersetDirection.REVERSE:
            return reverse_complement_with_tracking(ports_nfa, options), False
        elif options.powerset_direction == PowersetDirection.FORWARD:
            return forward_complement_with_tracking(ports_nfa, options), True
        elif options.powerset_direction == PowersetDirection.HEURISTIC:
            return heuristic_complement_with_tracking(ports_nfa, options)
        elif options.powerset_direction == PowersetDirection.BEST:
            return best_complement_with_tracking(ports_nfa, options)
        
        raise NotImplementedError("Missing handled case of PowersetDirection.")