from automata.fa.dfa import DFA
from automata.fa.nfa import NFA
from typing import Any, AbstractSet
from collections.abc import Iterable, Iterator
from collections import deque
from itertools import combinations, chain
from aligater.types import *

# Various helper functions for working with finite automata


def dfa_reachable_cross_product(dfa1: DFA, dfa2: DFA) \
                                -> set[tuple[State, State]]:
    """
    Create cross-product of two DFAs.
    Returns a set of reachable states of the cross-product.
    Supposes both automata have the same input symbols.
    """
    if dfa1.input_symbols != dfa2.input_symbols:
        raise ValueError("Both automata in cross-product \
                          must have the same input symbols.")
    
    input_symbols = dfa1.input_symbols
    # transitions = dict()
    initial_state = (dfa1.initial_state, dfa2.initial_state)

    visited: set[tuple[State, State]] = {initial_state}
    queue = deque()
    queue.append(initial_state)

    while queue:
        state = queue.popleft()

        state1, state2 = state

        for symbol in input_symbols:
            next_state1, next_state2 = None, None
            if state1 in dfa1.transitions and symbol in dfa1.transitions[state1]:
                next_state1 = dfa1.transitions[state1][symbol]
            if state2 in dfa2.transitions and symbol in dfa2.transitions[state1]:
                next_state2 = dfa2.transitions[state1][symbol]

            if next_state1 is None or next_state2 is None:
                continue

            next_state = (next_state1, next_state2)
            # add_edge_dfa(transitions, symbol, state, next_state)

            # If next state is new, add to queue
            if next_state not in visited:
                visited.add(next_state)
                queue.append(next_state)

    # return DFA(states=visited,
    #            input_symbols=input_symbols,
    #            transitions=transitions,
    #            initial_state=initial_state,
    #            final_states=set())
    return visited


def symbol_iterator() -> Iterator[Symbol]:
    symbols = list(map(chr, range(ord('a'), ord('z')+1)))
    for s in chain.from_iterable(combinations(symbols, k) for k in range(1, len(symbols)+1)):
        yield "".join(s)


def rename_states_and_add(nfa: ExtendedNFA | NFA, 
                          states: States, 
                          transitions: NFATransitions, 
                          initial_states: set[str], 
                          final_states: States, 
                          state_counter: Iterable[State]) \
                            -> dict[State, State]:
    state_map = create_state_map(nfa.states, counter=state_counter)

    for state in state_map.values():
        states.add(state)

    for state in nfa.final_states:
        final_states.add(state_map[state])

    if isinstance(nfa, NFA):
        initial_states.add(state_map[nfa.initial_state])
    else:
        for state in nfa.initial_states:
            initial_states.add(state_map[state])

    add_transitions(state_map, transitions, nfa.transitions)

    return state_map


def str_counter(start: int) -> Iterator[str]:
    """
    Iterable of natural number strings starting from given number.
    """
    num = start
    while True:
        yield str(num)
        num += 1


def str_counter_except(exclude: set[str]) -> Iterator[str]:
    """
    Iterable of natural number strings that do not appear in a given set.
    """
    num = 0
    while True:
        str_num = str(num)

        if str_num not in exclude and num not in exclude:
            yield str_num

        num += 1


def remove_states(transitions: NFATransitions, 
                  final_states: States, 
                  keep: States) \
                    -> tuple[States, NFATransitions, States]:
    """
    Remove states that should not be kept.
    Returns tuple (states, transitions, final states).
    """
    new_transitions = NFATransitions()

    for state in keep:
        new_transitions[state] = {}
        for ap, dests in transitions.get(state, {}).items():
            new_transitions[state][ap] = dests & keep

    final_states = final_states & keep

    return keep.copy(), new_transitions, final_states


def compute_backward_reachable(nfa: ExtendedNFA, want_to_reach: States | None = None):
    if want_to_reach is None:
        # automatically computing nondead states
        want_to_reach = nfa.final_states
    
    rev_transitions = nfa.transitions.reverse()
    backward_reachable = compute_reachable_states(rev_transitions, want_to_reach)
    return backward_reachable


def compute_reachable_states(transitions: NFATransitions, start_with: States):
    reachable = start_with
    new_reachable = set()

    while True:
        for s in reachable:
            if s in transitions:
                for dests in transitions[s].values():
                    new_reachable = new_reachable.union(dests)

        if reachable == reachable | new_reachable:
            break

        reachable = reachable | new_reachable
        new_reachable = set()

    return reachable


def remove_unreachable(aut: NFA, must_remain: States = set()) -> NFA:
    """
    Remove unreachable states in an NFA.
    must_remain contains states that must not be removed.
    """
    params = aut.input_parameters
    states = params['states']
    transitions = params['transitions']
    final_states = params['final_states']

    reachable = compute_reachable_states(transitions, 
                                  set([aut.initial_state]) | must_remain)

    params['states'], params['transitions'], params['final_states'] \
         = remove_states(transitions, final_states, reachable)
    return NFA(**params)


def count_edges(aut: NFA) -> int:
    """
    Count all transitions in a given NFA.
    """
    edges = 0

    for transitions in aut.transitions.values():
        for dests in transitions.values():
            edges += len(dests)
    
    return edges


def total_transition_function(states: States, 
                              transitions: DFATransDict, 
                              input_symbols: set[str]):
    """
    Modify the DFA transitions, so that every state 
    has a transition under every symbol.
    """
    sink = unoccuring_state(states)
    states.add(sink)

    for state in states:
        if state not in transitions:
            transitions[state] = {}
        
        for symbol in input_symbols:
            if symbol not in transitions[state]:
                transitions[state][symbol] = sink


def successor_state(states: States) -> int:
    """
    Return an int name of a state that is larger than all other state names.
    Returns 0 if there are no states to compare with.
    """
    if not states:
        return 0
    
    return max([int(state) for state in states]) + 1


def unoccuring_state(states: States, prefix: str = "") -> str:
    """
    Return a string name of a state not occuring in a given state set.
    """
    if prefix not in states and prefix != "":
        return prefix

    num = 0
    while prefix + str(num) in states or (prefix == "" and num in states):
        num += 1
    return prefix + str(num)


def unoccuring_int_state(states: States) -> int:
    """
    Return an int not appearing in a given state set.
    """
    num = 0
    while num in states or str(num) in states:
        num += 1
    return num


def rename_states(nfa: ExtendedNFA | NFA) -> ExtendedNFA:
    """
    Renames states to strings starting at 0, returns equivalent ExtendedNFA.
    """
    state_counter = str_counter(0)
    states: States = set()
    transitions = NFATransitions()
    initial_states: States = set()
    final_states: States = set()

    state_map = rename_states_and_add(nfa, states, transitions, initial_states, 
                          final_states, state_counter)
    
    return ExtendedNFA(states=states,
                       input_symbols=set(nfa.input_symbols),
                       transitions=transitions,
                       initial_states=initial_states,
                       final_states=final_states)


def rename_ports_map(ports_map: PortSetsMap, state_map: dict[State, State]) -> PortSetsMap:
    """
    Given a port sets map and a state map of original states to new states,
    returns the port sets map with renamen states.
    """
    return {port: {state_map[mapped_port] 
                for mapped_port in map_set}
                for port, map_set in ports_map.items()}

    
def rename_states_mapped(ports_nfa: MappedPortsNFA):
    """
    Renames states to strings starting at 0, 
    renames port targets in port maps,
    returns equivalent MappedPortsNFA.
    """
    state_counter = str_counter(0)
    states = set()
    initial_states = set()
    final_states = set()
    transitions = NFATransitions()
    state_map = rename_states_and_add(ports_nfa.nfa, states, transitions, initial_states, 
                                    final_states, state_counter)
    # pprint(pfrozenset.convert_rec(state_map))
    in_map = rename_ports_map(ports_nfa.in_map, state_map)
    out_map = rename_ports_map(ports_nfa.out_map, state_map)

    new_aut = ExtendedNFA(states=states,
                    input_symbols=ports_nfa.nfa.input_symbols,
                    transitions=transitions,
                    initial_states=initial_states,
                    final_states=final_states)
    return MappedPortsNFA(new_aut, in_map, out_map)


def create_state_map(states: States | AbstractSet[Any], start=0, counter=None) \
                      -> dict[State, State]:
    """
    Create a mapping of state names to new ones.
    If an iterator is provided, it is used to create new names.
    If not, names are numbers starting at 0.
    """
    if counter is not None:
        return {state: next(counter) for state in states}
    return {state: num for num, state in enumerate(states, start=start)}


def add_transitions(state_map: dict[State, State], 
                    add_to: NFATransitions, 
                    new_transitions: NFATransitions | NFATransFrozen):
    """
    Add new transitions. Newly added states are mapped using a given state map.
    """
    for src, transitions in new_transitions.items():
        for letter, dsts in transitions.items():
            for dst in dsts:
                add_to.add_edge(letter, state_map[src], state_map[dst])


def single_initial_state_new(aut: ExtendedNFA) -> ExtendedNFA:
    """
    Return a new equivalent ExtendedNFA with a single initial state.
    """
    aut_copy = aut.copy()
    new_init_state = aut_copy.single_initial_state(epsilon_allowed=False)
    return aut_copy
