from aligater.types import *


def decide_direction(nfa: ExtendedNFA, heuristic: Heuristic) -> Direction:
    """
    Given an NFA and a heuristic, decide which direction is better for powerset construction.
    """
    if heuristic == Heuristic.DET_SUCCS:
        heuristic_function = count_deterministic_succs
    elif heuristic == Heuristic.NONDET_STATES:
        heuristic_function = count_nondet_states

    rev_nfa = nfa.reverse()

    forward_value = heuristic_function(nfa)
    reverse_value = heuristic_function(rev_nfa)

    if forward_value >= reverse_value: # less nondeterminism from the back
        return Direction.REVERSE
    return Direction.FORWARD


def decide_direction_port_nfa(ports_nfa: MappedPortsNFA | PortsNFA, heuristic: Heuristic) -> Direction:
    """
    Given a ports NFA and a heuristic, decide which direction is better for powerset construction.
    """
    if heuristic == Heuristic.DET_SUCCS:
        heuristic_function = count_deterministic_succs
    elif heuristic == Heuristic.NONDET_STATES:
        heuristic_function = count_nondet_states
    
    forward_value = heuristic_function(ports_nfa.nfa)
    reverse_value = heuristic_function(ports_nfa.nfa.reverse())

    # to the heuristic value add the sum of sizes of port sets
    # if isinstance(ports_nfa, MappedPortsNFA):
    #     forward_value += sum_mapped_port_sets_size(ports_nfa.in_map)
    #     reverse_value += sum_mapped_port_sets_size(ports_nfa.out_map)
    # elif isinstance(ports_nfa, PortsNFA):
    #     forward_value += sum_port_sets_size(ports_nfa.in_port_sets)
    #     reverse_value += sum_port_sets_size(ports_nfa.out_port_sets)
    
    if forward_value >= reverse_value: # less nondeterminism from the back
        return Direction.REVERSE
    return Direction.FORWARD


def sum_port_sets_size(port_sets: PortSets):
    return sum([len(s) for s in set(port_sets)])

def sum_mapped_port_sets_size(port_sets: PortSetsMap):
    return sum([len(s) for s in set(port_sets.values())])


def count_nondet_transitions(nfa: ExtendedNFA) -> int:
    nondet_transitions = 0 # count nondeterministic transitions from each state

    for src in nfa.transitions:
        for _, dests in nfa.transitions[src].items():
            nondet_transitions += len(dests) - 1
    
    return nondet_transitions


def count_deterministic_succs(nfa: ExtendedNFA) -> int:
    det_successors = 0 # count unique destination sets for each state

    for src in nfa.transitions:
        
        state_det_successors = []

        for _, dests in nfa.transitions[src].items():
            if dests not in state_det_successors:
                state_det_successors.append(dests)

        det_successors += len(state_det_successors)
    
    return det_successors + len(nfa.initial_states)


def count_nondet_states(nfa: ExtendedNFA) -> int:
    nondet_states = 0 # count states with nondeterminism

    for src in nfa.transitions:
        is_nondet_state = False

        for _, dests in nfa.transitions[src].items():
            if len(dests) > 1:
                is_nondet_state = True

        if is_nondet_state:
            nondet_states += 1
    
    return nondet_states