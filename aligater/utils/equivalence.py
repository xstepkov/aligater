from aligater.types import *
from automata.fa.dfa import DFA
import automata.base.exceptions as exc
import aligater.utils.powerset_complement as powc

def assign_initial_state(dfa: DFA, init_state: State) -> DFA:
    params = dfa.input_parameters
    params["initial_state"] = init_state
    new_dfa = DFA(**params)

    return new_dfa


def assign_final_states(dfa: DFA, final_states: States) -> DFA:
    params = dfa.input_parameters
    params["final_states"] = final_states
    new_dfa = DFA(**params)

    return new_dfa


def check_state_init_equivalence(dfa: DFA, state1: State, state2: State) -> bool:
    if state1 == state2:
        return True

    dfa1 = assign_initial_state(dfa, state1)
    dfa2 = assign_initial_state(dfa, state2)

    return dfa1 == dfa2


def check_set_acc_equivalence(dfa: DFA, set1: States, set2: States) -> bool:
    if set1 == set2:
        return True

    dfa1 = assign_final_states(dfa, set1)
    dfa2 = assign_final_states(dfa, set2)

    return dfa1 == dfa2


def check_equal_acc_languages(dfa: DFA, set1: States, set2: States, state_map: PortSetsMap) -> bool:
    """
    Check if two sets of states accept the same languages in a DFA.
    """
    if set1 == set2:
        return True
    
    fin_set1 = state_map[frozenset(set1)]
    fin_set2 = state_map[frozenset(set2)]

    return check_set_acc_equivalence(dfa, fin_set1, fin_set2)


def check_equal_init_languages(dfa: DFA, set1: States, set2: States, state_map: PortSetsMap) -> bool:
    """
    Check it two sets of states produce the same languages in a DFA.
    """
    if set1 == set2:
        return True
    
    init_state_set = state_map[frozenset(set1)]
    init_state1 = next(iter(init_state_set))

    init_state_set = state_map[frozenset(set2)]
    init_state2 = next(iter(init_state_set))

    return check_state_init_equivalence(dfa, init_state1, init_state2)


def check_disjoint_acc_languauges(cross_product: set[tuple[State, State]], 
                                  set1: States, 
                                  set2: States,
                                  state_map: PortSetsMap) -> bool:
    """
    Check if two sets of states accept disjoint languages in a DFA.
    """
    for state1 in state_map[frozenset(set1)]:
        for state2 in state_map[frozenset(set2)]:
            if (state1, state2) in cross_product: # The intersection is not empty
                return False
    return True

def determinize_for_language_check(ports_nfa: PortsNFA) \
                                    -> tuple[DFA, PortSetsMap, PortSetsMap]:
    """
    Creates a DFA that is used for state language equivalence testing.
    """
    det_result = powc.determinize_with_tracking(ports_nfa)

    return det_result.nfa.to_dfa(), det_result.in_map, det_result.out_map


def equivalent_init_languages(nfa: ExtendedNFA, set1: States, set2: States) -> bool:
    frozen_set1, frozen_set2 = frozenset(set1), frozenset(set2)
    entry_sets = [frozen_set1, frozen_set2]
    exit_sets = []
    dfa, init_port_map, _ = determinize_for_language_check(PortsNFA(nfa, entry_sets, exit_sets))

    init_state_set = init_port_map[frozen_set1]
    init_state1 = next(iter(init_state_set))

    init_state_set = init_port_map[frozen_set2]
    init_state2 = next(iter(init_state_set))

    return check_state_init_equivalence(dfa, init_state1, init_state2)


def equivalent_acc_languages(nfa: ExtendedNFA, set1: States, set2: States) -> bool:
    frozen_set1, frozen_set2 = frozenset(set1), frozenset(set2)
    entry_sets = []
    exit_sets = [frozen_set1, frozen_set2]
    dfa, _, fin_port_map = determinize_for_language_check(PortsNFA(nfa, entry_sets, exit_sets))

    init_state_set = fin_port_map[frozen_set1]
    init_set1 = next(iter(init_state_set))

    init_state_set = fin_port_map[frozen_set2]
    init_set2 = next(iter(init_state_set))

    return check_set_acc_equivalence(dfa, init_set1, init_set2)


def validate_with_ports(orig_nfa: ExtendedNFA, new_nfa: ExtendedNFA, 
                        init_port_map: PortSetsMap, fin_port_map: PortSetsMap) -> bool:
    """
    Modifies NFAs in parameters.
    """
    orig_old_initial = orig_nfa.initial_states
    new_old_initial = new_nfa.initial_states

    lib_nfa = orig_nfa.to_nfa()
    lib_min = new_nfa.to_nfa()
    if lib_nfa != lib_min:
        return False

    for orig_set, new_set in init_port_map.items():
        exception1, exception2 = False, False
        orig_nfa.initial_states = set(orig_set)
        try:
            orig_lib_nfa = orig_nfa.to_nfa()
        except exc.MissingStateError:
            print("exception1", orig_set)
            continue

        new_nfa.initial_states = set(new_set)
        try:
            new_lib_nfa = new_nfa.to_nfa()
        except exc.MissingStateError:
            print("exception2")
            continue

        if new_lib_nfa != orig_lib_nfa:
            print(f"not valid because of init sets: {orig_set} (orig) and {new_set} (new)")
            return False
        
    orig_nfa.initial_states = orig_old_initial
    new_nfa.initial_states = new_old_initial
    
    for orig_set, new_set in fin_port_map.items():
        orig_nfa.final_states = set(orig_set)
        try:
            orig_lib_nfa = orig_nfa.to_nfa()
        except exc.MissingStateError:
            print("exception3", orig_set)
            continue

        new_nfa.final_states = set(new_set)
        try:
            new_lib_nfa = new_nfa.to_nfa()
        except exc.MissingStateError:
            print("exception4")
            continue

        if new_lib_nfa != orig_lib_nfa:
            print(f"not valid because of fin sets: {orig_set} (orig) and {new_set} (new), where {orig_nfa.initial_states} and {new_nfa.initial_states} are initial")
            return False
        
    return True
