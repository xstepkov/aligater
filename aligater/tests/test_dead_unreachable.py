from aligater.utils import parser
import os
from .test_equivalence_utils import automata_libs_path
from random import sample, randrange

def test_dead_unreachable():
    dir = os.path.join(automata_libs_path(), "random_small")

    for nfa, name in parser.go_through_directory(dir):
        print(name)
        # init_reachable = set(sample(sorted(nfa.states), randrange(1, 3)))
        # init_nondead = set(sample(sorted(nfa.states), randrange(1, 3)))
        init_reachable = set(sorted(nfa.states)[:2])
        init_nondead = set(sorted(nfa.states)[-2:])

        wo_dead = nfa.remove_dead(starting_nondead=init_nondead.copy())
        wo_unreach = wo_dead.remove_unreachable(init_reachable=init_reachable.copy())
        joint = nfa.remove_dead_and_unreachable(init_nondead=init_nondead, init_reachable=init_reachable)

        assert wo_unreach.states == joint.states