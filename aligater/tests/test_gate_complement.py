from aligater.types import *
from aligater.partitioning.state_equivalence_checker import StateEquivalenceChecker
from aligater.partitioning.gate_checker import GateChecker
from aligater.partitioning.gate_finder import GatePartitioner
from aligater.complement import GateComplement
from aligater.utils import mata_handler, parser
from .test_equivalence_utils import automata_libs_path
import os

def test_gate_complement_single_equal():
    nfa = ExtendedNFA(states = {0, 1, 2, 3},
          input_symbols={"a","b"},
          transitions={
              0 : {"a" : {1}},
              1 : {"b" : {2}},
              2 : {"a" : {3}},
              3 : {"a" : {2}},
          },
          initial_states={0},
          final_states={1, 3})
    eq_check = StateEquivalenceChecker(nfa)
    
    checker = GateChecker(PortsNFA(nfa), {0,1}, {2,3}, eq_check)
    gate_pair = checker.get_gate_comp_pair()
    complementer = GateComplement(PortsNFA(nfa), gate_pair, ComplementationOptions())
    comp = complementer.complement()

    assert mata_handler.validate_complement(nfa, comp.nfa)


def test_gate_complement_single_disjoint_ind():
    nfa = ExtendedNFA(states = {0, 1, 2, 3, 4},
          input_symbols={"a","b"},
          transitions={
              0 : {"a" : {1}},
              1 : {"a" : {4}, "b" : {2}},
              4 : {"b" : {3}},
              2 : {"a" : {3}},
              3 : {"a" : {2}},
          },
          initial_states={0},
          final_states={1, 3})
    eq_check = StateEquivalenceChecker(nfa)
    checker = GateChecker(PortsNFA(nfa), {0,1,4}, {2,3}, eq_check)
    gate_pair = checker.get_gate_comp_pair()
    complementer = GateComplement(PortsNFA(nfa), gate_pair, ComplementationOptions())
    comp = complementer.complement()
    
    assert mata_handler.validate_complement(nfa, comp.nfa)

def test_gate_complement_from_file():
    file = "/home/notme/aligater/automata_libs/random_small/gate_13_r10_hg.dot"
    nfa, name = parser.load_from_file(file)
    nfa = nfa.remove_dead_and_unreachable()
    ports_nfa = PortsNFA(nfa)
    partitioner = GatePartitioner(ports_nfa, 3)
    partitioner.partition()

    for i, gate_pair in enumerate(partitioner.partition()):
        print(i)
        print(gate_pair.type.to_string())
        print(gate_pair.ports_aut1.nfa.states)
        complementer = GateComplement(PortsNFA(nfa), gate_pair, ComplementationOptions())
        comp = complementer.complement()
        assert mata_handler.validate_complement(nfa, comp.nfa)

def test_gate_complement():
    dir = os.path.join(automata_libs_path(), "random_small")

    for nfa, name in parser.go_through_directory(dir):
        print(name)
        ports_nfa = PortsNFA(nfa)
        partitioner = GatePartitioner(ports_nfa, 3)

        for gate_pair in partitioner.partition():
            complementer = GateComplement(PortsNFA(nfa), gate_pair, ComplementationOptions())
            comp = complementer.complement()
            print(gate_pair.type.to_string())
            if not mata_handler.validate_complement(nfa, comp.nfa):
                print(gate_pair.type.to_string())
                print(gate_pair.ports_aut1.nfa.states)
                assert False