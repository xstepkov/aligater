from aligater.types import *
from aligater.utils import parser, equivalence as equiv, minimizing as min, powerset_complement as powc
import os
from .test_equivalence_utils import automata_libs_path, add_random_ports, add_nonrandom_ports

def test_dfa_minimization():
    dir = os.path.join(automata_libs_path(), "random_small")
    # dir = "/home/notme/nfa-bench/benchmarks/email_filter/"

    for nfa, name in parser.go_through_directory(dir):
        print(name)
        ports_nfa = add_random_ports(nfa)
        ports_dfa = powc.determinize_with_tracking(ports_nfa)
        min_nfa = min.minimize_dfa_with_tracking(ports_dfa)
        assert equiv.validate_with_ports(ports_nfa.nfa, min_nfa.nfa, min_nfa.in_map, min_nfa.out_map)


def test_mata_minimization():
    dir = os.path.join(automata_libs_path(), "random_small")
    # dir = "/home/notme/nfa-bench/benchmarks/email_filter/"

    for nfa, name in parser.go_through_directory(dir):
        print(name)
        ports_nfa = add_random_ports(nfa)
        mapped_ports_nfa = MappedPortsNFA(nfa, {pset: set(pset) for pset in ports_nfa.in_port_sets},
                                          {pset: set(pset) for pset in ports_nfa.out_port_sets})

        min_nfa = min.mata_simulation_minimize_with_tracking(mapped_ports_nfa)

        min_nfa.nfa.show_diagram(f"/home/notme/aligater/output/reduced_{name}.png")

        if not equiv.validate_with_ports(ports_nfa.nfa, min_nfa.nfa, min_nfa.in_map, min_nfa.out_map):
            print(min_nfa.in_map)
            assert False
