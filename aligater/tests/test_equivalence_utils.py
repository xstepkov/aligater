from aligater.types import *
from random import randrange, sample
import importlib.resources as resources

def choose_random_subset(choose_from):
    return frozenset(sample(choose_from, randrange(1, len(choose_from))))

def add_random_ports(nfa: ExtendedNFA) -> PortsNFA:
    sorted_states = sorted(nfa.states)
    in_port_sets: PortSets = []
    out_port_sets: PortSets = []

    for _ in range(randrange(1, 5)):
        in_port_sets.append(choose_random_subset(sorted_states))

    for _ in range(randrange(1, 5)):
        out_port_sets.append(choose_random_subset(sorted_states))

    return PortsNFA(nfa, in_port_sets, out_port_sets)

def add_nonrandom_ports(nfa: ExtendedNFA) -> PortsNFA:
    sorted_states = sorted(nfa.states)
    in_port_sets: PortSets = []
    out_port_sets: PortSets = []

    in_port_sets.append(frozenset(sorted_states[:3]))
    out_port_sets.append(frozenset(sorted_states[-3:]))

    return PortsNFA(nfa, in_port_sets, out_port_sets)

def automata_libs_path() -> str:
    return str(resources.files("aligater") / '../automata_libs')
