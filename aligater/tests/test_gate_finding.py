from aligater.partitioning.state_equivalence_checker import StateEquivalenceChecker
from aligater.partitioning.gate_checker import GateChecker
from aligater.types import *

def test_state_equivalence_checker():
    nfa = ExtendedNFA(states = {0, 1, 2, 3},
          input_symbols={"a"},
          transitions={
              0 : {"a" : {1}},
              1 : {"a" : {2}},
              2 : {"a" : {3}},
              3 : {"a" : {0}},
          },
          initial_states={0},
          final_states={1, 3})
    
    checker = StateEquivalenceChecker(nfa)
    assert checker.are_init_equivalent({0}, {2})
    assert checker.are_init_equivalent({0,1}, {2,3})
    assert not checker.are_init_equivalent({0}, {1})
    assert not checker.are_acc_equivalent({0,2}, {0})
    assert checker.m_nfa1.initial_states == [0]
    assert checker.m_nfa1.final_states == [1,3]

    checker.assign_final_states({2})
    assert checker.are_init_equivalent({2}, {2})
    assert checker.are_acc_equivalent({3,1}, {3,1})
    assert not checker.are_init_equivalent({0}, {2})

    checker.assign_initial_states({0,2})
    assert checker.are_acc_equivalent({1}, {3})
    assert checker.are_acc_equivalent({1}, {1,3})
    assert not checker.are_acc_disjoint({1}, {3})

    checker.assign_initial_states({0,2})
    assert checker.are_acc_disjoint({1}, {2})

def test_state_equivalence_checker_2():
    nfa = ExtendedNFA(states = {0, 1, 2},
          input_symbols={"a"},
          transitions={
              0 : {"a" : {1, 2}},
              1 : {"a" : {1, 2}},
              2 : {"a" : {1}},
          },
          initial_states={0},
          final_states=set())
    
    checker = StateEquivalenceChecker(nfa)
    assert checker.are_acc_disjoint({0}, {1})
    assert checker.are_acc_disjoint({0}, {2})
    assert not checker.are_acc_disjoint({1}, {2})

def test_gate_checker_equal():
    nfa = ExtendedNFA(states = {0, 1, 2, 3},
          input_symbols={"a","b"},
          transitions={
              0 : {"a" : {1}},
              1 : {"b" : {2}},
              2 : {"a" : {3}},
              3 : {"a" : {2}},
          },
          initial_states={0},
          final_states={1, 3})
    eq_check = StateEquivalenceChecker(nfa)
    
    checker = GateChecker(PortsNFA(nfa), {0,1}, {2,3}, eq_check)
    assert checker.is_gate
    gate_type = checker.get_type()
    assert gate_type.is_basic()
    assert gate_type.is_equal()

def test_gate_checker_disjoint_ind():
    nfa = ExtendedNFA(states = {0, 1, 2, 3, 4},
          input_symbols={"a","b"},
          transitions={
              0 : {"a" : {1}},
              1 : {"a" : {4}, "b" : {2}},
              4 : {"b" : {3}},
              2 : {"a" : {3}},
              3 : {"a" : {2}},
          },
          initial_states={0},
          final_states={1, 3})
    eq_check = StateEquivalenceChecker(nfa)
    
    checker = GateChecker(PortsNFA(nfa), {0,1,4}, {2,3}, eq_check)
    assert checker.is_gate
    gate_type = checker.get_type()
    assert gate_type.is_basic()
    assert gate_type.is_disjoint_ind()

def test_gate_checker_disjoint_grp():
    nfa = ExtendedNFA(states = {0, 1, 2, 3, 4},
          input_symbols={"a","b"},
          transitions={
              0 : {"a" : {1, 4}},
              1 : {"a" : {4}, "b" : {2}},
              4 : {"b" : {3}},
              2 : {"a" : {3}},
              3 : {"a" : {2}},
          },
          initial_states={0},
          final_states={1, 3})
    eq_check = StateEquivalenceChecker(nfa)
    
    checker = GateChecker(PortsNFA(nfa), {0,1,4}, {2,3}, eq_check)
    assert checker.is_gate
    gate_type = checker.get_type()
    assert gate_type.is_basic()
    assert gate_type.is_disjoint_ind()