import xml.etree.ElementTree as ET
import pandas as pd
import os
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import sys
from collections.abc import Iterable


def write_to_xml(data_dict: dict, path: str):
    root = ET.Element("automaton")

    for key, value in data_dict.items():
        child_element = ET.SubElement(root, key)
        child_element.text = str(value)

    tree = ET.ElementTree(root)
    with open(path, "ab") as file:
        ET.indent(tree, space="  ", level=0)
        tree.write(file)


def print_lines(data):
    for line in data:
        print(line)


def select_not_none(vals):
    if all(pd.isna(vals)):
        return pd.NA
    return next(iter([val for val in vals if pd.notna(val)]))


def xml_to_df(xml_path: str, group_by: str | None = "name" ):
    df = pd.read_xml(xml_path)

    if group_by is not None:
        df_merged = df.groupby('name').agg(func=select_not_none).reset_index()
        return df_merged
    return df


def xml_to_csv(xml_path: str, csv_path: str):
    try:
        df = xml_to_df(xml_path)
    except Exception:
        print(f"Could not translate xml file {xml_path}, perhaps it is empty or unclosed.", file=sys.stderr)
        return
    
    df.to_csv(csv_path, index=False)


def translate_untranslated_xmls_to_csv(xml_dir: str, csv_dir):
    for xml_filename in os.listdir(xml_dir):
        xml_path = os.path.join(xml_dir, xml_filename)
        csv_filename = xml_filename.replace("xml", "csv")
        csv_path = os.path.join(csv_dir, csv_filename)

        if not os.path.isfile(csv_path):
            xml_to_csv(xml_path, csv_path)


def mode_to_str(mode: str):
    if mode == "p":
        return "port_"
    elif mode == "g":
        return "gate_"
    elif mode == "f":
        return "pow_"
    return ""

def partition_to_str(partition_mode: str):
    if partition_mode == "b":
        return "scc_"
    elif partition_mode == "r":
        return "lrev_"
    elif partition_mode == "d":
        return "det_"
    elif partition_mode == "m":
        return "min_cut_"
    elif partition_mode == "dr":
        return "det_lrev_"
    return ""

def connect_to_str(connect: str):
    return connect + "_"

def get_stats_file_name(base_name: str, mode: str, options: list[str],
                        partition: str, connect: str) -> str:
    opt_copy = options.copy()
    if "r" not in opt_copy:
        opt_copy = ["f"] + opt_copy
    else:
        opt_copy.remove("r")
        opt_copy = ["r"] + opt_copy
        
    return base_name + "_" + mode_to_str(mode) + partition_to_str(partition) \
            + connect_to_str(connect) +  "_".join(opt_copy)


def merge_dataframes(dfs: list[pd.DataFrame], on_columns = ["name"], how='outer') -> pd.DataFrame:
    merged_df = dfs[0]

    for df in dfs[1:]:
        merged_df = pd.merge(merged_df, df, on=on_columns, how=how)

    return merged_df


def cols_to_numeric(data: pd.DataFrame, columns: list[str]):
    data.replace("ERROR", pd.NA, inplace=True)
    data[columns] = data[columns].apply(pd.to_numeric)


def concat_data_files_from_iterable(source_files: Iterable[str], 
                                    name_prefixes: list[str] | None = None,
                                    read_csv: bool = True, 
                                    read_xml: bool = True, 
                                    output_file: str | None = None,
                                    merge_xml_records: bool = True):
    dfs = []

    # Iterate through all XML files in the specified directory
    for i, file_path in enumerate(source_files):
        df = None

        if read_xml and file_path.endswith('.xml'):
            if merge_xml_records:
                df = xml_to_df(file_path, group_by=None)
            else:
                df = xml_to_df(file_path)
            
        elif read_csv and file_path.endswith('.csv'):
            df = pd.read_csv(file_path)

        if df is not None:
            name_prefix = ""
            if name_prefixes is not None:
                name_prefix = name_prefixes[i]

            df['name'] = name_prefix + df['name'].astype(str)
            dfs.append(df)

    # Concatenate DataFrames into a single DataFrame
    combined_df = pd.concat(dfs, ignore_index=True)

    # Save the combined DataFrame to CSV
    if output_file is not None:
        if output_file.endswith(".csv"):
            combined_df.to_csv(output_file, index=False)
        elif output_file.endswith(".xml"):
            combined_df.to_csv(output_file, index=False)
        else:
            raise ValueError(f"Cannot write dataframe to {output_file}, unrecognized extension.")

    return combined_df


def concat_data_files_from_dir(source_dir: str, 
                               read_csv: bool = True, 
                               read_xml: bool = True, 
                               output_file: str | None = None,
                               merge_xml_records: bool = True):
    source_files = []
    for filename in os.listdir(source_dir):
        file_path = os.path.join(source_dir, filename)
        source_files.append(file_path)
    
    return concat_data_files_from_iterable(source_files, read_csv=read_csv, read_xml=read_xml, 
                                           output_file=output_file, merge_xml_records=merge_xml_records)


def min_from_columns(df: pd.DataFrame, columns: list[str], new_col_name: str, origin_col_name: str):
    columns_filtered = [col for col in columns if col in df.columns]
    if columns_filtered:
        df[new_col_name] = df[columns_filtered].min(axis=1, skipna=True)
        df[origin_col_name] = df[columns_filtered].idxmin(axis=1, skipna=True)


def max_from_columns(df: pd.DataFrame, columns: list[str], new_col_name: str):
    columns_filtered = [col for col in columns if col in df.columns]
    df[new_col_name] = df[columns_filtered].max(axis=1)
    df['column_with_max'] = df[columns_filtered].idxmax(axis=1)


def extension(path: str):
    _, file = os.path.split(path)
    name, ext = os.path.splitext(file)
    return ext


def extract_name_wo_ext(path):
    _, file = os.path.split(path)
    name, ext = os.path.splitext(file)
    return name


def load_dataframes(dir: str):
    dfs = []
    return load_dataframes_from_iterable([filename.path for filename in os.scandir(dir)])


def load_dataframes_from_iterable(paths: Iterable[str]):
    dfs = []
    for path in paths:
        ext = extension(path)

        df = None
        if ext == ".csv":
            df = pd.read_csv(path)
        elif ext == ".xml":
            df = pd.read_xml(path)

        if df is not None:
            dfs.append(df)

    return dfs
        

def compose_into_one_csv(dir: str, merged_csv_path: str | None = None, drop_columns: list[str] = [], merge_how='outer'):
    dfs = load_dataframes(dir)
    dfs = remove_useess_columns(dfs, drop_columns)
    merged_df = merge_dataframes(dfs, how=merge_how)

    if merged_csv_path is None:
        merged_csv_path = os.path.join(dir, "all.csv")
    merged_df.to_csv(merged_csv_path, index=False)


def remove_useess_columns(dfs: list[pd.DataFrame], useless_columns: list[str]) -> list[pd.DataFrame]:
    updated_dfs = []
    for df in dfs:
        for column_name in useless_columns:
            if column_name in df.columns:
                df = df.drop(column_name, axis=1)
        updated_dfs.append(df)

    return updated_dfs


def set_ticks(ax, upper_limit: int | None = None, lower_limit: int = 0, error_val: float | None = None):
    if error_val is not None:
        error_val = int(error_val)

    ticks = list(ax.get_xticks())
    ticks = [val for val in ticks if val >= lower_limit]

    if upper_limit is not None:
        ticks = [val for val in ticks if val <= upper_limit]
        if upper_limit not in ticks:
            ticks.append(upper_limit)
    
    if error_val is not None:
        ticks = [val for val in ticks if val <= error_val]
        ticks.append(error_val)

    ax.set_xticks(ticks)

    ticks = list(ax.get_xticks())
    ticks = [str(int(val)) for val in ticks]

    if upper_limit is not None:
        print(ticks)
        ticks[ticks.index(str(upper_limit))] = '>' + str(upper_limit)

    if error_val is not None:
        ticks[ticks.index(str(error_val))] = 'TO+MO'
    ax.set_xticklabels(ticks)


    ticks = list(ax.get_yticks())
    ticks = [val for val in ticks if val >= lower_limit]

    if upper_limit is not None:
        ticks = [val for val in ticks if val <= upper_limit]
        if upper_limit not in ticks:
            ticks.append(upper_limit)

    if error_val is not None:
        ticks = [val for val in ticks if val <= error_val]
        ticks.append(error_val)

    ax.set_yticks(ticks)

    ticks = list(ax.get_yticks())
    ticks = [str(int(val)) for val in ticks]

    if upper_limit is not None:
        ticks[ticks.index(str(upper_limit))] = '>' + str(upper_limit)

    if error_val is not None:
        ticks[ticks.index(str(error_val))] = 'TO+MO'
    ax.set_yticklabels(ticks)


def graph_basic(data: pd.DataFrame, x_col: str, y_col: str, 
                x_col_name: str, y_col_name: str, 
                plot_limit: int | None = None,
                logscale: bool = False,
                hue: str | None = None,
                hue_order: list[str] | None = None):
    low_bound = 1 if logscale else 0
    color = "tab:blue"
    error_color = "tab:red"
    dot_transparency = 0.2

    fig, ax = plt.subplots()
    
    data = data.copy()
    max_val = np.max([data[x_col].max(), data[y_col].max()])

    # limits
    add_custom_ticks = False
    if plot_limit is not None:
        if plot_limit < max_val:
            for col in [x_col, y_col]:
                data[col] = data[col].mask(data[col] > plot_limit, plot_limit)

            vals = np.repeat([plot_limit], 1000)
            rang = np.linspace(low_bound, plot_limit, 1000)
            ax.plot(vals, rang, "k--", linewidth=0.5)
            ax.plot(rang, vals, "k--", linewidth=0.5)

            max_val = plot_limit
            add_custom_ticks = True
            
        else:
            plot_limit = None

    # handle NaN values
    error_val = None
    data_error = data[data[x_col].isna() | data[y_col].isna()]
    # print(data_error.empty)
    if data_error is not None and not data_error.empty:
        error_val = 1.1 * max_val
        # print(error_val)
        data_error = data_error.copy()

        for col in [x_col, y_col]:
            data[col] = data[col].mask(data[col].isna(), error_val)
            # data_error[col] = data_error[col].mask(~data_error[col].str.isnumeric(), error_val)

        vals = np.repeat([error_val], 1000)
        rang = np.linspace(low_bound, error_val, 1000)
        ax.plot(vals, rang, "k--", linewidth=0.5)
        ax.plot(rang, vals, "k--", linewidth=0.5)

        # sns.scatterplot(data=data_error, x=x_col, y=y_col, ax=ax, color=error_color)
        add_custom_ticks = True

    if hue is None:
        plot = sns.scatterplot(data=data, x=x_col, y=y_col, ax=ax, color=color, alpha=dot_transparency)
    else:
        plot = sns.scatterplot(data=data, x=x_col, y=y_col, ax=ax, hue=hue, alpha=dot_transparency, hue_order=hue_order)

    # draw middle line
    line = np.linspace(0, max_val, 1000)
    ax.plot(line, line, "-r")

    # set axis names
    ax.set_xlabel(x_col_name)
    ax.set_ylabel(y_col_name)

    if logscale:
        ax.set(xscale="log", yscale="log")

    ax.set_xlim(xmin=low_bound)
    ax.set_ylim(ymin=low_bound)

    # set tick names
    if add_custom_ticks:
        set_ticks(ax, plot_limit, low_bound, error_val)


def convert_cols_to_bool(df: pd.DataFrame, columns: list[str]):
    for col in columns:
        df[col] = df[col].astype(bool)

def store_names_to_file(df: pd.DataFrame, file_path: str):
    with open(file_path, "w") as file:
        for name in df["name"]:
            file.write(name + "\n")

def parse_xml_to_dataframe(xml_file):
    tree = ET.parse(xml_file)
    root = tree.getroot()

    data = []
    seen_names = set()

    for record in root:
        record_data = {}
        for element in record:
            if element.tag == 'name':
                name = element.text
                if name not in seen_names:
                    seen_names.add(name)
                    record_data['name'] = name
                else:
                    continue
            else:
                record_data[element.tag] = element.text
        data.append(record_data)

    df = pd.DataFrame(data)
    return df


if __name__ == "__main__":
    directory = "/home/xstepkov/aligater/src/experiments/csv_files/automatark"
    compose_into_one_csv(directory, drop_columns=["success", "Unnamed: 0"])

