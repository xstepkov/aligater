import sys
import os
import mmap
aligater_path = os.path.dirname(os.path.dirname(__file__))
sys.path.append(aligater_path)

from aligater.types import *
from aligater.utils import parser
import exp_utils
from aligater import partitioning
from aligater.partitioning.partitioning import partition_into_comp_infos
import signal
import argparse
import datetime


def parse_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument('directory', type=str, help='Directory with benchmarks')
    parser.add_argument('xml_path', type=str, help='XML file for results')
    parser.add_argument('--timeout', type=int, default=60, help='Timeout in seconds')

    return parser.parse_args()


def count_transitions(nfa: ExtendedNFA) -> tuple[int, int, int, int, int, int]:
    transitions = 0
    nondet_transitions = 0 # count transitions from each state
    det_successors = 0 # count unique destination sets for each state
    det_successors_size = 0
    state_successors = 0 # count unique states to which there is a transition from each state
    nondet_states = 0 # count states with nondeterminism

    for src in nfa.transitions:
        
        symbol_count = 0
        is_nondet_state = False
        state_det_successors = []

        for symbol, dests in nfa.transitions[src].items():
            nondet_transitions += len(dests) - 1
            transitions += len(dests)

            if len(dests) > 0:
                symbol_count += 1

            if len(dests) > 1:
                is_nondet_state = True

            if dests not in state_det_successors:
                state_det_successors.append(dests)

        if is_nondet_state:
            nondet_states += 1

        det_successors += len(state_det_successors)
        det_successors_size += sum(map(len, state_det_successors))

        state_successors += len(set(nfa.transitions.iterate_dests_only(src)))
    
    return transitions, nondet_transitions, det_successors, det_successors_size, nondet_states, state_successors


def signal_handler(signum, frame):
    raise Exception()


def count_sccs(nfa: ExtendedNFA, data, timeout):
    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(timeout)
    try:
        sccs = partitioning.find_sccs(nfa)
    except Exception:
        return
    signal.alarm(0)
    
    scc_sizes = [len(scc) for scc in sccs]
    one_state_sccs = len([scc for scc in sccs if len(scc) == 1])

    data["scc_count"] = len(sccs)

    if len(sccs) > 0:
        data["min_scc_size"] = min(scc_sizes)
        data["max_scc_size"] = max(scc_sizes)
        data["avg_scc_size"] = sum(scc_sizes) / len(scc_sizes)
        data["one_state_sccs"] = one_state_sccs

    # partitioning does not make sense if there is only 1 scc
    if len(sccs) > 1:
        det_partition = partition_into_comp_infos(PortsNFA(nfa), sccs, method=PortPartitionMode.DETERMINISTIC)
        data["det_components_count"] = len(det_partition)

        lrev_partition = partition_into_comp_infos(PortsNFA(nfa), sccs, method=PortPartitionMode.LREV)
        data["bottom_rev_det_component_size"] = len(lrev_partition[-1].ports_nfa.nfa.states)

        mincut_partition = partition_into_comp_infos(PortsNFA(nfa), sccs, method=PortPartitionMode.MIN_CUT)
        data["min_cut_last_component_size"] = len(mincut_partition[-1].ports_nfa.nfa.states)

        min_cut_edges = 0
        second_comp_states = mincut_partition[1].ports_nfa.nfa.states

        for port in mincut_partition[0].exit_ports:
            for symbol in nfa.input_symbols:
                min_cut_edges += len(second_comp_states.intersection(nfa.transitions.iterate(port, symbol)))
            
        data["min_cut_edges"] = min_cut_edges


def get_statistics(source: str, xml_path: str, timeout: int):

    # prepare xml file
    if os.path.isfile(xml_path):
        # do not overwrite the file if it already exists
        with open(xml_path, "r") as xml_file:
            for line in xml_file:
                pass
            
            if "</data>" in line:
                print(f"The xml file {xml_path} already exists and is finalized, aborting.")
                return

    else:
        xml_file = open(xml_path, "w")
        xml_file.write("<data>\n")
        xml_file.close()

    # prepare file paths
    if os.path.isdir(source):
        file_paths = [filename.path for filename in os.scandir(source)]
    elif os.path.isfile(source):
        source_file = open(source, "r")
        file_paths = [line.strip() for line in source_file.readlines() if not line.startswith("#")]
        source_file.close()
    else:
        print("invalid source, not file or directory")
        return
    
    # get files to skip
    files_to_skip = []
    with open(xml_path, "r") as xml_file:
        with mmap.mmap(xml_file.fileno(), 0, access=mmap.ACCESS_READ) as s:
            for file_path in file_paths:
                _, name = os.path.split(file_path)
                if s.find(name.encode('utf-8')) != -1:
                    files_to_skip.append(file_path)

    file_paths = [path for path in file_paths if path not in files_to_skip]

    for nfa, name in parser.load_from_iterable(file_paths):
        print(datetime.datetime.now().strftime('%H:%M:%S'), name)

        nfa = nfa.remove_dead_and_unreachable()

        data = {"name": name,
                "state_count": len(nfa.states),
                "init_state_count": len(nfa.initial_states),
                "fin_state_count": len(nfa.final_states),
                "alphabet_size": len(nfa.input_symbols)}

        trans_num, nondet_trans_num, det_successors, det_successors_size, nondet_states, successors = count_transitions(nfa)
        data["transitions_count"] = trans_num
        data["nondet_trans_count"] = nondet_trans_num
        data["deterministic_succs_count"] = det_successors
        data["deterministic_succs_size_sum"] = det_successors_size
        data["nondet_states_count"] = nondet_states
        data["state_successors_count"] = successors

        data["is_deterministic"] = nfa.is_deterministic()
        data["is_complete"] = nfa.is_complete()

        rev_nfa = nfa.reverse()
        _, rev_nondet_trans_num, rev_det_successors, rev_det_successors_size, rev_nondet_states, rev_successors = count_transitions(rev_nfa)
        data["is_rev_deterministic"] = rev_nfa.is_deterministic()
        data["rev_nondet_trans_count"] = rev_nondet_trans_num
        data["rev_deterministic_succs_count"] = rev_det_successors
        data["rev_deterministic_succs_size_sum"] = rev_det_successors_size
        data["rev_nondet_states_count"] = rev_nondet_states
        data["rev_state_successors_count"] = rev_successors

        count_sccs(nfa, data, timeout)

        exp_utils.write_to_xml(data, xml_path)

    with open(xml_path, "a") as file:
        file.write("</data>\n")

    csv_path = xml_path.replace("xml", "csv")
    exp_utils.xml_to_csv(xml_path, csv_path)


def main():
    args = parse_arguments()

    xml_path = args.xml_path
    dir = args.directory
    timeout = args.timeout

    get_statistics(dir, xml_path, timeout)

    print(f"Data stored to {xml_path}")


if __name__ == "__main__":
    main()
    # get_statistics("/home/notme/aligater/experiments/benchmark_groups/wrong.txt", "/home/notme/aligater/experiments/xml_files/test/1stats.xml", 2)
    pass