import os
import sys
from aligater.utils.parser import load_from_file


def export_pic(file_path, output_path):
    result = load_from_file(file_path)
    if result is None:
        print("Could not load from file.", file=sys.stderr)
        return

    nfa, name = result
    nfa.show_diagram(output_path)


def process_directory(source_path, output_path):
    for filename in os.listdir(source_path):

        source_file_path = os.path.join(source_path, filename)
        out_file_path = os.path.join(output_path, filename)

        export_pic(source_file_path, out_file_path)


def process_file(source_path, out_path):
    if os.path.isdir(out_path):
        _, file = os.path.split(source_path)
        name, ext = os.path.splitext(file)
        out_path = os.path.join(out_path, name + ".png")

    export_pic(source_path, out_path)


def main():
    if len(sys.argv) != 3:
        print("Usage: python3 export_pics.py <src_path> <dst_path>, paths can be files or directories.")
        sys.exit(1)

    path1 = sys.argv[1]
    path2 = sys.argv[2]

    if os.path.isdir(path1):
        if not os.path.isdir(path2):
            print("Invalid second path.")
        process_directory(path1, path2)
    elif os.path.isfile(path1):
        process_file(path1, path2)
    else:
        print("Invalid path. Please provide a valid directory or file path.")

if __name__ == "__main__":
    main()

