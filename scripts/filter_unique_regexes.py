import os
import shutil

def filter_regexes(dir: str, output_dir: str):
    appeared_regexes = set()
    distinct_count = 0
    all_count = 0

    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    for root, _, files in os.walk(dir):
        for file in files:
            if file.endswith(".mata"):
                all_count += 1
                file_full_path = os.path.join(root, file)

                regex = ""
                with open(file_full_path, "r") as aut_file:
                    regex = aut_file.readline()

                if regex not in appeared_regexes:
                    distinct_count += 1

                    appeared_regexes.add(regex)

                    regex_set = root.split("/")[-2]
                    output_file_name = regex_set + "_" + file.replace("/", "_")
                    output_file = os.path.join(output_dir, output_file_name)

                    shutil.copyfile(file_full_path, output_file)

    print(f"{distinct_count} distinct regexes chosen out of {all_count} in total")


if __name__ == "__main__":
    # Define input directories
    dir = "/home/notme/nfa-bench/benchmarks/regexps/"
    output_dir = "/home/notme/nfa-bench/benchmarks/regexps_distinct/"

    filter_regexes(dir, output_dir)

