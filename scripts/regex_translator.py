import os
import subprocess
import re
import codecs


def add_self_loops_manually(regex: str) -> str:
    # the mata compiler does not add Sigma^* to the beginning and end of the regex
    # and behaves weirdly when there is ^ at the beginning and | somewhere in the middle
    prefix = ""
    suffix = ""

    if regex.startswith("^"):
        regex = regex[1:]
    elif not regex.startswith(".*"):
        prefix = ".*"
    
    if regex.endswith("$"):
        regex = regex[:-1]
    elif not regex.endswith(".*"):
        suffix = ".*"

    if prefix != "" or suffix != "":
        regex = f"{prefix}({regex}){suffix}"

    return regex


def pcre_to_re2(regex: str):
    split = regex.split("/")
    return "/".join(split[1:-1])


def translate_regex(input_file, output_file):
    # Open the input file and read all regexes line by line
    with open(input_file, 'r') as infile:
        regexes = infile.readlines()
    
    translated_regexes = []
    
    for regex in regexes:
        regex = regex.strip()
        if regex and not regex.startswith("#"):

            translated_regex = pcre_to_re2(regex)
            translated_regex = add_self_loops_manually(translated_regex)
            translated_regexes.append(translated_regex)

    # Write all translated regexes to the output file
    with open(output_file, 'w') as outfile:
        outfile.write("\n".join(translated_regexes) + "\n")


def copy_directory_with_translation(dest_dir):
    
    # Iterate through the copied directory
    for root, _, files in os.walk(dest_dir):
        for file in files:
            if file.endswith('.pcre') or file.endswith('.reg'):
                pcre_file_path = os.path.join(root, file)
                print(pcre_file_path)
                re2_file_path = os.path.join(root, file.replace('.pcre', '.re2'))
                
                # Translate the PCRE regex to RE2
                translate_regex(pcre_file_path, re2_file_path)


if __name__ == "__main__":
    # Define your source and destination directories
    directory = "/home/notme/nfa-bench/benchmarks/regexps/"
    
    # Run the translation and copy
    copy_directory_with_translation(directory)
