from aligater.types import *
from typing import Any
from collections.abc import Iterator
from random import choice, sample, randrange, random, randint
from automata.fa.nfa import NFA
from aligater.utils import parser, generic as utils
from itertools import count, product
from math import ceil, log

# Various generators for NFAs, gate NFAs, ...

class CFG():
    def __init__(self,
                 first: str,
                 productions: dict[str,
                                   list[str]],
                 alphabet: set[str]):
        """
        Suppose there are no epsilon-productions or simple productions.
        """
        self.alphabet = alphabet
        self.first_nonterm: str = first
        self.productions: dict[str, list[tuple[str, str] | str]] = \
            self._to_cnf(productions)
        self.word_counts = None
        self.precomputed_for = None

        assert first in productions

    def _process_part(self, part, todo, processed):
        if part in self.alphabet and "x" + part not in processed:
            todo.append(("x" + part, part))
            part = "x" + part
        elif part not in processed:
            todo.append((part, part))
        return part

    def _to_cnf(self, productions) \
                -> dict[str, list[tuple[str, str] | str]]:
        new_productions: dict[str, list[tuple[str, str] | str]] = {}
        processed = set()

        todo = []
        for nonterm, prods in productions.items():
            for prod in prods:
                todo.append((nonterm, prod))

        while todo:
            nonterm, result = todo.pop()
            processed.add(nonterm)

            if result in self.alphabet:
                new_productions[nonterm] = new_productions.get(nonterm, []) + [result]
            else:
                part1, part2 = result[:1], result[1:]

                part1 = self._process_part(part1, todo, processed)
                part2 = self._process_part(part2, todo, processed)

                new_productions[nonterm] = new_productions.get(
                    nonterm, []) + [(part1, part2)]

        return new_productions

    # generating based on
    # https://reader.elsevier.com/reader/sd/pii/0020019094900337?token=6A069898C9D8165C8762B028F80874B794739ABDBEFC835889AB7DE3C5DCC8167877DB409DF5D21765F5279EC374AF61&originRegion=eu-west-1&originCreation=20230326163552

    def _prod_word_count(self, struct, nonterm_b: str, nonterm_c: str, j: int):
        return sum(
            [struct[nonterm_b][i] * struct[nonterm_c][j - i]
                for i in range(1, j)
             ])

    def initialize_generating(self, n: int):
        # suppose productions are in Chomsky Normal Form
        word_counts: dict = {
            nonterm: [
                0 for _ in range(
                    n +
                    1)] for nonterm in self.productions}

        for nonterm in self.productions:
            for prod in self.productions[nonterm]:
                if prod in self.alphabet:
                    word_counts[nonterm][1] += 1

        for j in range(1, n + 1):
            for nonterm_a, prods in self.productions.items():
                for prod in prods:
                    if isinstance(prod, tuple):
                        nonterm_b, nonterm_c = prod
                        word_counts[nonterm_a][j] += self._prod_word_count(
                            word_counts, nonterm_b, nonterm_c, j)

        return word_counts

    def generate_random_rec(self, n: int, nonterm_a) -> str:
        if n == 1:
            terms = [
                prod for prod in self.productions[nonterm_a] 
                    if isinstance(prod, str)]
            return choice(terms)

        threshold = random()
        current_rand_val = 0.0

        for prod in self.productions[nonterm_a]:
            if isinstance(prod, tuple):
                nonterm_b, nonterm_c = prod
                prod_word_count = self._prod_word_count(
                    self.word_counts, nonterm_b, nonterm_c, n)
                production_prob = prod_word_count / \
                    self.word_counts[nonterm_a][n]

                current_rand_val += production_prob
                if current_rand_val >= threshold:  # choose this production

                    # choose this split for k
                    split_threshold = random()
                    current_split_rand_val = 0.0
                    for k in range(1, n):
                        split_prob = ((self.word_counts[nonterm_b][k] * 
                                      self.word_counts[nonterm_c][n - k])
                                      / prod_word_count)

                        current_split_rand_val += split_prob
                        if current_split_rand_val >= split_threshold:
                            return self.generate_random_rec(k, nonterm_b) \
                                   + self.generate_random_rec(n - k, nonterm_c)

    def generate_random(self, n: int) -> str:
        if self.word_counts is None or (self.precomputed_for is not None 
                                        and self.precomputed_for < n):
            self.precomputed_for = n
            self.word_counts = self.initialize_generating(n)
        return self.generate_random_rec(n, self.first_nonterm)


def clean_regex(regex: str) -> str:
    while "**" in regex:
        regex = regex.replace("**", "*")
    return regex


def generate_from_regex(regex_length: int, 
                        alphabet: set[str] = {"a", "b"}) \
                            -> tuple[NFA, str]:
    """
    Generates a regex, translates it into an NFA, and removes epsilon-transitions.
    """
    alpha_list = list(alphabet)
    grammar = CFG("E",
                  {"E": ["(EE)", "(E|E)", "E*"] + alpha_list},
                  {"(", ")", "*", "|"} | alphabet)

    regex = grammar.generate_random(regex_length)
    regex = clean_regex(regex)
    aut = NFA.from_regex(regex)
    aut_no_eps = aut.eliminate_lambda()

    return aut_no_eps, regex


def generate_new_transitions_equal(aut1: NFA, 
                                   aut2: NFA, 
                                   gate_symbols: set[str]) \
                                    -> set[tuple[State, str, State]]:
    new_transitions: set[tuple[State, str, State]] = set()
    state_list1 = list(aut1.states)
    state_list2 = list(aut2.states)

    for symbol in gate_symbols:
        num_ports1 = randint(1, max(len(aut1.states) // 2, 1))
        num_ports2 = randint(1, max(len(aut2.states) // 2, 1))

        exit_ports1 = sample(state_list1, num_ports1)
        entry_ports2 = sample(state_list2, num_ports2)

        for src in exit_ports1:
            for dst in entry_ports2:
                new_transitions.add((src, symbol, dst))

    return new_transitions


# Both disjoint functions are obsolete

def generate_new_transitions_disjoint2(aut1: NFA,
                                       aut2: NFA,
                                       gate_symbols: set[str],
                                       state_counter: Iterator) \
                                        -> tuple[set[tuple[State,
                                                           str,
                                                           State]],
                                                 set[State]]:
    new_transitions: set[tuple[State, str, State]] = set()
    new_states: set[State] = set()

    state_list1 = list(aut1.states)
    state_list2 = list(aut2.states)

    # generate new exit ports in A_1 that accept pairwise disjoint languages
    for gate_symbol in gate_symbols:

        if len(aut1.input_symbols) > 1:
            gates_num = randint(1, max(len(aut1.states) // 2, 1))
            chains_length = ceil(log(gates_num, len(aut1.input_symbols)))
        else:
            gates_num = 1
            chains_length = 0
        print(gate_symbol, gates_num)

        symbol_chains = product(aut1.input_symbols, repeat=chains_length)

        for _ in range(gates_num):
            symbol_chain = next(symbol_chains)
            prev_state = choice(state_list1)

            for symbol in symbol_chain:
                next_state = next(state_counter)
                new_states.add(next_state)

                new_transitions.add((prev_state, symbol, next_state))
                prev_state = next_state

            entry_port = choice(state_list2)
            new_transitions.add((prev_state, gate_symbol, entry_port))

    return new_transitions, new_states


def generate_new_transitions_disjoint1(aut1: NFA, 
                                       aut2: NFA, 
                                       gate_symbols: set[str]) \
                                        -> set[tuple[State, str, State]]:
    new_transitions: set[tuple[State, str, State]] = set()
    state_list1 = list(aut1.states)
    state_list2 = list(aut2.states)

    for symbol in gate_symbols:
        num_ports1 = randint(1, max(len(aut1.states) // 2, 1))
        num_ports2 = 1

        exit_ports1 = sample(state_list1, num_ports1)
        entry_ports2 = sample(state_list2, num_ports2)

        for src in exit_ports1:
            for dst in entry_ports2:
                new_transitions.add((src, symbol, dst))

    return new_transitions


def generate_new_transitions_random(aut1: NFA, 
                                    aut2: NFA, 
                                    gate_symbols: set[str]) \
                                     -> set[tuple[State, str, State]]:
    new_transitions: set[tuple[State, str, State]] = set()
    state_list1 = list(aut1.states)
    state_list2 = list(aut2.states)

    for symbol in gate_symbols:
        num_gates = randint(2, 6)

        for _ in range(num_gates):
            src = choice(state_list1)
            dst = choice(state_list2)
            new_transitions.add((src, symbol, dst))

    return new_transitions


def compose_with_gates(aut1: NFA,
                       aut2: NFA,
                       gate_symbols: set[str],
                       mode: str,
                       fin_states_in_first: bool,
                       init_states_in_second: bool) \
                        -> ExtendedNFA:
    state_counter = utils.str_counter_except(set(aut1.states) | aut2.states)
    states = set(aut1.states)
    transitions = NFATransitions.from_frozen(aut1.transitions)
    input_symbols = set(aut1.input_symbols)

    if mode == "disjoint":
        gates, new_states = generate_new_transitions_disjoint2(
            aut1, aut2, gate_symbols, state_counter)
        states.update(new_states)
    elif mode == "random":
        gates = generate_new_transitions_random(aut1, aut2, gate_symbols)
    else:
        gates = generate_new_transitions_equal(aut1, aut2, gate_symbols)

    state_map = utils.create_state_map(aut2.states, counter=state_counter)
    states.update(state_map.values())

    utils.add_transitions(state_map, transitions, aut2.transitions)
    input_symbols.update(aut2.input_symbols, gate_symbols)

    for src, symbol, dst in gates:
        if dst in aut2.states:
            transitions.add_edge(symbol, src, state_map[dst])
        else:
            transitions.add_edge(symbol, src, dst)

    initial_states = {aut1.initial_state}
    if init_states_in_second:
        initial_states.add(state_map[aut2.initial_state])

    final_states = {state_map[state] for state in aut2.final_states}
    if fin_states_in_first:
        final_states.update(aut1.final_states)

    return ExtendedNFA(states=states, 
                       input_symbols=input_symbols,
                       transitions=transitions, 
                       initial_states=initial_states,
                       final_states=final_states)


def generate_and_save_2comp_gate(count: int,
                                rlength: int,
                                name_counter: Iterator[Any],
                                alphabet: set[str],
                                gate_symbols: set[str],
                                gate_sym_in_first: bool,
                                gate_sym_in_second: bool,
                                fin_states_in_first: bool,
                                init_states_in_second: bool,
                                dir: str,
                                regex_file_path: str,
                                save_pics: bool = False,
                                mode: str = "equal"):
    with open(regex_file_path, "a") as regex_file:

        for i in range(count):
            rlength1 = randrange(3, rlength - 2)
            rlength2 = rlength - rlength1

            first_alphabet = alphabet | gate_symbols if gate_sym_in_first else alphabet
            second_alphabet = alphabet | gate_symbols if gate_sym_in_second else alphabet

            aut1, regex1 = generate_from_regex(rlength1, alphabet=first_alphabet)
            aut2, regex2 = generate_from_regex(rlength2, alphabet=second_alphabet)

            aut = compose_with_gates(aut1,
                                     aut2,
                                     gate_symbols,
                                     mode,
                                     fin_states_in_first,
                                     init_states_in_second)

            gate_sym_string = "".join(gate_symbols)
            name = f"gate_{next(name_counter)}_r{rlength}_{gate_sym_string}"

            parser.ext_nfa_to_dot(aut, f"{dir}{name}.dot")
            if save_pics:
                parser.dot_to_png(f"{dir}{name}.dot", f"{dir}{name}.png")

            regex_file.write(f"{name} {regex1} {regex2}\n")


def generate_and_save_from_regexes(rlength: int,
                                    count: int,
                                    name_start: int,
                                    dir: str,
                                    regex_file_path: str,
                                    alphabet: set[str] = {"a", "b"}):
    with open(regex_file_path, "a") as regex_file:

        for i in range(name_start, name_start + count):
            aut, regex = generate_from_regex(rlength, alphabet=alphabet)
            regex = clean_regex(regex)

            name = f"{i}_r{rlength}"
            aut.show_diagram(path=f"{dir}random_aut_{name}.png")
            parser.nfa_to_dot(aut, f"{dir}{name}.dot")

            regex_file.write(f"{name} {regex} \n")


# gate NFA accepting (a+b)*.a.(a+b)^n.c.(a+b)^n.a.(a+b)*

def special_gate_2(n: int) -> NFA:
    state_counter = utils.str_counter(0)
    first_loop = next(state_counter)
    last_loop = next(state_counter)
    first_next = next(state_counter)
    pred_last = next(state_counter)
    middle1 = next(state_counter)
    middle2 = next(state_counter)

    states = {first_loop, last_loop, first_next, pred_last, middle1, middle2}
    input_symbols = {"a", "b", "c"}
    trans_dict: NFATransDict = {
        first_loop : {"a": {first_loop, first_next}, "b": {first_loop}},
        middle1 : {"c": {middle2}},
        pred_last : {"a": {last_loop}},
        last_loop : {"a": {last_loop}, "b": {last_loop}}
    }
    transitions = NFATransitions(trans_dict)
    initial_state = first_loop
    final_states = {last_loop}

    src1 = first_next
    src2 = middle2
    for _ in range(n-1):
        dst1 = next(state_counter)
        dst2 = next(state_counter)
        states.add(dst1)
        states.add(dst2)

        for symbol in ["a", "b"]:
            transitions.add_edge(symbol, src1, dst1)
            transitions.add_edge(symbol, src2, dst2)

        src1 = dst1
        src2 = dst2

    for symbol in ["a", "b"]:
        transitions.add_edge(symbol, src1, middle1)
        transitions.add_edge(symbol, src2, pred_last)

    return NFA(states=states,
               input_symbols=input_symbols,
               transitions=transitions.get_dict(),
               initial_state=initial_state,
               final_states=final_states)


def generate_special_gate_2(n: int, path: str):
    nfa = special_gate_2(n)
    parser.nfa_to_dot(nfa, path)
    nfa.show_diagram(path[:-4] + ".png")


# NFA accepting (a+b)*.a.(a+b).(a+b)

def special_back_det() -> NFA:
    nfa = NFA(
        states={"0", "1", "2", "3"},
        input_symbols={"a", "b"},
        transitions={
            "0": {"a": {"0", "1"}, "b": {"0"}},
            "1": {"a": {"2"}, "b": {"2"}},
            "2": {"a": {"3"}, "b": {"3"}},
        },
        initial_state="0",
        final_states={"3"}
    )
    return nfa


# NFA accepting (a+b).(a+b).a.(a+b)*

def special_2() -> NFA:
    nfa = NFA(
        states={"0", "1", "2", "3"},
        input_symbols={"a", "b"},
        transitions={
            "0": {"a": {"1"}, "b": {"1"}},
            "1": {"a": {"2"}, "b": {"2"}},
            "2": {"a": {"3"}},
            "3": {"a": {"3"}, "b": {"3"}}
        },
        initial_state="0",
        final_states={"3"}
    )
    return nfa


# NFA accepting (a+b).a.(a+b)*

def special_3() -> NFA:
    nfa = NFA(
        states={"1", "2", "3"},
        input_symbols={"a", "b"},
        transitions={
            "1": {"a": {"2"}, "b": {"2"}},
            "2": {"a": {"3"}},
            "3": {"a": {"3"}, "b": {"3"}}
        },
        initial_state="1",
        final_states={"3"}
    )
    return nfa


# SELF-LOOP NFAs

def initially_connect_forward(num_states: int,
                              input_symbols: set[str],
                              initial: str):
    connected = [initial]
    not_connected = [str(s) for s in range(num_states - 1, 0, -1)]
    transitions = NFATransitions({str(s): {} for s in range(num_states)})
    input_symbols_lst = list(input_symbols)

    while len(connected) < num_states:
        state_in = choice(connected)
        state_out = not_connected.pop()
        ap = choice(input_symbols_lst)
        transitions.add_edge(ap, state_in, state_out)
        connected.append(state_out)

    return transitions


def leslie_random_nfa_forward(num_states: int,
                              input_symbols: set[str],
                              num_transitions: int) -> NFA:
    initial = "0"
    states = set([str(s) for s in range(num_states)])
    transitions = initially_connect_forward(num_states, input_symbols, initial)
    prob = (num_transitions - num_states + 1) / \
        ((num_states * (num_states + 1) / 2) * len(input_symbols))

    for src in range(num_states):
        rand_loop = random()
        for ap in input_symbols:
            if rand_loop <= 0.2 or random() <= 0.1:
                transitions.add_edge(ap, str(src), str(src))

            for dst in range(src + 1, num_states):
                if random() <= prob:
                    transitions.add_edge(ap, str(src), str(dst))

    final_states = set(sample(states, randrange(1, num_states)))

    nfa = NFA(states=states, input_symbols=input_symbols,
              transitions=transitions.get_dict(), initial_state=initial,
              final_states=final_states)
    return nfa


# GENERAL NFAs
# source: https://repositorio-aberto.up.pt/bitstream/10216/120971/2/341105.pdf

def initially_connect(num_states: int, input_symbols: set[str]):
    connected = ["0"]
    not_connected = [str(s) for s in range(num_states - 1, 0, -1)]
    transitions = NFATransitions({str(s): {} for s in range(num_states)})
    input_symbols_lst = list(input_symbols)

    while len(connected) < num_states:
        state_in = choice(connected)
        state_out = choice(not_connected)
        not_connected.remove(state_out)
        ap = choice(input_symbols_lst)
        transitions.add_edge(ap, state_in, state_out)
        connected.append(state_out)

    return transitions


def leslie_random_nfa(num_states: int,
                      input_symbols: set[str],
                      num_transitions: int) -> NFA:
    states = set([str(s) for s in range(num_states)])
    transitions = initially_connect(num_states, input_symbols)
    prob = (num_transitions - num_states + 1) \
            / ((num_states**2) * len(input_symbols))

    for src in range(num_states):
        for ap in input_symbols:
            for dst in range(num_states):
                if random() <= prob:
                    transitions.add_edge(ap, str(src), str(dst))

    final_states = set(sample(states, randrange(1, num_states)))

    nfa = NFA(states=states, input_symbols=input_symbols,
              transitions=transitions.get_dict(), initial_state="0",
              final_states=final_states)
    return nfa


def generate_nfa(min_states: int, max_states: int, alphabet: set[str]) -> NFA:
    num_states = randrange(min_states, max_states)
    num_transitions = randrange(num_states, num_states * 2 * len(alphabet))
    aut_nfa = leslie_random_nfa(num_states, alphabet, num_transitions)
    return aut_nfa


def generate_and_save(start: int,
                      amount: int,
                      min_states: int,
                      max_states: int,
                      dir: str):
    alphabets = [set(["a", "b"]), set(["a", "b", "c"]), 
                 set(["a", "b", "c", "d"]),
                 set(["a", "b", "c", "d", "e"]), 
                 set(["a", "b", "c", "d", "e", "f"])]

    for i in range(start, start + amount):
        num_states = randrange(min_states, max_states)
        alphabet = choice(alphabets)
        num_transitions = randrange(num_states, num_states *2* len(alphabet))

        aut_nfa = leslie_random_nfa(num_states, alphabet, num_transitions)
        aut_nfa.show_diagram(path="{}random_aut_{}.png".format(dir, str(i)))

        parser.nfa_to_dot(aut_nfa, "{}{}.dot".format(dir, str(i)))


if __name__ == "__main__":
    # generate_and_save(10, 2, 7, "./testing_gates/")

    # generate_and_save(150, 50, 3, 5, "./nfa_lib/")

    generate_and_save_from_regexes(12, 50, 200, 
                                   "./src/automata_libs/random_general_small/",
                                   "./src/automata_libs/random_general_small/regexes.txt",
                                   alphabet={"a", "b", "c", "d"})

    # rlength = 1000
    # equal = False
    # first = True
    # basic = True

    # mode = "equal" if equal else "disjoint"
    # dir_suffix1 = "" if first else "_second"
    # dir_suffix2 = "" if basic else "_product"
    # dir_name = f"gate_lib_{mode}_{rlength}{dir_suffix1}{dir_suffix2}"
    # name_counter = count(809)
    # dir = f"./automata_libs/gate_random_1000/"
    # # dir = "./testing/"

    # for mode in ["equal", "random"]:
    #     for first in [True, False]:
    #         for basic in [True]:
    #             # mode = "equal" if equal else "disjoint"
    #             generate_and_save_2comp_gate(count=20,
    #                                          rlength=rlength,
    #                                          name_counter=name_counter,
    #                                          alphabet={"a", "b", "c"},
    #                                          gate_symbols={"g", "h"},
    #                                          gate_sym_in_first=(not first),
    #                                          gate_sym_in_second=first,
    #                                          fin_states_in_first=((first) or (not first and not basic)),
    #                                          init_states_in_second=((not first) or (first and not basic)),
    #                                          dir=dir,
    #                                          regex_file_path=f"{dir}regexes.txt",
    #                                          mode=mode,
    #                                          save_pics=False)
