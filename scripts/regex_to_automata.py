import os
import libmata.parser as mata_parser
import libmata.alphabets as alphabets
import libmata.nfa.nfa as mata_nfa
import aligater.utils.parser as ali_parser
from aligater.utils import mata_handler
import re
import codecs


number_map = {str(c): c for c in range(256)}
wanted_symbols = set(number_map.keys())


def unescape_invalid_escapes(string: str) -> str:
    return re.sub(r'([^\\])\\([\dceGghikKlmopPqTuyVE])', r'\1\2', string)


def is_allowed(regex: str) -> bool:
    invalid = re.compile(r"\{\d{4},?\}|\(\?=|\\x80-t")

    return re.search(invalid, regex) is None


def regex_to_mata_nfa(regex: str):
    regex = unescape_invalid_escapes(regex)

    try:
        return mata_parser.from_regex(regex, encoding="Latin-1")
    except Exception as e:
        print(regex)
        raise e


def from_regex(regex, output_path):
    m_nfa = regex_to_mata_nfa(regex)
    alphabet = alphabets.OnTheFlyAlphabet.from_symbol_map(number_map)

    try:
        ext_nfa = ali_parser.mata_nfa_to_ext_nfa(mata_handler.MataNfaContainer(m_nfa, alphabet=alphabet), default_symbol_value = "256")
    except Exception as e:
        print(regex)
        raise e

    # ext_nfa_wo_sym = ext_nfa.remove_symbols(wanted_symbols)

    ali_parser.ext_nfa_to_vtf(ext_nfa, output_path, f"regex: {regex}")


def add_union(regex: str, add_to):
    try:
        m_nfa = regex_to_mata_nfa(regex)
    except Exception as e:
        return
    
    add_to = add_to.union(m_nfa)


def create_automaton_for_regex_file(regexes: list[str]):
    nfa = mata_nfa.Nfa()

    for regex in regexes:
        regex = regex.strip()
        if regex and is_allowed(regex) and not regex.startswith("#"):
            # print(regex)
            # regex = preprocess_regex(regex) # not needed anymore, this is handled in regex_translator
            add_union(regex, nfa)

    return nfa


def process_re2_files(directory: str, union_automata_dir: str, generate_union: bool = False):
    # Iterate over all files in the directory
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith('.re2'):
                print(file)
                re2_file_path = os.path.join(root, file)
                
                # Read the .re2 file line by line
                with open(re2_file_path, 'r') as re2_file:
                    regexes = re2_file.readlines()

                automaton_dir_name = os.path.splitext(file)[0]

                if generate_union:
                    os.makedirs(union_automata_dir, exist_ok=True)
                    automaton_path = os.path.join(union_automata_dir, f"{os.path.splitext(file)[0]}.mata")

                    if not os.path.isfile(automaton_path):
                        m_nfa = create_automaton_for_regex_file(regexes)
                        alphabet = alphabets.OnTheFlyAlphabet.from_symbol_map(number_map)
                        ext_nfa = ali_parser.mata_nfa_to_ext_nfa(mata_handler.MataNfaContainer(m_nfa, alphabet=alphabet), default_symbol_value = "256")
                        ali_parser.ext_nfa_to_vtf(ext_nfa, automaton_path)
                
                # Process each regex in the file
                automaton_dir = os.path.join(root, automaton_dir_name)

                if not os.path.isdir(automaton_dir):
                    os.mkdir(automaton_dir)
                
                for i, regex in enumerate(regexes):
                    regex = regex.strip()
                    if regex and is_allowed(regex) and not regex.startswith("#"):
                        # Generate a filename for the automaton
                        automaton_file_path = os.path.join(automaton_dir, os.path.basename(automaton_dir) + f'_aut_{i+1}.mata')

                        if not os.path.isfile(automaton_file_path):
                            # print(file)
                            # print(regex)
                            from_regex(regex, automaton_file_path)


if __name__ == "__main__":
    # Define the directory containing the .re2 files
    directory = "/home/notme/nfa-bench/benchmarks/regexps/"
    union_automata_dir = "/home/notme/nfa-bench/benchmarks/regexps_union/"
    
    # Process the .re2 files
    process_re2_files(directory, union_automata_dir, True)
