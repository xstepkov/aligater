import os

def remove_states_line(src_path: str, dst_path: str):
    with open(src_path, "r") as src:
        with open(dst_path, "w") as dst:

            for line in src:
                if not line.startswith("%States"):
                    dst.write(line)

def rename_files(root_dir):
    for dirpath, dirnames, filenames in os.walk(root_dir):
        for filename in filenames:
            if ".mata" in filename:
                old_path = os.path.join(dirpath, filename)
                new_filename = os.path.basename(dirpath) + '_' + filename
                new_path = os.path.join(dirpath, new_filename)

                remove_states_line(old_path, new_path)
                os.remove(old_path)

if __name__ == "__main__":
    root_directory = '/home/xstepkov/nfa-bench/benchmarks/regexps/yang2010'
    rename_files(root_directory)